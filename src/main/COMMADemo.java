package main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import org.hal.core.agent.AgentController;
import org.hal.core.agent.AgentID;

import argumentationProtocols.triggers.ArgumentationMessage;
import argumentationProtocols.triggers.PublishPrompt;
import demoProtocol.DemoProtocol;
/** Auxiliary class for the different demos. */
public class COMMADemo {
	protected final static Map<String, AgentID> ids = new HashMap<>();
	protected final static Map<AgentID, String> roles = new HashMap<>();
	protected final static StringBuffer output = new StringBuffer();
	protected static int nrOfAgentsNotDone = 4;
	protected static String outputFilePath = "./visualizer/js/dialogGraphDivision.js";
	protected static String outputGraphName = "Division";
	protected final static String visualizationOptionsFilePath = "visualizationOptions.json";
	
	protected final static void outputMessage(final ArgumentationMessage<?> messageRaw) {
		System.out.println("From: "+roles.get(messageRaw.getSender()) +" to: "+roles.get(messageRaw.getReceiver())+" content: "+messageRaw.getContent());
	}
	
	/** Turn the dialog graph into JSON and send to the dialog reader. */
	protected final static void publish(final PublishPrompt p, final AgentController controller) {
		final BeliefBase belief = controller.getContext(BeliefBase.class);
		final DemoProtocol dialog = belief.getProtocol();
		final Map<AgentID, String> peerNames = new HashMap<>();
		final String ownerName =  roles.get(dialog.getDialogGraph().getOwner());
		peerNames.put(dialog.getDialogGraph().getOwner(), ownerName);
		for(AgentID peer : dialog.getTurnMechanism().getAllConnectedPeers())
			peerNames.put(peer, roles.get(peer));
		publishGraph((new StringBuffer()).append("\""+ownerName+"\":").append(dialog.getDialogGraph().toJSON(peerNames)));
	} 
	
	/**
	 * Add the dialog graph to the output.
	 * @param agent
	 * @param graph
	 */
	protected final static void publishGraph(final StringBuffer graph){
		synchronized(output){
			// Append the result
			output.append(",\r\n").append(graph);
			nrOfAgentsNotDone--;
			// Everybody is done! output to file
			if(nrOfAgentsNotDone == 0){
				final StringBuffer buffer = new StringBuffer();
				buffer.append("var dialogGraphs").append(outputGraphName).append(" = {\r\n");
				output.setCharAt(0, ' '); // Remove the first comma
				buffer.append(output);
				buffer.append("};\r\n");
				// Write to file and exit
				try (final PrintWriter writer = new PrintWriter(outputFilePath)){
					// Add the visualization options
					readFileLineByLine(visualizationOptionsFilePath, line -> {buffer.append(line).append("\r\n");});
					writer.print(buffer.toString()); 
					writer.flush();
					writer.close();
					System.exit(0);
				} catch (IOException e) { 
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Read the given file line by line.
	 * @param filePath The path to the file to be read.
	 * @param lineOperation The operation to perform on each line.
	 * @throws IOException Thrown when the file did not exist.
	 */
	private final static void readFileLineByLine(final String filePath, final Consumer<String> lineOperation) throws IOException { 
		final BufferedReader reader = new BufferedReader(new FileReader(filePath));
		reader.lines().forEach(lineOperation);
		reader.close();
	}
}
