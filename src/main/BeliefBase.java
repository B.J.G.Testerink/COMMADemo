package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;

import org.hal.core.agent.Context;
import org.politie.nl.argumentation.andorgraph.AndNode;
import org.politie.nl.argumentation.andorgraph.AndOrGraph;
import org.politie.nl.argumentation.andorgraph.Argument;
import org.politie.nl.argumentation.andorgraph.OrNode;
import org.politie.nl.argumentation.dunglabelling.GroundedSemantics;
import org.politie.nl.argumentation.dunglabelling.Label; 

import demoProtocol.DemoProtocol;
/**
 * We use a simple belief base that consists of a set of observations and 
 * a set of arguments that form the defeasible beliefs given the observations. 
 * The arguments are built with defeasible rules. For ease of use we store also
 * the state of the argumentation dialog in the belief base.
 */
public final class BeliefBase implements Context {
	private final Map<String, Set<Set<String>>> rules;
	private final Set<String> topics, observable, observations;
	private final DemoProtocol protocol;
	private Map<Argument<String>, Label> labeling;
	
	public BeliefBase(
			final Map<String, Set<Set<String>>> rules, 
			final String[] topics, 
			final String[] observable,
			final UUID session) {
		this.rules = rules;
		this.topics = new HashSet<>(Arrays.asList(topics));
		this.observable = new HashSet<>(Arrays.asList(observable));
		this.observations = new HashSet<>();
		this.protocol = new DemoProtocol(session);
		this.labeling = new HashMap<>();
	}
	
	public final void addObservation(final String observation) {
		this.observations.add(observation); 
	}
	
	/**
	 * Create the arguments and evaluate them using grounded semantics.
	 */
	public final void updateArguments() {
		final Map<String, OrNode<String>> andOrGraph = AndOrGraph.buildAndOrGraph(this.rules); 
        
		// Ensure the existence of all observable propositions in the and-or-graph
		for(String proposition : this.observable){
		  if(andOrGraph.get(proposition) == null){
			  andOrGraph.put(proposition, new OrNode<>(proposition));
		  }
		}  

		final Map<String, Set<String>> relevance = new HashMap<>();
		final Map<OrNode<String>, Label> preAssumptionOrColoring = new HashMap<>();
		final Map<AndNode<String>, Label> preAssumptionAndColoring = new HashMap<>();
		final Map<OrNode<String>, Label> postAssumptionOrColoring = new HashMap<>();
		final Map<AndNode<String>, Label> postAssumptionAndColoring = new HashMap<>();
		AndOrGraph.getRelevantObservations(andOrGraph, BeliefBase::contrary, this.observations, observable, topics,
					preAssumptionOrColoring, preAssumptionAndColoring, postAssumptionOrColoring, postAssumptionAndColoring,
					relevance);
		final Map.Entry<Argument<String>[], Function<Argument<String>, Set<Argument<String>>>> dungGraph = createDungGraph(andOrGraph, this.observations);
        final Map<Argument<String>, Set<Argument<String>>> attackRelation = createAttackRelation(dungGraph);
        this.labeling = GroundedSemantics.label(dungGraph.getKey(), attackRelation::get);
          
		final Map<String, Label> topicToPostAssumptionLabel = new HashMap<>(); 
		final Set<String> topicsWithArgument = new HashSet<>();
		for(String topic : topics) {
			final Label label = postAssumptionOrColoring.getOrDefault(andOrGraph.get(topic), Label.BLANK);
			topicToPostAssumptionLabel.put(topic, label);
			for(Map.Entry<Argument<String>, Label> dung : labeling.entrySet()) {
				if(dung.getKey().getConclusion().equals(topic)) {
					topicsWithArgument.add(topic);
				}
			}
		}
	}
	
	/** Builds the Dung graph (attack graph) from the defeasible rules and observations. */
	private static final Map.Entry<Argument<String>[], Function<Argument<String>, Set<Argument<String>>>> createDungGraph(
  		  final Map<String, OrNode<String>> andOrGraph, 
  		  final Set<String> observations) {
        return AndOrGraph.produceDungGraph(
                andOrGraph,
                observations,
                (a,b) -> contraryDung(a, b, observations));
    }

    private static final Map<Argument<String>, Set<Argument<String>>> createAttackRelation(
  		  final Map.Entry<Argument<String>[], Function<Argument<String>, Set<Argument<String>>>> dungGraph) {
        final Map<Argument<String>, Set<Argument<String>>> attackRelation = new HashMap<>();
        for (Argument<String> argument : dungGraph.getKey()) {
            attackRelation.put(argument, dungGraph.getValue().apply(argument));
        }
        return attackRelation;
    }

    private static final boolean contraryDung(final String a, final String b, final Set<String> observations) {
  	  if(observations.contains(b)) return false; // Can't contradict observation
        return negates(a, b) || negates(b, a);
    }

    private static final boolean contrary(final String a, final String b) {
  	  return negates(a, b) || negates(b, a);
    }

    private static final boolean negates(final String a, final String b) {
        return a.startsWith("~") && (a.length() == b.length() + 1) && a.regionMatches(false, 1, b, 0, b.length());
    }
	
	public final DemoProtocol getProtocol() {
		return this.protocol;
	}
	
	public final Map<Argument<String>, Label> getArguments(){ 
		return this.labeling;
	}
	
	public final boolean isObservable(final String proposition) {
		return this.observable.contains(proposition);
	}
	
	public final boolean hasInArgumentFor(final String proposition) {
		for(Entry<Argument<String>, Label> e : this.labeling.entrySet()) {
			if(e.getValue() == Label.IN && e.getKey().getConclusion().equals(proposition))
				return true;
		}
		return false;
	}
}
