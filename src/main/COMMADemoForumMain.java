package main;
 
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.hal.core.agent.Agent;
import org.hal.core.agent.AgentController;
import org.hal.core.agent.AgentID;
import org.hal.core.agent.AgentSpecification;
import org.hal.core.agent.Platform;
import org.politie.nl.argumentation.andorgraph.Argument;
import org.politie.nl.argumentation.andorgraph.RuleParser;
import org.politie.nl.argumentation.dunglabelling.Label;

import argumentationProtocols.graphs.Argument2Node;
import argumentationProtocols.graphs.INode;
import argumentationProtocols.triggers.ArgumentationMessage;
import argumentationProtocols.triggers.PublishPrompt;
import demoProtocol.DemoProtocol;
import demoProtocol.locutions.Claim;
import demoProtocol.locutions.DemoLocution;
import demoProtocol.locutions.Question;
import demoProtocol.locutions.Since;
import demoProtocol.locutions.Skip;
import demoProtocol.locutions.Why;

/** 
 * This demo shows a construction where agents send locutions to the group node but do 
 * not directly engage in an exchange of locutions as the group never sends a locution 
 * back. Instead, the group notifies agents of a new locution by passing its turn through 
 * a skip action. The agent can then consult the groups dialog graph to see if they want 
 * to submit a reply as their own turn, or whether they want to skip theirs too. Note that 
 * in the resulting dialog graphs, the client agent has no argument 1 nodes as it never 
 * asserts propositions or inferences. It does, however, absorb knowledge from the group and 
 * hence learns things from the interactions. Note also that all locutions are not directly
 * corresponding to a specific agent, but to specific locutions.
 */
public final class COMMADemoForumMain extends COMMADemo { 
	private static String outputFile = "./visualizer/js/dialogGraphForum.js"; 
	
	public final static void main(final String[] args) {
		System.out.println("Starting demo...");
		COMMADemo.outputFilePath = outputFile;
		COMMADemo.outputGraphName = "Forum";
		final Platform platform = new Platform();
		final UUID session = UUID.randomUUID();
		
		// Argumentation configuration
        final Map<String, Set<Set<String>>> rules = 
        		RuleParser.parseRules(s -> s, "=>", "QA=>PA;QB=>PB;QA,QB=>PC"); 
        final String[] observable = new String[] {"QA", "~QA", "QB", "~QB"};
        final String[] topics = new String[] {"PC"};
        
		// The group decision logic 
		final AgentSpecification group = (new AgentSpecification())
				.addContext(() -> new BeliefBase(rules, topics, observable, session))
				.addExternalPlanScheme(StartTrigger.class, COMMADemoForumMain::start)
				.addExternalPlanScheme(PublishPrompt.class, COMMADemoForumMain::publish)
				.addExternalPlanScheme(RequestDialogGraph.class, COMMADemoForumMain::returnForumGraph)
				.addExternalPlanScheme(ArgumentationMessage.class, COMMADemoForumMain::decisionLogicForum);
        final Agent groupAgent = Agent.create(platform, group);
		ids.put("group", groupAgent.getAgentID());
		roles.put(groupAgent.getAgentID(), "group");
		
		// The group members (experts)
		final AgentSpecification user = (new AgentSpecification())
				.addContext(() -> new BeliefBase(rules, topics, observable, session))
				.addExternalPlanScheme(StartTrigger.class, COMMADemoForumMain::start)
				.addExternalPlanScheme(PublishPrompt.class, COMMADemoForumMain::publish)
				.addExternalPlanScheme(ShareDialogueGraphTrigger.class, COMMADemoForumMain::userPost)
				.addExternalPlanScheme(ArgumentationMessage.class, COMMADemoForumMain::decisionLogicUser);
        final Agent expertAAgent = Agent.create(platform, user);
        final Agent expertBAgent = Agent.create(platform, user);
        ids.put("expertA", expertAAgent.getAgentID());
		roles.put(expertAAgent.getAgentID(), "expertA");
        ids.put("expertB", expertBAgent.getAgentID());
		roles.put(expertBAgent.getAgentID(), "expertB"); 
		
		// The peer to the group (client)  
		final Agent clientAgent = Agent.create(platform, user);
		ids.put("client", clientAgent.getAgentID());
		roles.put(clientAgent.getAgentID(), "client");
		 
        expertAAgent.add(new StartTrigger(""));
        expertBAgent.add(new StartTrigger(""));
		clientAgent.add(new StartTrigger(""));
        groupAgent.add(new StartTrigger(""));
		
		try {
			Thread.sleep(500);
			clientAgent.add(new PublishPrompt());
			groupAgent.add(new PublishPrompt());
			expertAAgent.add(new PublishPrompt());
			expertBAgent.add(new PublishPrompt());
			System.out.println("Done");
		} catch (InterruptedException e) { 
			e.printStackTrace();
		}
	} 
	
	
	/** Some initialization for the demo such as who may speak first. */
	private final static void start(final StartTrigger trigger, final AgentController controller) {
		final BeliefBase belief = controller.getContext(BeliefBase.class);
		final DemoProtocol dialog = belief.getProtocol();
		dialog.finalize(controller.getAgentID());
		if(roles.get(controller.getAgentID()).equals("client")) {
			dialog.getTurnMechanism().addPeer(ids.get("group"), true);
		} else if(roles.get(controller.getAgentID()).equals("group")) { 
			dialog.getTurnMechanism().addPeer(ids.get("client"), false);
			dialog.getTurnMechanism().addPeer(ids.get("expertA"), false);
			dialog.getTurnMechanism().addPeer(ids.get("expertB"), false);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) { 
				e.printStackTrace();
			}
			dialog.send(controller, new ArgumentationMessage<>(
					controller.getAgentID(), 
					dialog.getSessionId(), 
					new Skip(), 
					ids.get("client")));
		} else { 
			dialog.getTurnMechanism().addPeer(ids.get("group"), true); 
			if(roles.get(controller.getAgentID()).equals("expertA")) {
				belief.addObservation("QA");
			} else {
				belief.addObservation("QB");
			}
			belief.updateArguments();
		} 
	}
	
	/** A user simply requests the dialog graph upon each turn. */
	private final static void decisionLogicUser(final ArgumentationMessage<?> messageRaw, final AgentController controller) {
		outputMessage(messageRaw); 
		final BeliefBase belief = controller.getContext(BeliefBase.class);
		final DemoProtocol dialog = belief.getProtocol();
		if(dialog.isLegallyReceived(messageRaw)) {
			controller.send(ids.get("group"), new RequestDialogGraph(controller.getAgentID()));
		}
	}
	
	/** Upon receiving the group's dialog graph it is checked whether the agent has anything 
	 * useful to add to the discussion. */
	private final static void userPost(final ShareDialogueGraphTrigger trigger, final AgentController controller) {
		// Absorb all observables from graph
		final demoProtocol.DialogGraph graph = (demoProtocol.DialogGraph) trigger.getGraph();
		final BeliefBase belief = controller.getContext(BeliefBase.class);
		for(INode node : graph.getNodesOfType(INode.class))
			if(belief.isObservable(node.getText()))
				belief.addObservation(node.getText());
		belief.updateArguments();
		
		DemoLocution locution = new Skip();
		if(roles.get(controller.getAgentID()).equals("client")) {  
			if(!existsLocution(graph, new Question("PA"))) {
				locution = new Question("PA");
			} else if(belief.hasInArgumentFor("PA") && !existsLocution(graph, new Question("PB"))) {
				locution = new Question("PB");
			} else {
				final Optional<DemoLocution> challenge = canChallengeClaim(belief, graph);
				if(challenge.isPresent()) {
					locution = challenge.get();
				}
			}
		} else {
			final Optional<DemoLocution> claim = canAnswerQuestion(belief, graph);
			if(claim.isPresent()) {
				locution = claim.get();
			} else {
				final Optional<DemoLocution> answer = canAnswerChallenge(belief, graph);
				if(answer.isPresent())
					locution = answer.get();
			}
		}
		
		final DemoProtocol dialog = belief.getProtocol();
		dialog.send(controller, new ArgumentationMessage<>(
				controller.getAgentID(), 
				dialog.getSessionId(), 
				locution, 
				ids.get("group"))); 
	}
	
	/**Whether a specific locution has been uttered.*/
	private final static boolean existsLocution(final demoProtocol.DialogGraph graph, final DemoLocution locution) {
		for(Argument2Node<DemoLocution> node : graph.getMessageNodes()) {
			if(node.getContent().equals(locution))
				return true;
		}
		return false;
	}

	/**Whether a open question remains on the agent's expertise (as modeled in the belief base).*/
	private final static Optional<DemoLocution> canAnswerQuestion(final BeliefBase belief, final demoProtocol.DialogGraph graph) {
		for(Argument2Node<DemoLocution> node : graph.getMessageNodes()) {
			if(node.getContent() instanceof Question) {
				final String question = ((Question)node.getContent()).getProposition();
				final DemoLocution claim = new Claim(question);
				if(!existsLocution(graph, claim) && belief.hasInArgumentFor(question)) {
					return Optional.of(claim);
				}
			}
		}
		return Optional.empty();
	}
	
	/** Whether there is a claim that this agent does not believe. */
	private final static Optional<DemoLocution> canChallengeClaim(final BeliefBase belief, final demoProtocol.DialogGraph graph) {
		for(Argument2Node<DemoLocution> node : graph.getMessageNodes()) {
			if(node.getContent() instanceof Claim) {
				final String claim = ((Claim)node.getContent()).getProposition();
				final DemoLocution challenge = new Why(claim);
				if(!existsLocution(graph, challenge) && !belief.hasInArgumentFor(claim)) { 
					return Optional.of(challenge);
				}
			}
		}
		return Optional.empty();
	}
	
	/** Whether there is an unanswered challenge that the agent may answer. */
	private final static Optional<DemoLocution> canAnswerChallenge(final BeliefBase belief, final demoProtocol.DialogGraph graph) {
		for(Argument2Node<DemoLocution> node : graph.getMessageNodes()) {
			if(node.getContent() instanceof Why) {
				final String challenge = ((Why)node.getContent()).getProposition(); 
				for(Entry<Argument<String>, Label> e : belief.getArguments().entrySet()) {
					if(e.getValue() == Label.IN && e.getKey().getConclusion().equals(challenge)) {
						DemoLocution since = new Since(
								e.getKey().getConclusion(), 
								e.getKey().getPremises().stream()
								.map(a -> a.getConclusion())
								.collect(Collectors.toList())
								.toArray(new String[e.getKey().getPremises().size()])); 
						if(!existsLocution(graph, since)) {
							return Optional.of(since);
						}
					}
				} 
			}
		}
		return Optional.empty();
	}
	
	/** Deliver the dialog graph upon request. */
	private final static void returnForumGraph(final RequestDialogGraph t, final AgentController controller) {
		final BeliefBase belief = controller.getContext(BeliefBase.class);
		final DemoProtocol dialog = belief.getProtocol();
		controller.send(t.getDeliverAddress(), new ShareDialogueGraphTrigger(dialog.getDialogGraph()));
	} 
	
	private static boolean stable = true;
	
	/** The forum works in a round based fashion. Agents all receive a skip from the forum when a new round starts, 
	 * upon which they all have to reply before the next round can begin. */
	private final static void decisionLogicForum(final ArgumentationMessage<?> messageRaw, final AgentController controller) {
		outputMessage(messageRaw);
		final BeliefBase belief = controller.getContext(BeliefBase.class);
		final DemoProtocol dialog = belief.getProtocol();
		if(dialog.isLegallyReceived(messageRaw)) { 
			if(!(messageRaw.getContent() instanceof Skip)) {
				stable = false;
			}
			if(!stable && canBroadcast(controller.getAgentID(), dialog)) {
				stable = true;
				for(Entry<AgentID, String> e : roles.entrySet())
					if(!e.getValue().equals("group"))
						dialog.send(controller, new ArgumentationMessage<>(
								controller.getAgentID(), 
								dialog.getSessionId(), 
								new Skip(), 
								e.getKey()));
			} 
		}
	}
	
	/** Whether the agent can send to all other agents a messages given the turntaking mechanism. */
	private final static boolean canBroadcast(final AgentID me, final DemoProtocol dialog) {
		for(AgentID id : roles.keySet()) {
			if(!id.equals(me) && !dialog.getTurnMechanism().allowedToSendTo(me, id))
				return false;
		}
		return true;
	}
}
