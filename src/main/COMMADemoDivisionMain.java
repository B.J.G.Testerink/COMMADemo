package main;
 
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.hal.core.agent.Agent;
import org.hal.core.agent.AgentController;
import org.hal.core.agent.AgentID;
import org.hal.core.agent.AgentSpecification;
import org.hal.core.agent.Platform;
import org.politie.nl.argumentation.andorgraph.Argument;
import org.politie.nl.argumentation.andorgraph.RuleParser;
import org.politie.nl.argumentation.dunglabelling.Label;
 
import argumentationProtocols.triggers.ArgumentationMessage;
import argumentationProtocols.triggers.PublishPrompt;
import demoProtocol.DemoProtocol;
import demoProtocol.locutions.Claim;
import demoProtocol.locutions.Concede;
import demoProtocol.locutions.DemoLocution;
import demoProtocol.locutions.Question;
import demoProtocol.locutions.Since;
import demoProtocol.locutions.Skip;
import demoProtocol.locutions.Why;
/**
 * This demo shows a group interaction where a client interacts with a group as if it 
 * is one agent, but the group routes locutions to experts. The experts either ignore 
 * the locutions (by skipping their turn using a skip locution) or reply. Replies from 
 * experts are sent back to the client. To the experts, the group is also a single 
 * arguing agent.
 */
public final class COMMADemoDivisionMain extends COMMADemo { 
	
	public final static void main(final String[] args) {
		System.out.println("Starting demo..."); 
		final Platform platform = new Platform();
		final UUID session = UUID.randomUUID();
		
		// Argumentation configuration
        final Map<String, Set<Set<String>>> rules = 
        		RuleParser.parseRules(s -> s, "=>", "QA=>PA;QB=>PB;QA,QB=>PC"); 
        final String[] observable = new String[] {"QA", "~QA", "QB", "~QB"};
        final String[] topics = new String[] {"PC"};
        
		// The group decision logic 
		final AgentSpecification group = (new AgentSpecification())
				.addContext(() -> new BeliefBase(rules, topics, observable, session))
				.addExternalPlanScheme(StartTrigger.class, COMMADemoDivisionMain::start)
				.addExternalPlanScheme(PublishPrompt.class, COMMADemoDivisionMain::publish)
				.addExternalPlanScheme(ArgumentationMessage.class, COMMADemoDivisionMain::decisionLogicLaborDivision);
        final Agent groupAgent = Agent.create(platform, group);
		ids.put("group", groupAgent.getAgentID());
		roles.put(groupAgent.getAgentID(), "group");
		
		// The group members (experts)
		final AgentSpecification expert = (new AgentSpecification())
				.addContext(() -> new BeliefBase(rules, topics, observable, session))
				.addExternalPlanScheme(StartTrigger.class, COMMADemoDivisionMain::start)
				.addExternalPlanScheme(PublishPrompt.class, COMMADemoDivisionMain::publish)
				.addExternalPlanScheme(ArgumentationMessage.class, COMMADemoDivisionMain::decisionLogicExpert);
        final Agent expertAAgent = Agent.create(platform, expert);
        final Agent expertBAgent = Agent.create(platform, expert);
        ids.put("expertA", expertAAgent.getAgentID());
		roles.put(expertAAgent.getAgentID(), "expertA");
        ids.put("expertB", expertBAgent.getAgentID());
		roles.put(expertBAgent.getAgentID(), "expertB");
		
		// The peer to the group (client) 
		final AgentSpecification client = (new AgentSpecification())
				.addContext(() -> new BeliefBase(rules, topics, observable, session))
				.addExternalPlanScheme(StartTrigger.class, COMMADemoDivisionMain::start)
				.addExternalPlanScheme(PublishPrompt.class, COMMADemoDivisionMain::publish)
				.addExternalPlanScheme(ArgumentationMessage.class, COMMADemoDivisionMain::decisionLogicClient);
		final Agent clientAgent = Agent.create(platform, client);
		ids.put("client", clientAgent.getAgentID());
		roles.put(clientAgent.getAgentID(), "client");
		 
        groupAgent.add(new StartTrigger(""));
        expertAAgent.add(new StartTrigger(""));
        expertBAgent.add(new StartTrigger(""));
		clientAgent.add(new StartTrigger("PA"));
		
		try {
			Thread.sleep(500);
			clientAgent.add(new PublishPrompt());
			groupAgent.add(new PublishPrompt());
			expertAAgent.add(new PublishPrompt());
			expertBAgent.add(new PublishPrompt());
			System.out.println("Done");
		} catch (InterruptedException e) { 
			e.printStackTrace();
		}
	} 
	
	/** Some initialization for the demo such as who may speak first. */
	private final static void start(final StartTrigger trigger, final AgentController controller) {
		final BeliefBase belief = controller.getContext(BeliefBase.class);
		final DemoProtocol dialog = belief.getProtocol();
		dialog.finalize(controller.getAgentID());
		if(roles.get(controller.getAgentID()).equals("client")) {
			dialog.getTurnMechanism().addPeer(ids.get("group"), false);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) { 
				e.printStackTrace();
			}
			dialog.send(controller, new ArgumentationMessage<>(
					controller.getAgentID(), 
					dialog.getSessionId(), 
					new Question(trigger.getTopic()), 
					ids.get("group")));
		} else if(roles.get(controller.getAgentID()).equals("group")) {
			for(Entry<String, AgentID> e : ids.entrySet()) {
				if(e.getKey().equals("client")) {
					dialog.getTurnMechanism().addPeer(e.getValue(), true);
				} else if(!e.getKey().equals("group")) {
					dialog.getTurnMechanism().addPeer(e.getValue(), false);
				}
			}
		} else { 
			dialog.getTurnMechanism().addPeer(ids.get("group"), true);
			if(roles.get(controller.getAgentID()).equals("expertA")) {
				belief.addObservation("QA");
			} else {
				belief.addObservation("QB");
			}
			belief.updateArguments();
		}
		
	}

	/** The client tries to get arguments for PA and PB, which it asks from the group.  */
	private final static void decisionLogicClient(final ArgumentationMessage<?> messageRaw, final AgentController controller) {
		outputMessage(messageRaw); 
		final BeliefBase belief = controller.getContext(BeliefBase.class);
		final DemoProtocol dialog = belief.getProtocol();
		if(dialog.isLegallyReceived(messageRaw)) {
			DemoLocution locution = null; 
			if(messageRaw.getContent() instanceof Claim) { 
				locution = new Why(((Claim)messageRaw.getContent()).getProposition());
			} else if(messageRaw.getContent() instanceof Since) {
				final Since since = (Since)messageRaw.getContent();
				for(String proposition : since.getPremises()) {
					if(belief.isObservable(proposition)) {
						belief.addObservation(proposition);
					}
				}
				belief.updateArguments();
				locution = new Concede(since.getConclusion());
			}
			if(locution == null && !belief.hasInArgumentFor("PB"))
				locution = new Question("PB");
			if(locution != null) {
				dialog.send(controller, new ArgumentationMessage<>(
						controller.getAgentID(), 
						dialog.getSessionId(), 
						locution, 
						ids.get("group"))); 
			}
		}
		
	}
	
	/** The experts are triggered by questions on their expertise and challenges. They reply with 
	 * claims and since locutions. */
	private final static void decisionLogicExpert(final ArgumentationMessage<?> messageRaw, final AgentController controller) {
		outputMessage(messageRaw); 
		final BeliefBase belief = controller.getContext(BeliefBase.class);
		final DemoProtocol dialog = belief.getProtocol();
		if(dialog.isLegallyReceived(messageRaw)) {
			DemoLocution locution = new Skip();
			final Map<Argument<String>, Label> arguments = belief.getArguments();
			if(messageRaw.getContent() instanceof Question) {
				// If has IN for argument: return claim x
				final String proposition = ((Question)messageRaw.getContent()).getProposition();
				if(belief.hasInArgumentFor(proposition))
					locution = new Claim(proposition); 
			} else if(messageRaw.getContent() instanceof Why) {
				final String proposition = ((Why)messageRaw.getContent()).getProposition();
				for(Entry<Argument<String>, Label> e : arguments.entrySet()) { 
					if(e.getValue() == Label.IN && e.getKey().getConclusion().equals(proposition)) {
						locution = new Since(
								e.getKey().getConclusion(), 
								e.getKey().getPremises().stream()
								.map(a -> a.getConclusion())
								.collect(Collectors.toList())
								.toArray(new String[e.getKey().getPremises().size()])); 
						break;
					}
				}
			}
			dialog.send(controller, new ArgumentationMessage<>(
					controller.getAgentID(), 
					dialog.getSessionId(), 
					locution, 
					ids.get("group"))); 
		} 
	}

	/** The group node routes all messages from the non expert to the experts and all messages 
	 * from the experts to the client. */
	private final static void decisionLogicLaborDivision(final ArgumentationMessage<?> messageRaw, final AgentController controller) {
		outputMessage(messageRaw);
		final AgentID sender = messageRaw.getSender();
		final BeliefBase belief = controller.getContext(BeliefBase.class);
		final DemoProtocol dialog = belief.getProtocol();
		if(dialog.isLegallyReceived(messageRaw)) {
			if(roles.get(sender).equals("client")) {
				// Send to experts
				for(String expert : new String[] {"expertA","expertB"})
					dialog.send(controller, new ArgumentationMessage<>(
							controller.getAgentID(), 
							dialog.getSessionId(), 
							messageRaw.getContent(), 
							ids.get(expert)));
				if(messageRaw.getContent() instanceof Concede)
					dialog.send(controller, new ArgumentationMessage<>(
							controller.getAgentID(), 
							dialog.getSessionId(), 
							new Skip(), 
							ids.get("client")));
			} else {
				// Send to client if not skip
				if(!(messageRaw.getContent() instanceof Skip)) { 
					dialog.send(controller, new ArgumentationMessage<>(
							controller.getAgentID(), 
							dialog.getSessionId(), 
							messageRaw.getContent(), 
							ids.get("client")));
				}
			}
		} 
	}
}
