package main;

import org.hal.core.agent.Trigger;
/** Trigger to start the demo. */
public final class StartTrigger implements Trigger {
	private final String topic;
	
	public StartTrigger(final String topic) {
		this.topic = topic;
	}
	
	public final String getTopic() { return this.topic; }
}
