package main;

import org.hal.core.agent.Trigger;

import argumentationProtocols.graphs.DialogGraph;
/** Delivery of a dialog graph from one agent to another. */
public final class ShareDialogueGraphTrigger implements Trigger {
	private final DialogGraph graph;
	
	public ShareDialogueGraphTrigger(final DialogGraph dialogGraph) {
		this.graph = dialogGraph;
	}
	
	public final DialogGraph getGraph() { return this.graph; }
}
