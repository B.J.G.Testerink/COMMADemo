package main;
 
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.hal.core.agent.Agent;
import org.hal.core.agent.AgentController;
import org.hal.core.agent.AgentID;
import org.hal.core.agent.AgentSpecification;
import org.hal.core.agent.Platform;
import org.politie.nl.argumentation.andorgraph.Argument;
import org.politie.nl.argumentation.andorgraph.RuleParser;
import org.politie.nl.argumentation.dunglabelling.Label;
 
import argumentationProtocols.triggers.ArgumentationMessage;
import argumentationProtocols.triggers.PublishPrompt;
import demoProtocol.DemoProtocol;
import demoProtocol.locutions.Claim;
import demoProtocol.locutions.Concede;
import demoProtocol.locutions.DemoLocution;
import demoProtocol.locutions.Question;
import demoProtocol.locutions.Since;
import demoProtocol.locutions.Skip;
import demoProtocol.locutions.Why;
/** 
 * This demo shows an example of how a group node can be used by a client agent to find an expert.
 * The client submits its locutions to the group which routes them to the experts. The experts 
 * directly respond to the client. Hence, the group can see what kind of actions were asked but 
 * not how they were answered.
 *  */
public final class COMMADemoYellowPageMain extends COMMADemo { 
	private static String outputFile = "./visualizer/js/dialogGraphYellowPage.js"; 
	
	public final static void main(final String[] args) {
		System.out.println("Starting demo...");
		COMMADemo.outputFilePath = outputFile;
		COMMADemo.outputGraphName = "YellowPage";
		final Platform platform = new Platform();
		final UUID session = UUID.randomUUID();
		
		// Argumentation configuration
        final Map<String, Set<Set<String>>> rules = 
        		RuleParser.parseRules(s -> s, "=>", "QA=>PA;QB=>PB;QA,QB=>PC"); 
        final String[] observable = new String[] {"QA", "~QA", "QB", "~QB"};
        final String[] topics = new String[] {"PC"};
        
		// The group decision logic 
		final AgentSpecification group = (new AgentSpecification())
				.addContext(() -> new BeliefBase(rules, topics, observable, session))
				.addExternalPlanScheme(StartTrigger.class, COMMADemoYellowPageMain::start)
				.addExternalPlanScheme(PublishPrompt.class, COMMADemoYellowPageMain::publish)
				.addExternalPlanScheme(ArgumentationMessage.class, COMMADemoYellowPageMain::decisionLogicYellowPage);
        final Agent groupAgent = Agent.create(platform, group);
		ids.put("group", groupAgent.getAgentID());
		roles.put(groupAgent.getAgentID(), "group");
		
		// The group members (experts)
		final AgentSpecification expert = (new AgentSpecification())
				.addContext(() -> new BeliefBase(rules, topics, observable, session))
				.addExternalPlanScheme(StartTrigger.class, COMMADemoYellowPageMain::start)
				.addExternalPlanScheme(PublishPrompt.class, COMMADemoYellowPageMain::publish)
				.addExternalPlanScheme(ArgumentationMessage.class, COMMADemoYellowPageMain::decisionLogicExpert);
        final Agent expertAAgent = Agent.create(platform, expert);
        final Agent expertBAgent = Agent.create(platform, expert);
        ids.put("expertA", expertAAgent.getAgentID());
		roles.put(expertAAgent.getAgentID(), "expertA");
        ids.put("expertB", expertBAgent.getAgentID());
		roles.put(expertBAgent.getAgentID(), "expertB");
		
		// The peer to the group (client) 
		final AgentSpecification client = (new AgentSpecification())
				.addContext(() -> new BeliefBase(rules, topics, observable, session))
				.addExternalPlanScheme(StartTrigger.class, COMMADemoYellowPageMain::start)
				.addExternalPlanScheme(PublishPrompt.class, COMMADemoYellowPageMain::publish)
				.addExternalPlanScheme(ArgumentationMessage.class, COMMADemoYellowPageMain::decisionLogicClient);
		final Agent clientAgent = Agent.create(platform, client);
		ids.put("client", clientAgent.getAgentID());
		roles.put(clientAgent.getAgentID(), "client");
		 
        groupAgent.add(new StartTrigger(""));
        expertAAgent.add(new StartTrigger(""));
        expertBAgent.add(new StartTrigger(""));
		clientAgent.add(new StartTrigger("PA"));
		
		try {
			Thread.sleep(500);
			clientAgent.add(new PublishPrompt());
			groupAgent.add(new PublishPrompt());
			expertAAgent.add(new PublishPrompt());
			expertBAgent.add(new PublishPrompt());
			System.out.println("Done");
		} catch (InterruptedException e) { 
			e.printStackTrace();
		}
	} 

	/** Some initialization for the demo such as who may speak first. */
	private final static void start(final StartTrigger trigger, final AgentController controller) {
		final BeliefBase belief = controller.getContext(BeliefBase.class);
		final DemoProtocol dialog = belief.getProtocol();
		dialog.finalize(controller.getAgentID());
		if(roles.get(controller.getAgentID()).equals("client")) {
			dialog.getTurnMechanism().addPeer(ids.get("group"), false);
			dialog.getTurnMechanism().addPeer(ids.get("expertA"), true);
			dialog.getTurnMechanism().addPeer(ids.get("expertB"), true);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) { 
				e.printStackTrace();
			}
			dialog.send(controller, new ArgumentationMessage<>(
					controller.getAgentID(), 
					dialog.getSessionId(), 
					new Question(trigger.getTopic()), 
					ids.get("group")));
		} else if(roles.get(controller.getAgentID()).equals("group")) { 
			dialog.getTurnMechanism().addPeer(ids.get("client"), true);
			dialog.getTurnMechanism().addPeer(ids.get("expertA"), false);
			dialog.getTurnMechanism().addPeer(ids.get("expertB"), false);
		} else { 
			dialog.getTurnMechanism().addPeer(ids.get("group"), true);
			dialog.getTurnMechanism().addPeer(ids.get("client"), false);
			if(roles.get(controller.getAgentID()).equals("expertA")) {
				belief.addObservation("QA");
			} else {
				belief.addObservation("QB");
			}
			belief.updateArguments();
		}
		
	}
	
	/** The client submits questions to the group but direct arguing reponses to the experts. */
	private final static void decisionLogicClient(final ArgumentationMessage<?> messageRaw, final AgentController controller) {
		outputMessage(messageRaw); 
		final BeliefBase belief = controller.getContext(BeliefBase.class);
		final DemoProtocol dialog = belief.getProtocol();
		if(dialog.isLegallyReceived(messageRaw)) {
			DemoLocution locution = null;
			AgentID recipient = messageRaw.getSender();
			if(messageRaw.getContent() instanceof Claim) { 
				locution = new Why(((Claim)messageRaw.getContent()).getProposition());
			} else if(messageRaw.getContent() instanceof Since) {
				final Since since = (Since)messageRaw.getContent();
				for(String proposition : since.getPremises()) {
					if(belief.isObservable(proposition)) {
						belief.addObservation(proposition);
					}
				}
				belief.updateArguments();
				locution = new Concede(since.getConclusion());
			}
			if(!ids.get("group").equals(messageRaw.getSender())
					&& locution == null 
					&& !belief.hasInArgumentFor("PB")) {
				locution = new Question("PB");
				recipient = ids.get("group");
			}
			if(locution != null) {
				dialog.send(controller, new ArgumentationMessage<>(
						controller.getAgentID(), 
						dialog.getSessionId(), 
						locution, 
						recipient)); 
			}
		}
		
	}
	
	/** The expert always skips its turns regarding the group but may reply to the client if it can, given its belief base. */
	private final static void decisionLogicExpert(final ArgumentationMessage<?> messageRaw, final AgentController controller) {
		outputMessage(messageRaw); 
		final BeliefBase belief = controller.getContext(BeliefBase.class);
		final DemoProtocol dialog = belief.getProtocol();
		if(dialog.isLegallyReceived(messageRaw)) {
			if(roles.get(messageRaw.getSender()).equals("group")) { 
				dialog.send(controller, new ArgumentationMessage<>(
						controller.getAgentID(), 
						dialog.getSessionId(), 
						new Skip(), 
						ids.get("group"))); 
			}
			DemoLocution locution = null;
			final Map<Argument<String>, Label> arguments = belief.getArguments();
			if(messageRaw.getContent() instanceof Question) {
				// If has IN for argument: return claim x
				final String proposition = ((Question)messageRaw.getContent()).getProposition();
				if(belief.hasInArgumentFor(proposition))
					locution = new Claim(proposition); 
			} else if(messageRaw.getContent() instanceof Why) {
				final String proposition = ((Why)messageRaw.getContent()).getProposition();
				for(Entry<Argument<String>, Label> e : arguments.entrySet()) { 
					if(e.getValue() == Label.IN && e.getKey().getConclusion().equals(proposition)) {
						locution = new Since(
								e.getKey().getConclusion(), 
								e.getKey().getPremises().stream()
								.map(a -> a.getConclusion())
								.collect(Collectors.toList())
								.toArray(new String[e.getKey().getPremises().size()])); 
						break;
					}
				}
			} else if(messageRaw.getContent() instanceof Concede) {
				locution = new Skip();
			}
			if(locution != null)
				dialog.send(controller, new ArgumentationMessage<>(
						controller.getAgentID(), 
						dialog.getSessionId(), 
						locution, 
						ids.get("client"))); 
		} 
	}

	/** The group simply broadcasts all client messages to the experts and skips its turns with the client. */
	private final static void decisionLogicYellowPage(final ArgumentationMessage<?> messageRaw, final AgentController controller) {
		outputMessage(messageRaw);
		final AgentID sender = messageRaw.getSender();
		final BeliefBase belief = controller.getContext(BeliefBase.class);
		final DemoProtocol dialog = belief.getProtocol();
		if(dialog.isLegallyReceived(messageRaw)) {
			if(roles.get(sender).equals("client")) {
				// Send to experts
				for(String expert : new String[] {"expertA","expertB"})
					dialog.send(controller, new ArgumentationMessage<>(
							controller.getAgentID(), 
							dialog.getSessionId(), 
							messageRaw.getContent(), 
							ids.get(expert))); 
				dialog.send(controller, new ArgumentationMessage<>(
						controller.getAgentID(), 
						dialog.getSessionId(), 
						new Skip(), 
						ids.get("client")));
			}
		}
	} 
}
