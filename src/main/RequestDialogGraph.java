package main;

import org.hal.core.agent.AgentID;
import org.hal.core.agent.Trigger;
/** Signal for an agent to give its dialog graph. */
public final class RequestDialogGraph implements Trigger {
	private final AgentID deliverAddress;
	
	public RequestDialogGraph(final AgentID deliverAddress) {
		this.deliverAddress = deliverAddress;
	}
	
	public final AgentID getDeliverAddress() { return this.deliverAddress; }
}
