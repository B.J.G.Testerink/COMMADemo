package argumentationProtocols;
 
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.hal.core.agent.AgentID;
 

/**
 * With this turn mechanism an agent maintains a strict turn order with any of its peers. 
 * It does so by using the norm that the one who send the session invitation is the 
 * starting agent. 
 * 
 * @author Bas Testerink
 */
public final class StrictP2PTurnMechanism {
	// Maintenance of who is allowed to send to whom
	private final Map<AgentID, Boolean> allowedToSendToMe;
	private final Map<AgentID, Boolean> allowedToReceiveFromMe;
	private final AgentID self;
	 
	public StrictP2PTurnMechanism(final AgentID self) {  
		this.allowedToSendToMe = new HashMap<>();
		this.allowedToReceiveFromMe = new HashMap<>();
		this.self = self;
	}
	
	/** Returns true iff the sender is allowed to send a message to the receiver. */
	public final boolean allowedToSendTo(final AgentID sender, final AgentID receiver){
		if(sender == null || receiver == null) return false;
		if(sender.equals(this.self)) return this.allowedToReceiveFromMe.get(receiver);
		else return this.allowedToSendToMe.get(sender);
	}
	
	/** Handle the fact that a message was sent by the sender to the receiver. This method does not check 
	 * whether the sender was allowed to send. */
	public final void sentMessage(final AgentID sender, final AgentID receiver){
		if(sender == null || receiver == null) return;
		// Flip the booleans
		if(sender.equals(this.self)){
			this.allowedToReceiveFromMe.put(receiver, !this.allowedToReceiveFromMe.get(receiver));
			this.allowedToSendToMe.put(receiver, !this.allowedToSendToMe.get(receiver));
		} else { 
			this.allowedToReceiveFromMe.put(sender, !this.allowedToReceiveFromMe.get(sender));
			this.allowedToSendToMe.put(sender, !this.allowedToSendToMe.get(sender));
		} 
	}
	
	/** Add a peer to the mechanism. In this mechanism the agent that invited the owner of this mechanism (which 
	 * can be the owner itself) is the starting agent. */
	public final void addPeer(final AgentID peer, boolean invitedMe){
		// Only the first time that the peer is added we store the starting player. Hence, if A invites B and B invites A simultaneously, then 
		// it is possible that both agents consider themselves to be the starting player
		if(this.allowedToSendToMe.get(peer) == null){
			this.allowedToReceiveFromMe.put(peer, !invitedMe);
			this.allowedToSendToMe.put(peer, invitedMe);
		}
	}
	
	/** Returns all the peers of the owner of this mechanism. */
	public final Set<AgentID> getAllConnectedPeers(){
		final Set<AgentID> result = new HashSet<>(this.allowedToSendToMe.keySet());
		result.add(this.self);
		return result;
	}
}
