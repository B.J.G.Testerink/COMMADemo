package argumentationProtocols;

import java.util.List;

import argumentationProtocols.triggers.ArgumentationMessage; 
/**
 * 
 * The instantiate method of the interceptor immediately updates the status of the session, i.e., 
 * changes the argumentation graphs. This is a choice that breaks the norm of not executing 'plan' 
 * decision/business logic in the instantiate method. However, since multiple messages might be 
 * simultaneously in the queue, we need to update the session state, otherwise we cannot check 
 * the legality of the messages in the queue. 
 * 
 * @author Bas Testerink
 */
public final class MessageInterceptor {
	private final StrictP2PTurnMechanism turnMechanism;
	private final List<Template<?>> templates;
	
	public MessageInterceptor(final StrictP2PTurnMechanism turnMechanism, final List<Template<?>> templates) {
		this.turnMechanism = turnMechanism;
		this.templates = templates;
	}
 
	/** The interceptor only triggers for argumentation messages. When it triggers, then it will initially flag the 
	 * message to illegal, until proven otherwise. First it checks the turn mechanism, and then tries to apply the 
	 * templates. The first template that is applicable is applied and no other templates. */
	public final ArgumentationMessage<?> check(final ArgumentationMessage<?> message){
		// By default set the message to non-legal
		message.setLegal(false); 
		
		// Check the turn status, if the message is out-of-turn, then return a plan to send the sender a 
		// notification that its message is bounced
		if(!this.turnMechanism.allowedToSendTo(message.getSender(), message.getReceiver())) {
			return message;
		} 
 
		// Go through the templates to see if any allows the message
		for(Template<?> template : this.templates){
			if(template.apply(message)){
				message.setLegal(true);
				// Process that you received successfully a message from the sender
				this.turnMechanism.sentMessage(message.getSender(), message.getReceiver());
				return message;
			}
		} 
		return message;
	} 
}
