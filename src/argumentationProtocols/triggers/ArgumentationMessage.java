package argumentationProtocols.triggers;

import java.util.UUID;

import org.hal.core.agent.AgentID;
import org.hal.core.agent.Trigger;

/**
 * Trigger that carries the message in an argumentation dialog. These kind of messages 
 * can be flagged to be legal or illegal so that agents can be made aware of when 
 * the protocol of the dialog has been violated.
 * 
 * @author Bas Testerink
 * 
 * @param <Content>
 */
public final class ArgumentationMessage<Content> implements Trigger {
	private final AgentID sender, receiver;
	private final UUID sessionId;
	private final Content content;
	private boolean legal = false;
	
	public ArgumentationMessage(final AgentID sender, final UUID sessionId, final Content content, final AgentID receiver){
		this.sender = sender;
		this.sessionId = sessionId; 
		this.content = content;
		this.receiver = receiver;
	}
	
	public final AgentID getSender(){ return this.sender; }
	public final UUID getSessionId(){ return this.sessionId; }
	public final Content getContent(){ return this.content; }
	public final AgentID getReceiver(){ return this.receiver; }
	
	/** Whether the protocol that governs the session allows the message. */
	public final boolean isLegal(){ return this.legal; }
	public final void setLegal(final boolean b){ this.legal = b; }
}
