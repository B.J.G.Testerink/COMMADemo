package argumentationProtocols.triggers;

import java.util.UUID;

import org.hal.core.agent.AgentID;
import org.hal.core.agent.Trigger;

/**
 * When an agent receives a message that breaks the protocol according to the receiver, 
 * then the receiver automatically sends a bounce message to the sender of the illegal 
 * message.
 * 
 * @author Bas Testerink
 */
public final class BounceMessage implements Trigger {  
	public static enum BounceReason{ NOT_YOUR_TURN, NO_APPLICABLE_TEMPLATE }
	private final UUID sessionId;
	private final AgentID sender;
	private final ArgumentationMessage<?> bouncedMessage;
	private final BounceReason reason;
	
	public BounceMessage(final UUID sessionId, final AgentID sender, 
			final ArgumentationMessage<?> bouncedMessage, final BounceReason reason){
		this.sessionId = sessionId;
		this.sender = sender;
		this.bouncedMessage = bouncedMessage;
		this.reason = reason;
	}
	
	public final UUID getSessionId(){ return this.sessionId; }
	public final AgentID getSender(){ return this.sender; }
	public final ArgumentationMessage<?> getBouncedMessage(){ return this.bouncedMessage; }
	public final BounceReason getReason(){ return this.reason; }
}	
