package argumentationProtocols.triggers;

import java.util.UUID;

import org.hal.core.agent.AgentID;
import org.hal.core.agent.Trigger;

public final class OpeningMove implements Trigger{
	private final UUID session;
	private final AgentID peer;
	
	public OpeningMove(final UUID session, final AgentID peer) {
		this.session = session;
		this.peer = peer;
	}
	
	public final UUID getSession() {
		return this.session;
	}
	
	public final AgentID getPeer() {
		return this.peer;
	}
}
