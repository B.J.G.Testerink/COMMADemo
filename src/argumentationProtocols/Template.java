package argumentationProtocols;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;

import argumentationProtocols.graphs.Argument2Node;
import argumentationProtocols.graphs.DialogGraph;
import argumentationProtocols.graphs.Node;
import argumentationProtocols.triggers.ArgumentationMessage;
/**
 * A template is a construct that can check whether a message is allowed to be sent/received 
 * and when it is sent/received, how it relates to previous messages, and how it updates the 
 * argument 1 graph part of the dialog graph. 
 * 
 * @author Bas Testerink
 *
 * @param <ContentClass>
 */
public final class Template<ContentClass> { 
	private DialogGraph dialogGraph;
	private final Class<ContentClass> klass;
	private final BiPredicate<DialogGraph, Argument2Node<ContentClass>> isAllowed;
	private final BiFunction<DialogGraph, Argument2Node<ContentClass>, List<Node>> responseRelation;
	private final BiConsumer<DialogGraph, Argument2Node<ContentClass>> argument1Update;
	
	public Template( 
			final Class<ContentClass> klass,
			final BiPredicate<DialogGraph, Argument2Node<ContentClass>> isAllowed,
			final BiFunction<DialogGraph, Argument2Node<ContentClass>, List<Node>> responseRelation,
			final BiConsumer<DialogGraph, Argument2Node<ContentClass>> argument1Update){ 
		this.klass = klass;
		this.isAllowed = isAllowed;
		this.responseRelation = responseRelation;
		this.argument1Update = argument1Update;
	}
	
	public final void setDialogGraph(final DialogGraph dialogGraph){
		this.dialogGraph = dialogGraph;
	}
	
	/**
	 * Applies the template to the message. Returns true if the template was applicable for 
	 * the message. 
	 * @param message
	 * @return
	 */
	public final boolean apply(final ArgumentationMessage<?> message) {
		if(this.klass.isInstance(message.getContent())){
			// Add arg2 node and check if this template allows the message
			@SuppressWarnings("unchecked")
			final ContentClass content = (ContentClass) message.getContent();
			final Argument2Node<ContentClass> messageNode = new Argument2Node<>(message.getSender(), content, message.getReceiver());
			if(!this.isAllowed.test(this.dialogGraph, messageNode))
				return false;
			this.dialogGraph.addNode(messageNode);  
			// Apply argument 2 response relation to obtain the coherence between the message and the rest of the dialog
			final List<Node> responseToNodes = this.responseRelation.apply(this.dialogGraph, messageNode);
			for(Node responseTo : responseToNodes)
				this.dialogGraph.connect(messageNode, responseTo); 
			
			// Start recording the changes that the message makes
			this.dialogGraph.startRecordingChanges();
			// Apply argument 1 update method, TODO: ensure that this method ONLY changes the arg1 subnetwork
			this.argument1Update.accept(this.dialogGraph, messageNode);
			// Store the recorded changes as an effect node
			this.dialogGraph.stopRecordingChanges();
			// Template successfully applied!
			return true;
		} else return false;
	}
}
