package argumentationProtocols.graphs;

import org.hal.core.agent.AgentID;

/**
 * A commitment node is part of a linked list that maintains the status of whether an agent is committed 
 * or not to a specific i node in the dialog.
 * 
 * @author Bas Testerink
 *
 */
public class CommitmentNode extends Node {
	private final AgentID committedAgent; 
	private final Node parent;
	
	public CommitmentNode(final AgentID committedAgent, final Node parent){
		this.committedAgent = committedAgent;
		this.parent = parent;
	}
	

	public final AgentID getCommittedAgent(){ return this.committedAgent; } 
	public final Node getParent(){ return this.parent; } 
}
