package argumentationProtocols.graphs;
/**
 * A rule application node is a kind of s node that is used to connect premise nodes with a conclusion.
 * 
 * @author Bas Testerink
 *
 */
public class RuleApplicationNode extends SNode {
	private final Node conclusion;
	private final Node[] minorPremises;
	
	public RuleApplicationNode(final Node[] minorPremises, final Node conclusion){
		this.minorPremises = minorPremises;
		this.conclusion = conclusion;
	}
	
	public boolean equals(final Object other){
		if(other == null) return false;
		if(other == this) return true;
		if(other instanceof RuleApplicationNode){
			final RuleApplicationNode otherNode = (RuleApplicationNode) other;
			if(otherNode.minorPremises.length != this.minorPremises.length || otherNode.conclusion != this.conclusion)
				return false;
			for(Node myMinorPremise : this.minorPremises){
				boolean found = false;
				for(Node otherMinorPremise : otherNode.minorPremises){
					if(otherMinorPremise == myMinorPremise){
						found = true;
						break;
					}
				}
				if(!found) return false;
			} 
			return true;
		}
		return false;
	}
}
