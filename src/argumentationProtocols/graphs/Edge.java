package argumentationProtocols.graphs;
/**
 * Standard directed edge among two nodes.
 *
 * @author Bas Testerink
 *
 */
public final class Edge {
	private final Node start, end;
	
	public Edge(final Node start, final Node end){
		this.start = start;
		this.end = end;
	}
	
	public final Node getStart(){ return this.start; }
	public final Node getEnd(){ return this.end; } 
}
