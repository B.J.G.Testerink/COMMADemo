package argumentationProtocols.graphs;

import org.hal.core.agent.AgentID;

/**
 * An argument 2 node stores a locution that was made during the dialog.
 * 
 * @author Bas Testerink
 * @param <Content>
 */
public class Argument2Node<Content> extends Node {
	private final AgentID sender, receiver;
	private final Content content; 
	
	public Argument2Node(final AgentID sender, final Content content, final AgentID receiver){
		this.sender = sender;
		this.content = content;
		this.receiver = receiver;
	}

	public final AgentID getSender(){ return this.sender; }
	public final Content getContent(){ return this.content; }
	public final AgentID getReceiver(){ return this.receiver; }
	
}
