package argumentationProtocols.graphs;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.hal.core.agent.AgentID;
import org.hal.core.agent.Trigger;

import argumentationProtocols.triggers.ArgumentationSendError;
import argumentationProtocols.triggers.BounceMessage; 
/**
 * A dialog graph captures the status of an argumentation dialog. On an abstract level it is just a 
 * graph. However, we often divided in subgraphs when processing the data, such as the argument 2 and 1 
 * graphs and the locution coherence graph. 
 * 
 * This graph is automatically updated when agents execute dialogs. The updates themselves are specified 
 * by the protocol templates that govern the interaction.
 * 
 * @author Bas Testerink
 *
 */
public class DialogGraph {
	private final AgentID owner;
	protected final List<Node> nodes;
	private List<Node> newNodes;
	private List<Edge> newEdges;
	private boolean isRecording;
	private Optional<Argument2Node<?>> lastMove;
	protected Optional<Trigger> error;
	
	public DialogGraph(final AgentID owner){
		this.owner = owner;
		this.nodes = new ArrayList<>();
		this.lastMove = Optional.empty();
		this.error = Optional.empty();
		this.isRecording = false;
	}
	
	/** Add a node. Will remember that the node was added if the graph is recording. */
	public final void addNode(final Node node){
		this.nodes.add(node);
		if(this.isRecording)
			this.newNodes.add(node);
		if(node instanceof Argument2Node<?>)
			this.lastMove = Optional.of((Argument2Node<?>) node);
	}

	/** Add a directed edge. Will remember that the edge was added if the graph is recording. */
	public final void connect(final Node a, final Node b){
		final Edge edge = a.connectTo(b);
		if(this.isRecording)
			this.newEdges.add(edge);
	}
	
	/** Start recording the changes made to the graph (addition of nodes and edges). */
	public final void startRecordingChanges(){
		if(this.lastMove.isPresent()){ // If there is no last move, then there is no move to record the effects of
			this.isRecording = true;
			this.newNodes = new ArrayList<>();
			this.newEdges = new ArrayList<>();
		}
	}
	
	/** Stop recording the changes that are made to the graph. When called, this method will automatically make an 
	 * effect node and attach it to the latest move. */
	public final void stopRecordingChanges(){
		if(this.lastMove.isPresent()){ // If there is no last move, then there is no move to record the effects of
			this.isRecording = false;
			final EffectNode effect = new EffectNode(this.lastMove.get(), this.newNodes, this.newEdges); 
			this.nodes.add(effect);
			connect(this.lastMove.get(), effect);
		}
	}
	
	/** To be overridden, exports this graph as a JSON object. */
	public StringBuffer toJSON(final Map<AgentID,String> peers){
		return new StringBuffer("{}");
	}
	
	/** Remember that an error occurred. */
	public final void setError(final Trigger error){
		if(error != null && (error instanceof BounceMessage || error instanceof ArgumentationSendError)){
			this.error = Optional.of(error);
		}
	}
	
	// Getters
	public final AgentID getOwner(){ return this.owner; }
	public final Optional<Argument2Node<?>> getLastMove(){ return this.lastMove; }
	public final Optional<Trigger> getError(){ return this.error; }
}
