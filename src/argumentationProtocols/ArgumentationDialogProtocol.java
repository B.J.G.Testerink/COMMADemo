package argumentationProtocols;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.hal.core.agent.AgentController;
import org.hal.core.agent.AgentID;

import argumentationProtocols.graphs.DialogGraph;
import argumentationProtocols.triggers.ArgumentationMessage;
import argumentationProtocols.triggers.ArgumentationSendError;
import argumentationProtocols.triggers.ArgumentationSendError.ErrorReason;
  
/**
 * A protocol more or less consists of the receive and send templates that tell 
 * how to interpret locution for this protocol. The templates update the dialog 
 * graph which is also stored in the protocol instance. 
 * 
 * We store alongside the graph a turn mechanism separately. The mechanism could 
 * be stored as part of the graph but that would be unnecessarily complicating 
 * the system architecture. The specification and maintenance of turn semantics 
 * will in the future be revisited.
 * 
 * Finally, we add a message interceptor to the agent that automatically checks 
 * all the receive templates whenever an argumentation messages arrives. 
 * 
 * @author Bas Testerink
 *
 * @param <Locution>
 */
public abstract class ArgumentationDialogProtocol<Locution> {
	private final UUID sessionId;
	private final List<Template<?>> receiveTemplates, sendTemplates;
	private StrictP2PTurnMechanism turnMechanism;
	private DialogGraph dialogGraph; 
	private MessageInterceptor interceptor;
	
	public ArgumentationDialogProtocol(final UUID sessionId){
		this.sessionId = sessionId;
		this.receiveTemplates = new ArrayList<>();
		this.sendTemplates = new ArrayList<>();
	}  
 
	/** Make this protocol ready to use by making the dialog graph, setting 
	 * this graph in the templates, making the turn mechanism and finally 
	 * adopting the message interceptor. */
	public final void finalize(final AgentID owner){
		this.dialogGraph = makeDialogGraph(owner);
		for(Template<?> template : this.receiveTemplates)
			template.setDialogGraph(this.dialogGraph);
		for(Template<?> template : this.sendTemplates)
			template.setDialogGraph(this.dialogGraph); 
		this.turnMechanism = new StrictP2PTurnMechanism(owner); 
		this.interceptor = new MessageInterceptor(this.turnMechanism, this.receiveTemplates);
	}
	
	/** Finalize the protocol and adopt the message interceptor that checks incoming messages. This method 
	 * ought to be called before the agent responds to an invitation to join an argumentation session. */
	public final static void initiateArgumentationDialog(
			final AgentController planInterface,  
			final ArgumentationDialogProtocol<?> protocol){
		protocol.finalize(planInterface.getAgentID());   
	}
	
	/** An argumentation agent can use this send method to send an argumentation message which is then immediately 
	 * checked against the protocol. If the message is illegal, then the agent will adopt an internal trigger 
	 * (an <code>ArgumentationSendError</code>). */
	public final void send(final AgentController controller, final ArgumentationMessage<?> message){ 
		// First check if the turn mechanism allows the agent's move
		if(!this.turnMechanism.allowedToSendTo(controller.getAgentID(), message.getReceiver())){ 
			controller.addInternalTrigger(new ArgumentationSendError(this.sessionId, controller.getAgentID(),message,ErrorReason.NOT_MY_TURN)); 
		} else {
			// Check if there is a send template that allows the agent's move
			for(Template<?> template : sendTemplates){
				if(template.apply(message)){ 
					// Successfully sent the message
					this.turnMechanism.sentMessage(controller.getAgentID(), message.getReceiver());  
					controller.send(message.getReceiver(), message); 
					return;
				}
			}
			
			// If there were not applicable templates, then the agent notifies itself that there was an error
			controller.addInternalTrigger(new ArgumentationSendError(this.sessionId, controller.getAgentID(),message,ErrorReason.NO_APPLICABLE_TEMPLATE));
		} 
	} 
	
	/** Make a new dialog graph with the given agent as the owner. */
	protected abstract DialogGraph makeDialogGraph(final AgentID owner);
	
	/** Add a template that filters incoming messages. */
	public final void addReceiveTemplate(final Template<?> template){
		this.receiveTemplates.add(template); 
	}
	
	/** Add a template that filters outgoing messages. */
	public final void addSendTemplate(final Template<?> template){
		this.sendTemplates.add(template);
	}
 
	// Getters
	public final boolean isLegallyReceived(final ArgumentationMessage<?> message) { return this.interceptor.check(message).isLegal(); }
	public final DialogGraph getDialogGraph(){ return this.dialogGraph; }
	public final StrictP2PTurnMechanism getTurnMechanism(){ return this.turnMechanism; }
	public final List<Template<?>> getSendTemplates(){ return this.sendTemplates; }
	public final List<Template<?>> getReceiveTemplates(){ return this.receiveTemplates; }
	public final UUID getSessionId(){ return this.sessionId; }
	
}
