package org.hal.core.agent;

import java.util.function.BiConsumer; 
/**
 * This class specifies an interface between the execution of a plan and the 
 * state of an agent.
 *
 */
public final class AgentController {
	private final AgentState state;
	private final BiConsumer<AgentID, Trigger> messenger;
	
	AgentController(final AgentState state, final BiConsumer<AgentID, Trigger> messenger) {
		this.state = state;
		this.messenger = messenger;
	} 
	
	/** Add an internal trigger that is to be processed later. */
	public final void addInternalTrigger(final Trigger trigger) {
		this.state.addInternalTrigger(trigger);
	}
	
	/** Get the ID of the agent. */
	public final AgentID getAgentID() { 
		return state.getAgentID();
	}
	
	/** Obtain the context for the given class. Note: returns null if there is no context of the 
	 * given class. */
	public final <T extends Context> T getContext(final Class<T> klass) {
		return this.state.getContext(klass);
	}
	
	/** Send a trigger to another agent. Will be received as an external trigger
	 * (hence the agent can also use this to send an external trigger to itself). NOTE: 
	 * this method can only be used for sending a trigger to another agent on the 
	 * same platform. */
	public final void send(final AgentID receiver, final Trigger trigger) {
		this.messenger.accept(receiver, trigger);
	}
	
	/** Kill the agent. */
	public final void kill() {
		this.state.kill();
	}
}
