package org.hal.core.agent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Supplier;
/**
 * An <code>AgentSpecification</code> is to an agent what a class is to an object. 
 * When you program an agent, you first program its specification (by extending this class) and use that to 
 * create agents. The same specification can be reused indefinitely. 
 */
public class AgentSpecification {
	/** Default deliberation step for processing external triggers. */
	public final static Consumer<AgentState> APPLY_EXTERNAL_SCHEMES = state -> {
		for(Trigger trigger : state.getAndRemoveExternalTriggers())
			for(BiPredicate<Trigger, AgentController> scheme : state.getExternalPlanSchemes())
				if(scheme.test(trigger, state.getAgentController()))
					break;
	};

	/** Default deliberation step for processing internal triggers. */
	public final static Consumer<AgentState> APPLY_INTERNAL_SCHEMES = state -> {
		for(Trigger trigger : state.getAndRemoveInternalTriggers())
			for(BiPredicate<Trigger, AgentController> scheme : state.getInternalPlanSchemes())
				if(scheme.test(trigger, state.getAgentController()))
					break;
	};
	
	/** The default deliberation cycle. Will go through the external triggers, apply schemes until one 
	 * returns true for each trigger, and then repeat the same for the internal triggers. */
	public final static List<Consumer<AgentState>> DEFAULT_DELIBERATION_CYCLE = 
			Collections.unmodifiableList(Arrays.asList(APPLY_EXTERNAL_SCHEMES, APPLY_INTERNAL_SCHEMES));
	
	/** Schemes for processing triggers. */
	private final List<BiPredicate<Trigger, AgentController>> externalSchemes, internalSchemes; 
	
	/** Context suppliers for storing data that is used in the agent's decision logic. */
	private final List<Supplier<Context>> contexts;
	
	/** The deliberation cycle of the agent. */
	private final List<Consumer<AgentState>> deliberationCycle;  
	
	public AgentSpecification() {
		this.externalSchemes = new ArrayList<>();
		this.internalSchemes = new ArrayList<>(); 
		this.contexts = new ArrayList<>();
		this.deliberationCycle = new ArrayList<>(); 
	}
	
	/** Create and initiate an agent state given this specification. */
	final AgentState makeState(final Consumer<Runnable> rescheduler, final BiConsumer<AgentID, Trigger> messenger) {
		final AgentID agentID = AgentID.newInstance();
		final List<Context> contexts = new ArrayList<>();
		for(Supplier<Context> supplier : this.contexts)
			contexts.add(supplier.get());
		final List<Consumer<AgentState>> deliberationCycle = this.deliberationCycle.isEmpty() ? 
				DEFAULT_DELIBERATION_CYCLE : this.deliberationCycle;
		return new AgentState(agentID, this.externalSchemes, this.internalSchemes, 
				rescheduler, deliberationCycle, contexts, messenger);
	}
	
	/** Add a supplier for a context. If your context has no arguments, note that you can then use: 
	 * <code>addContext(MyContext::new)</code>. If it does have some arguments then you can use: 
	 * <code>addContext(() -> new MyContext(arg1...argk))</code>. */
	public final AgentSpecification addContext(final Supplier<Context> context) {
		this.contexts.add(context);
		return this;
	}
	
	/** Adds a plan scheme that is used for processing external triggers that fires and returns true for each trigger that 
	 * is an instance of the given class. */
	public final <T extends Trigger> AgentSpecification addExternalPlanScheme(final Class<T> klass, final BiConsumer<T, AgentController> plan) {
		this.externalSchemes.add(classTriggerScheme(klass, plan));
		return this;
	}
	
	/** Adds a plan scheme that is used for processing external triggers. */
	public final AgentSpecification addExternalPlanScheme(final BiPredicate<Trigger, AgentController> scheme) {
		this.externalSchemes.add(scheme);
		return this;
	} 
	
	/** Adds a plan scheme that is used for processing internal triggers that fires and returns true for each trigger that 
	 * is an instance of the given class. */
	public final <T extends Trigger> AgentSpecification addInternalPlanScheme(final Class<T> klass, final BiConsumer<T, AgentController> plan) {
		this.internalSchemes.add(classTriggerScheme(klass, plan));
		return this;
	}

	/** Adds a plan scheme that is used for processing internal triggers. */
	public final AgentSpecification addInternalPlanScheme(final BiPredicate<Trigger, AgentController> scheme) {
		this.internalSchemes.add(scheme);
		return this;
	}

	/** Adds a plan scheme that is used for processing triggers and fires for each trigger of the given class. */
	private final static <T extends Trigger> BiPredicate<Trigger, AgentController> classTriggerScheme(final Class<T> klass, final BiConsumer<T, AgentController> plan){
		return (trigger, planToAgent) -> { 
			if(klass.isInstance(trigger)) {
				plan.accept(klass.cast(trigger), planToAgent);
				return true;
			} else return false;
		};
	}
	
	public final AgentSpecification include(final AgentSpecification other) {
		this.externalSchemes.addAll(other.externalSchemes);
		this.internalSchemes.addAll(other.internalSchemes);
		this.contexts.addAll(other.contexts); 
		return this;
	}
}
