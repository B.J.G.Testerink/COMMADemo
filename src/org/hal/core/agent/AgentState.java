package org.hal.core.agent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiPredicate;
import java.util.function.Consumer;

/**
 * This class implements the runtime state of the agent. The state is executable, which 
 * means that it can mutate to the next state as specified by the operational semantics
 *  of the agent, which in turn are specified by the agent's deliberation cycle.
 *
 */
final class AgentState implements Runnable {
	/** The identifier of the agent. */
	private final AgentID agentID;
	
	/** Listeners that are notified when the agent dies. */
	private final Set<Consumer<AgentID>> deathListeners;
	
	/** The triggers that may trigger the agent into action. */
	private List<Trigger> externalTriggers, internalTriggers;
	
	/** Schemes for processing triggers. */
	private final List<BiPredicate<Trigger, AgentController>> externalSchemes, internalSchemes; 
	
	/** The deliberation cycle of the agent. */
	private final List<Consumer<AgentState>> deliberationCycle;  
	
	/** Interface that exposes the relevant parts of the agent run time data for plans. */
	private final AgentController agentController;
	
	/** Interface to the platform that allows the agent to reschedule its own deliberation runnable. */
	private final Consumer<Runnable> rescheduler;
	
	/** Map to store the contexts. */
	private final Map<Class<?>, Context> contexts;
	
	/** Whether the agent should not execute any deliberation cycles anymore (dead) and whether the agent is 
	 * currently executing a cycle or is scheduled to execute a cycle (sleep). */
	private boolean dead, sleep; 
	 
	AgentState(final AgentID agentID, final List<BiPredicate<Trigger, AgentController>> externalSchemes,
			final List<BiPredicate<Trigger, AgentController>> internalSchemes, final Consumer<Runnable> rescheduler,
			final List<Consumer<AgentState>> deliberationCycle, final List<Context> contexts, 
			final BiConsumer<AgentID, Trigger> messenger) {
		this.agentID = agentID;
		this.externalTriggers = new ArrayList<>();
		this.internalTriggers = new ArrayList<>();
		this.deathListeners = new HashSet<>();
		this.externalSchemes = externalSchemes;
		this.internalSchemes = internalSchemes;
		this.rescheduler = rescheduler;
		this.deliberationCycle = deliberationCycle; 
		this.contexts = new HashMap<>();
		for(Context context : contexts)
			addContext(context);
		this.agentController = new AgentController(this, messenger);
		this.dead = false;
		this.sleep = false;
	}

	/** Returns the current external triggers and creates a new list in the state. */
	final List<Trigger> getAndRemoveExternalTriggers(){
		synchronized(this.externalTriggers){
			if(this.externalTriggers.isEmpty()) 
				return Collections.emptyList();
			final List<Trigger> result = this.externalTriggers;
			this.externalTriggers = new ArrayList<>();
			return result;
		}   
	} 
	
	/** Returns the current internal triggers and creates a new list in the state. */
	final List<Trigger> getAndRemoveInternalTriggers(){
		synchronized (this.internalTriggers) {
			if(this.internalTriggers.isEmpty()) 
				return Collections.emptyList();
			final List<Trigger> result = this.internalTriggers;
			this.internalTriggers = new ArrayList<>();
			return result;
		} 
	} 
	
	/** Return the interface that plans use to access this state. */
	final AgentController getAgentController() {
		return this.agentController;
	}

	/** Put an external event in this agent. Will be processed the next deliberation cycle. */
	final void addExternalTrigger(final Trigger trigger){
		if(this.dead || trigger == null) return;
		synchronized(this.externalTriggers){ 
			this.externalTriggers.add(trigger);
		}	
		checkWhetherToReschedule();
	}
	
	/** Put an internal event in this agent. Will be processed the next deliberation cycle. */
	final void addInternalTrigger(final Trigger trigger){
		if(this.dead || trigger == null) return;
		synchronized(this.internalTriggers){ 
			this.internalTriggers.add(trigger);
		}
		checkWhetherToReschedule();
	}
	
	/** Run one deliberation cycle. */
	public final void run() {
		if(!this.dead) {
			for(Consumer<AgentState> step : this.deliberationCycle)
				step.accept(this); 
			this.sleep = true; 
			checkWhetherToReschedule(); 
		}
	}
	
	/**
	 * If the agent receives input then this method will be called upon to check 
	 * whether it is required to reschedule. If so (i.e. when the agent is currently 
	 * sleeping) then the agent's runnable will be rescheduled by this method.  
	 */
	private final void checkWhetherToReschedule(){
		if(!this.sleep || this.dead) return;
		synchronized (this.externalTriggers) {
			synchronized (this.internalTriggers) {
				// Note: if this method is called simultaneously then the process could pass the first
				// sleep test before entering the synchronized block. Hence, we have to check the sleep 
				// Boolean again in order to make sure that we do not doubly schedule the agent.
				if(this.sleep && (!this.externalTriggers.isEmpty() || !this.internalTriggers.isEmpty())) {
					this.sleep = false;
					this.rescheduler.accept(this); 
				} else this.sleep = true;
			}
		} 
	}
	
	/** Kills the agent, immediately notifies the death listeners. */
	final void kill() {
		if(!this.dead){ 
			this.dead = true;
			synchronized (this.deathListeners) {
				for(Consumer<AgentID> listener : this.deathListeners)
					listener.accept(this.agentID);
			}
		}
	}
	
	/** Add a consumer that is called when the agent dies. */
	final void addDeathListener(final Consumer<AgentID> listener) {
		synchronized(this.deathListeners) {
			this.deathListeners.add(listener);
		} 
	}
	
	/** Remove a death lisener. */
	final void removeDeathListener(final Consumer<AgentID> listener) {
		synchronized(this.deathListeners) {
			this.deathListeners.remove(listener);
		}
	}

	/** Obtain the ID of the agent whose state this is. */
	final AgentID getAgentID() {
		return this.agentID;
	}
	
	/** Store a new context. Will overwrite any context with the same class that 
	 * was previously added. */
	final void addContext(final Context context){
		this.contexts.put(context.getClass(), context);
	}
	
	/**
	 * Suppressed unchecked warning because the add method ensures that the cast
	 * will always be successful. 
	 * @param klass
	 * @return
	 */
	@SuppressWarnings("unchecked")
	final <C extends Context> C getContext(final Class<C> klass){
		return (C) this.contexts.get(klass);
	}
	
	final List<BiPredicate<Trigger, AgentController>> getExternalPlanSchemes(){
		return this.externalSchemes;
	}
	
	final List<BiPredicate<Trigger, AgentController>> getInternalPlanSchemes(){
		return this.internalSchemes;
	}
	
	final boolean isSleeping() { 
		return this.sleep;
	}
	
	final boolean isDead() { 
		return this.dead;
	}
}
