package org.politie.nl.argumentation.dunglabelling;

public enum Label {
	IN, OUT, UNDECIDED, MUST_OUT, BLANK, CONTESTED, UNSATISFIABLE, IN_BLOCKED;
}
