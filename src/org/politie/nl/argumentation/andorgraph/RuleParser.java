package org.politie.nl.argumentation.andorgraph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
/**
 * Auxiliary class for parsing rules from strings.
 */
public class RuleParser {
    public static final <P> Map<P, Set<Set<P>>> parseRules(final Function<String, P> parser, final String ruleDelimiter, final String input) {
        final Map<P, Set<Set<P>>> rules = new HashMap<>();
        final String[] rulesAsStrings = input.replaceAll("\\s", "").split(";");
        for (String ruleAsString : rulesAsStrings) {
            final String[] decomposition = ruleAsString.split(",|" + ruleDelimiter);
            final int indexHead = decomposition.length - 1;
            final List<P> body = new ArrayList<>();
            for (int i = 0; i < indexHead; i++) {
                body.add(parser.apply(decomposition[i]));
            }
            rules.computeIfAbsent(parser.apply(decomposition[indexHead]), p -> new HashSet<>()).add(new HashSet<>(body));
        }
        return rules;
    }

}
