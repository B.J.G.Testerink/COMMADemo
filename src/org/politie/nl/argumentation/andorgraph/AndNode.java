package org.politie.nl.argumentation.andorgraph;

import java.util.ArrayList;
import java.util.List;
/**
 * An and-node represents a defeasible rule.
 *  
 * @param <T>
 */
public final class AndNode<T> {
    private final OrNode<T> parent;
    private final List<OrNode<T>> children = new ArrayList<>();
    private boolean active = false;

    AndNode(final OrNode<T> parent) {
        this.parent = parent;
    }

    void activate() {
        if (this.active) {
            return;
        }
        for (OrNode<?> child : children) {
            if (!child.isActive()) {
                return;
            }
        }
        this.active = true;
        parent.activate();
    }

    boolean isActive() {
        return this.active;
    }

    void addChild(final OrNode<T> child) {
        this.children.add(child);
    }

    public OrNode<T> getParent() {
        return this.parent;
    }

    public List<OrNode<T>> getChildren() {
        return this.children;
    }
}
