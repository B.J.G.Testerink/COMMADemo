package org.politie.nl.argumentation.andorgraph;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
/**
 * An argument is an inference tree. An argument object stores the root of 
 * an inference tree (its conclusion) and the subarguments that act as its
 * premises. 
 * @param <P>
 */
public final class Argument<P> {
    private final P conclusion;
    private final List<Argument<P>> premises = new ArrayList<>();
    // All the (intermediate) premises. This is used to prevent argumentative cycles where p is a premise of an 
    // argument for p. 
    private final Set<P> literalDependencies = new HashSet<>(); 

    public Argument(final P conclusion) {
        this.conclusion = conclusion;
        this.literalDependencies.add(conclusion);
    }

    public boolean dependsOn(final P premise) {
        return this.literalDependencies.contains(premise);
    }

    public void addPremise(final Argument<P> argument) {
        this.premises.add(argument);
        this.literalDependencies.addAll(argument.getLiteralDependencies());
    }

    public List<Argument<P>> getPremises() {
        return this.premises;
    }

    public P getConclusion() {
        return this.conclusion;
    }

    Set<P> getLiteralDependencies() {
        return this.literalDependencies;
    }

    public String toString() {
        if (premises.isEmpty()) {
            return (String) this.conclusion;
        }
        final StringBuilder buffer = new StringBuilder();
        buffer.append(conclusion).append(":[");
        for (Argument<P> premise : this.premises) {
            buffer.append(' ').append(premise);
        }
        buffer.append(" ]");
        return buffer.toString();
    }
}
