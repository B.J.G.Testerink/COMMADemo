package org.politie.nl.argumentation.andorgraph;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.politie.nl.argumentation.dunglabelling.Label;
 
/**
 * An and-or-graph is a graph representation of a set of defeasible rules and 
 * the observable literals. We use this representation to generate arguments 
 * given a set of observations and to determine whether new observations could 
 * change the opinion on a literal (its status in a Dung graph which is evaluated 
 * using grounded semantics). 
 */
public final class AndOrGraph {
    private AndOrGraph() {
        // noop to prevent instantiation
    }

    @SuppressWarnings("unchecked")
    public static <P> Entry<Argument<P>[], Function<Argument<P>, Set<Argument<P>>>> produceDungGraph(final Map<P, OrNode<P>> andOrGraph, final Set<P> observations,
                                                                                                     final BiPredicate<P, P> contrariness) {
        // Get the arguments
        final Map<P, Set<Argument<P>>> conclusionToArguments = makeArguments(andOrGraph, observations);
        final List<Argument<P>> arguments = new ArrayList<>();
        for (Map.Entry<P, Set<Argument<P>>> conclusionToArgument : conclusionToArguments.entrySet()) {
            arguments.addAll(conclusionToArgument.getValue());
        }

        // Make the attack relation
        final Map<Argument<P>, Set<Argument<P>>> attackRelation = new HashMap<>();
        for (Argument<P> attacker : arguments) {
            for (Argument<P> attacked : arguments) {
                for (P dependency : attacked.getLiteralDependencies()) {
                    if (contrariness.test(attacker.getConclusion(), dependency)) {
                        attackRelation.computeIfAbsent(attacker, a -> new LinkedHashSet<>()).add(attacked);
                        break;
                    }
                }
            }
        }

        return new SimpleEntry<>(
                (Argument<P>[]) arguments.toArray(new Argument[arguments.size()]), a -> attackRelation.getOrDefault(a, Collections.emptySet()));
    }

    private static <P> Map<P, Set<Argument<P>>> makeArguments(final Map<P, OrNode<P>> graph, final Set<P> observations) {
        // Activate the children
        for (P observation : observations) {
            final OrNode<P> orNode = graph.get(observation);
            if (orNode != null) {
                orNode.observed();
            }
        }

        final Map<P, Set<Argument<P>>> conclusionToArguments = new HashMap<>();

        // Activate the observations and get the and-nodes that possibly generate new arguments
        Set<AndNode<P>> nextRound = getAndNodes(graph, observations, conclusionToArguments);

        // Iterative make arguments (inference trees) from increasing height (where max height is the number of or-nodes)
        iterateArgumentsTillSettled(conclusionToArguments, nextRound);
        return conclusionToArguments;
    }

    private static <P> void iterateArgumentsTillSettled(Map<P, Set<Argument<P>>> conclusionToArguments, Set<AndNode<P>> nextRound) {
        while (!nextRound.isEmpty()) {
            Set<AndNode<P>> front = nextRound;
            nextRound = new LinkedHashSet<>();
            for (AndNode<P> node : front) {
                // Every front node might have new arguments that it can generate
                final OrNode<P> conclusion = node.getParent();
                // Make arguments
                final int nrOfCurrentArguments = nrOfArguments(conclusionToArguments, conclusion.getContent());
                findNewArgumentsFor(node, conclusionToArguments);
                final int nrOfNewArguments = nrOfArguments(conclusionToArguments, conclusion.getContent()) - nrOfCurrentArguments;
                // If the node generates new arguments, then its parents may generate new arguments
                if (nrOfNewArguments > 0) {
                    for (AndNode<P> next : conclusion.getParents()) {
                        // Only add the parents if for each of its premises there exists an argument
                        if (isActive(next, conclusionToArguments)) {
                            nextRound.add(next);
                        }
                    }
                }
            }
        }
    }

    private static <P> Set<AndNode<P>> getAndNodes(Map<P, OrNode<P>> graph, Set<P> observations, Map<P, Set<Argument<P>>> conclusionToArguments) {
        Set<AndNode<P>> nextRound = new LinkedHashSet<>();
        for (P observation : observations) {
            final OrNode<P> orNode = graph.get(observation);
            if (orNode != null) {
                conclusionToArguments.computeIfAbsent(observation, a -> new LinkedHashSet<>()).add(new Argument<>(observation));
                for (AndNode<P> parent : orNode.getParents()) {
                    if (isActive(parent, conclusionToArguments)) {
                        nextRound.add(parent);
                    }
                }
            }
        }
        return nextRound;
    }

    private static <P> boolean isActive(final AndNode<P> node, final Map<P, Set<Argument<P>>> conclusionToArguments) {
        for (OrNode<P> premise : node.getChildren()) {
            if (conclusionToArguments.get(premise.getContent()) == null) {
                return false;
            }
        }
        return true;
    }

    private static <P> int nrOfArguments(final Map<P, Set<Argument<P>>> conclusionToArguments, final P conclusion) {
        return conclusionToArguments.getOrDefault(conclusion, Collections.emptySet()).size();
    }

    private static <P> void findNewArgumentsFor(final AndNode<P> rule, final Map<P, Set<Argument<P>>> conclusionToArguments) {
        // Collect the arguments for each premise (becomes a vector of argument sets of which each combination
        // constitutes an argument)
        final List<Set<Argument<P>>> premiseOptions = new ArrayList<>();
        for (OrNode<P> child : rule.getChildren()) {
            premiseOptions.add(conclusionToArguments.get(child.getContent()));
        }
        visitCombinations(
                rule.getParent().getContent(),
                premiseOptions,
                0,
                new ArrayList<>(),
                conclusionToArguments.computeIfAbsent(rule.getParent().getContent(), p -> new LinkedHashSet<>()));
    }

    /**
     * Visit all combinations of arguments in the premises (each of which constitutes an argument) and check whether
     * the conclusion, given those premises is a new and legal argument.
     */
    private static <P> void visitCombinations(final P conclusion, final List<Set<Argument<P>>> premiseOptions, final int depth, final List<Argument<P>> current, final Set<Argument<P>> existing) {
        if (depth == premiseOptions.size()) {
            // Check if the argument is new
            for (Argument<P> oldArgument : existing) {
                if (oldArgument.getPremises().containsAll(current)) {
                    return;
                }
            }
            // Check if argument is legal (no double occurrences of premises on a path)
            if (legal(conclusion, current)) {
                final Argument<P> newArgument = new Argument<>(conclusion);
                for (Argument<P> premise : current) {
                    newArgument.addPremise(premise);
                }
                existing.add(newArgument);
            }
        } else {
            // Go through all the options of this premise
            for (Argument<P> argument : premiseOptions.get(depth)) {
                current.add(argument);
                visitCombinations(conclusion, premiseOptions, depth + 1, current, existing);
                current.remove(current.size() - 1);
            }
        }
    }

    private static <P> boolean legal(final P taboo, final List<Argument<P>> premises) {
        for (Argument<P> premise : premises) {
            if (premise.getLiteralDependencies().contains(taboo)) {
                return false;
            }
        }
        return true;
    }

    public static <P> Map<P, OrNode<P>> buildAndOrGraph(final Map<P, Set<Set<P>>> rules) {
        // Build the and/or graph
        final Map<P, OrNode<P>> propositionToNode = new HashMap<>();
        for (P proposition : rules.keySet()) {
            final OrNode<P> conclusionNode = getNode(proposition, propositionToNode);
            final Set<Set<P>> bodies = rules.getOrDefault(proposition, Collections.emptySet());
            for (Set<P> body : bodies) {
                final AndNode<P> ruleNode = new AndNode<>(conclusionNode);
                conclusionNode.addChild(ruleNode);
                for (P premise : body) {
                    final OrNode<P> premiseNode = getNode(premise, propositionToNode);
                    ruleNode.addChild(premiseNode);
                    premiseNode.addParent(ruleNode);
                }
            }
        }
        return propositionToNode;
    }

    private static <P> OrNode<P> getNode(final P proposition, final Map<P, OrNode<P>> map) {
        return map.computeIfAbsent(proposition, OrNode::new);
    }
    
    /**
     * Color an and-node. IN means that all children are colored IN and UNSATISFIABLE means that at least one child is colored but not IN.
     * @param node
     * @param andColors
     * @param orColors
     */
    private static <P> void colorAndNode(final AndNode<P> node, final Map<AndNode<P>, Label> andColors, final Map<OrNode<P>, Label> orColors){
    	// An and-node is IN if all children are IN, and is UNSAT if any child cannot be IN, and otherwise remains uncolored
    	boolean isIn = true;
    	for(OrNode<P> child : node.getChildren()){
    		final Label label = orColors.getOrDefault(child, Label.BLANK);
    		if(label != Label.BLANK && label != Label.IN){  
    			andColors.put(node, Label.UNSATISFIABLE);
    			return;
    		} else if(label == Label.BLANK){
    			isIn = false;
    		}
    	} 
		if(isIn){
			andColors.put(node, Label.IN); 
		}
    }
    
    /**
     * Check whether an or-node has an IN and-node.
     * @param node
     * @param andColors
     * @return
     */
    private static <P> boolean hasArgument(final OrNode<P> node, final Map<AndNode<P>, Label> andColors){
    	for(AndNode<P> child : node.getChildren()){
    		if(andColors.getOrDefault(child, Label.BLANK) == Label.IN){
    			return true;
    		}
    	}
    	return false;
    }
    
    /**
     * Color an or-node. UNSATISFIABLE means that no argument can be made for the proposition of the node. OUT means that a contrary
     * proposition was observed. IN means that an argument can be made for the proposition and no counter-argument can be made that is IN. 
     * Contested means that an argument that is IN can be made, but also for a counter-argument.
     * @param node
     * @param isObservable
     * @param andColors
     * @param orColors
     * @param contraryNode
     */
    private static <P> void colorOrNode(final OrNode<P> node, final boolean isObservable, final Map<AndNode<P>, Label> andColors, final Map<OrNode<P>, Label> orColors, final Optional<OrNode<P>> contraryNode){
    	boolean hasArgument = false;
    	boolean hasUncoloredChild = false;
    	for(AndNode<P> child : node.getChildren()){
    		final Label label = andColors.getOrDefault(child, Label.BLANK);
    		if(label == Label.IN){
    			hasArgument = true; 
    		} else if(label == Label.BLANK){
    			hasUncoloredChild = true;
    		}
    	} 
    	final Label contraryLabel = contraryNode.isPresent() ? orColors.getOrDefault(contraryNode.get(), Label.BLANK) : Label.BLANK;
    	
    	if(!isObservable && !hasUncoloredChild && !hasArgument)
    		orColors.put(node, Label.UNSATISFIABLE);
    	else if(contraryLabel == Label.IN)
    		orColors.put(node, Label.OUT);
    	else if(hasArgument && (!contraryNode.isPresent() || contraryLabel == Label.UNSATISFIABLE || contraryLabel == Label.OUT))
    		orColors.put(node, Label.IN);
    	else if(!isObservable && contraryNode.isPresent() && hasArgument && hasArgument(contraryNode.get(), andColors))
    		orColors.put(node, Label.CONTESTED);
//    	if(orColors.containsKey(node))
//    		System.out.println(node.getContent() + "::\t"+ orColors.get(node));
    }
    
    /**
     * Convert the contrariness relation to a set of or-node pairs.
     * @param graph
     * @param contrariness
     * @return
     */
    private static <P> Map<OrNode<P>, OrNode<P>> contraryOrNodePairs(final Map<P, OrNode<P>> graph, final BiPredicate<P, P> contrariness){
    	final Map<OrNode<P>, OrNode<P>> result = new HashMap<>();
    	for(Entry<P, OrNode<P>> a : graph.entrySet()){
    		for(Entry<P, OrNode<P>> b : graph.entrySet()){
    			if(contrariness.test(a.getKey(), b.getKey()))
    				result.put(a.getValue(), b.getValue());
    		}
    	}
    	return result;
    } 
    
    /**
     * Label the and-or graph to determine which propositions can have no more valid arguments.
     * @param graph
     * @param contraryOrNodes
     * @param observations
     * @param observable
     * @param andColors
     * @param orColors
     */
    private static <P> void makeAndOrColoring(final Map<P, OrNode<P>> graph, final Map<OrNode<P>, OrNode<P>> contraryOrNodes, final Set<P> observations, final Set<P> observable, final Map<AndNode<P>, Label> andColors, final Map<OrNode<P>, Label> orColors){
    	//
    	Set<P> roundOr = new HashSet<>(observations);
    	Set<P> frontOr = new HashSet<>();
    	Set<AndNode<P>> roundAnd = new HashSet<>();
    	Set<AndNode<P>> frontAnd = new HashSet<>();
    	while(!(roundOr.isEmpty() && roundAnd.isEmpty())){
    		for(P proposition : roundOr){
    			final OrNode<P> node = graph.get(proposition);
				final Optional<OrNode<P>> contraryNode = Optional.ofNullable(contraryOrNodes.get(node));
    			if(!orColors.containsKey(node)){ // A color means that the node is already done
    				if(observations.contains(proposition)){ // Observations are always IN
    					orColors.put(node, Label.IN);
    				} else {
    					colorOrNode(node, observable.contains(proposition), andColors, orColors, contraryNode);
    				}
    				if(orColors.containsKey(node)){ // If the node is now stable, then perhaps any of its parents are too
    					frontAnd.addAll(node.getParents());
    					if(contraryNode.isPresent()){
    						frontOr.add(contraryNode.get().getContent());
    					}
    				}
    			}
    		}
    		for(AndNode<P> node : frontAnd){
    			if(!andColors.containsKey(node)){
    				colorAndNode(node, andColors, orColors);
    				if(andColors.containsKey(node)){ // If the node is now stable, then perhaps its parent is too
    					frontOr.add(node.getParent().getContent());
    				}
    			}
    		}
    		roundOr = frontOr;
    		frontOr = new HashSet<>();
    		roundAnd = frontAnd;
    		frontAnd = new HashSet<>();
    	}
    }
    
    /**
     * Get all all observables that are reachable from the topic by only visiting BLANK and IN nodes. 
     * @param graph
     * @param contraryOrNodes
     * @param observations
     * @param observable
     * @param andColors
     * @param orColors
     * @param assumptions
     * @param topic
     * @return
     */
    private static <P> Set<P> getReachableObservables(final Map<P, OrNode<P>> graph, final Map<OrNode<P>, OrNode<P>> contraryOrNodes, final Set<P> observations, final Set<P> observable, final Map<AndNode<P>, Label> andColors, final Map<OrNode<P>, Label> orColors, final Set<P> assumptions, final P topic){
    	final Set<P> relevant = new HashSet<>();
    	Set<OrNode<P>> frontOr = new HashSet<>();
    	Set<OrNode<P>> roundOr = new HashSet<>();
    	roundOr.add(graph.get(topic));
    	Set<OrNode<P>> visitedOr = new HashSet<>();
    	Set<AndNode<P>> frontAnd = new HashSet<>();
    	Set<AndNode<P>> roundAnd = new HashSet<>();
    	Set<AndNode<P>> visitedAnd = new HashSet<>();
    	// This is a simple breadth first search, but having to node types makes it a bit bulky
    	while(!(roundOr.isEmpty() && roundAnd.isEmpty())){
    		for(OrNode<P> node : roundOr){
    			if(!visitedOr.contains(node)){
    				visitedOr.add(node); 
    				final boolean observed = observations.contains(node.getContent());
    				final boolean assumed = assumptions.contains(node.getContent());
    				if( (observable.contains(node.getContent()) && !observed) ||
    					(assumed && observed)){
    					relevant.add(node.getContent());
    				}
    				if(!observed || assumed){
    					for(AndNode<P> child : node.getChildren()){
    						final Label label = andColors.getOrDefault(child, Label.BLANK); 
    						// TODO: for if you want to get more IN arguments
//    						if(label == Label.IN || label == Label.BLANK){
//    							frontAnd.add(child);
//    						}
    						if(label == Label.BLANK){
    							frontAnd.add(child);
    						}
    					} 
    					final Optional<OrNode<P>> contraryNode = Optional.ofNullable(contraryOrNodes.get(node));
    					if(contraryNode.isPresent()){
    						final Label label = andColors.getOrDefault(contraryNode.get(), Label.BLANK);
    						// TODO: for if you want to get more IN arguments
//    						if(label == Label.IN || label == Label.BLANK){
//    							frontOr.add(contraryNode.get());
//    						}
    						if(label == Label.BLANK){
    							frontOr.add(contraryNode.get());
    						} 
    					}
    				}
    			}
    		}
    		for(AndNode<P> node : roundAnd){
    			if(!visitedAnd.contains(node)){
    				visitedAnd.add(node);
    				for(OrNode<P> child : node.getChildren()){
    					final Label label = orColors.getOrDefault(child, Label.BLANK);
    					// TODO: for if you want to get more IN arguments
//						if(label == Label.IN || label == Label.BLANK){
//							frontOr.add(child);
//						}
    					if(label == Label.BLANK){
							frontOr.add(child);
						}
    				}
    			}
    		}
    		roundOr = frontOr;
    		frontOr = new HashSet<>();
    		roundAnd = frontAnd;
    		frontAnd = new HashSet<>();
    	}
    	return relevant;
    }
    public static <P> void getRelevantObservations(final Map<P, OrNode<P>> graph, final BiPredicate<P, P> contrariness, final Set<P> observations, final Set<P> observable, final Set<P> topics){
    	getRelevantObservations(graph, contrariness, observations, observable, topics, new HashMap<>(), new HashMap<>(), new HashMap<>(), new HashMap<>(), new HashMap<>());
    }
    /**
     * Get the observables that may sway the opinion on the topic if observed, but only if the topic can potentially be argued for.
     * @param graph
     * @param contrariness
     * @param observations
     * @param observable
     * @param topic
     * @return
     */
    public static final <P> void getRelevantObservations(
    		final Map<P, OrNode<P>> graph, 
    		final BiPredicate<P, P> contrariness, 
    		final Set<P> observations, 
    		final Set<P> observable, 
    		final Set<P> topics,
    		final Map<OrNode<P>, Label> preAssumptionOrColoring,
    		final Map<AndNode<P>, Label> preAssumptionAndColoring,
    		final Map<OrNode<P>, Label> postAssumptionOrColoring,
    		final Map<AndNode<P>, Label> postAssumptionAndColoring,
    		final Map<P, Set<P>> relevance){
    	final Map<OrNode<P>, OrNode<P>> contraryOrNodes = contraryOrNodePairs(graph, contrariness);
    	// First see if the opinion on the topic is already stable. 
    	//System.out.println("====== COLORING WITHOUT ASSUMPTIONS ======");
    	makeAndOrColoring(graph, contraryOrNodes, observations, observable, preAssumptionAndColoring, preAssumptionOrColoring);
    	for(P topic : topics) {
    		if(preAssumptionOrColoring.containsKey(graph.get(topic))){
    			relevance.put(topic, Collections.emptySet());
    		}
    	}

    	// Assume all observables that have no contrary proposition pointing to them
    	final Set<P> observationsAndAssumptions = new HashSet<>(observations);
    	final Set<P> assumptions =  
    			observable.stream()
    			.filter(p -> {
    					final OrNode<P> contrary = contraryOrNodes.get(graph.get(p));
    					return contrary == null || !observable.contains(contrary.getContent());
    				})
    			.collect(Collectors.toSet());
    	observationsAndAssumptions.addAll(assumptions); 
    	
    	// Redo the coloring with the assumptions
    	//System.out.println("====== COLORING WITH ASSUMPTIONS ======");
    	makeAndOrColoring(graph, contraryOrNodes, observationsAndAssumptions, observable, postAssumptionAndColoring, postAssumptionOrColoring);
    	
    	// Check if the topic can be argued for
    	for(P topic : topics) {
    		if(!relevance.containsKey(topic)) {
    			final Label label = postAssumptionOrColoring.getOrDefault(graph.get(topic), Label.BLANK);
            	if(label != Label.IN && label != Label.BLANK){
            		//System.out.println("Opinion on topic will eventually not be positive.");
            		relevance.put(topic, Collections.emptySet());
            	} else {
            		if(label == Label.IN){
            			//System.out.println("There exists a set of observables such that we will get a stable IN argument for the topic. (oftewel 1 informatieverzoek is voldoende)");
            			// TODO: in dit geval alleen maar een (minimale) verzameling proposities verzamelen zodanig dat de topic gegarandeerd IN wordt? 
            		} else {
            			//System.out.println("Depending on the answer on one or more observables, the opinion on the topic may change.");
            			// TODO: meer uitgebreide informatieverzoeken opstellen
            		}
            		relevance.put(topic, getReachableObservables(graph, contraryOrNodes, observationsAndAssumptions, observable, preAssumptionAndColoring, preAssumptionOrColoring, assumptions, topic));
            	}
    		} 
    	}
    }
    
    /** An or-node is blocked from becoming in if its contrary or node has in IN and-node or if for each 
     * potential argument, there is a premise for which this holds.  */
    public static final <P> Set<P> getBlockedInConclusion(
    		final Map<P, OrNode<P>> graph, 
    		final BiPredicate<P, P> contrariness, 
    		final Map<OrNode<P>, Label> postAssumptionOrColoring,
    		final Map<AndNode<P>, Label> postAssumptionAndColoring,
    		final Argument<P>[] arguments){
    	final Set<P> result = new HashSet<>();
    	
    	Set<OrNode<P>> roundOr = new HashSet<>();
    	for(Argument<P> argument : arguments) {
    		for(Map.Entry<P, OrNode<P>> node : graph.entrySet()) {
    			if(contrariness.test(argument.getConclusion(), node.getKey())) { 
    				if(postAssumptionOrColoring.getOrDefault(node.getValue(), Label.BLANK) == Label.BLANK) {
    					roundOr.add(node.getValue());
    					break;
    				}
    			}
    		} 
    	}
    	
    	Set<OrNode<P>> frontOr = new HashSet<>();
    	Set<OrNode<P>> visitedOr = new HashSet<>();
    	Set<AndNode<P>> frontAnd = new HashSet<>();
    	Set<AndNode<P>> roundAnd = new HashSet<>();
    	Set<AndNode<P>> visitedAnd = new HashSet<>();
    	while(!(roundOr.isEmpty() && roundAnd.isEmpty())){
    		for(OrNode<P> node : roundOr){
    			if(!visitedOr.contains(node)){
    				visitedOr.add(node); 
    				postAssumptionOrColoring.put(node, Label.IN_BLOCKED);
    				result.add(node.getContent());
    				for(AndNode<P> parent : node.getParents()) {
    					if(postAssumptionAndColoring.getOrDefault(parent, Label.BLANK) == Label.BLANK) {
    						roundAnd.add(parent);
    					}
    				}
    			}
    		}
    		for(AndNode<P> node : roundAnd){
    			if(!visitedAnd.contains(node)){
    				visitedAnd.add(node);
    				postAssumptionAndColoring.put(node, Label.IN_BLOCKED);
    				if(postAssumptionOrColoring.getOrDefault(node.getParent(), Label.BLANK) != Label.BLANK) {
	    				boolean blocked = true;
	    				for(AndNode<P> child : node.getParent().getChildren()) {
	    					final Label label = postAssumptionAndColoring.getOrDefault(child, Label.BLANK);
	    					if(label == Label.BLANK || label == Label.IN) {
	    						blocked = false;
	    						break;
	    					}
	    				}
	    				if(blocked) {
	    					frontOr.add(node.getParent());
	    				}
    				}
    			}
    		}
    		roundOr = frontOr;
    		frontOr = new HashSet<>();
    		roundAnd = frontAnd;
    		frontAnd = new HashSet<>();
    	}
    	
    	// Add all the nodes that were already colored
    	for(Map.Entry<P, OrNode<P>> node : graph.entrySet()) {
    		final Label label = postAssumptionOrColoring.getOrDefault(node.getValue(), Label.BLANK);
    		if(label != Label.BLANK && label != Label.IN) {
    			result.add(node.getKey());
    		}
    	}
    	return result;
    }
}


