package org.politie.nl.argumentation.andorgraph;

import java.util.ArrayList;
import java.util.List;
/**
 * An or-node represents a literal that occurs in an argumentation knowledgebase (either in a rule 
 * or as an observable).
 * @param <P>
 */
public final class OrNode<P> {
    private final List<AndNode<P>> parents = new ArrayList<>();
    private final List<AndNode<P>> children = new ArrayList<>();
    private final P content;
    private boolean active = false;
    private boolean observed = false;

    public OrNode(final P content) {
        this.content = content;
    }

    void observed() {
        this.observed = true;
        activate();
    }

    void activate() {
        if (this.active) {
            return;
        }
        this.active = true;
        for (AndNode<P> parent : parents) {
            parent.activate();
        }
    }

    boolean isActive() {
        return this.active;
    }

    boolean isObserved() {
        return this.observed;
    }

    void addParent(final AndNode<P> parent) {
        this.parents.add(parent);
    }

    void addChild(final AndNode<P> child) {
        this.children.add(child);
    }

    public P getContent() {
        return this.content;
    }

    List<AndNode<P>> getParents() {
        return this.parents;
    }

    public List<AndNode<P>> getChildren() {
        return this.children;
    }

}
