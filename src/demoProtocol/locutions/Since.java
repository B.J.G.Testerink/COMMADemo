package demoProtocol.locutions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
 
import argumentationProtocols.Template;
import argumentationProtocols.graphs.Argument2Node; 
import argumentationProtocols.graphs.Node;
import argumentationProtocols.graphs.RuleApplicationNode;
import demoProtocol.DialogGraph;
import demoProtocol.DemoProtocol;
/**
 * A since locution allows an agent to make an argument. It does so by saying which proposition by 
 * defeasible reasoning is directly derived form what premises.
 * 
 * @author Bas Testerink
 *
 */
public final class Since  implements DemoLocution {
	private final String conclusion;
	private final String[] premises;
	
	public Since(final String propositionA, final String[] premises){
		this.conclusion = propositionA;
		this.premises = premises;
	}

	public final String getConclusion(){
		return this.conclusion;
	}
	
	public final String[] getPremises(){
		return this.premises;
	}

	/** Creates the template. */
	public static final Template<Since> produceTemplate(){
		return new Template<Since>(
				Since.class, 
				DemoProtocol::mandateCheck, 
				Since::responseRelation, 
				Since::argument1Update);
	}
	
	/** A since operation asserts all the premises and the inference itself (called the major premise). */
	private final static void argument1Update(final argumentationProtocols.graphs.DialogGraph dialogGraph, final Argument2Node<Since> newNode){
		if(!(dialogGraph instanceof DialogGraph))
			return; // Can't update non-demo graphs
		final DialogGraph graph = (DialogGraph) dialogGraph;
		final String conclusion = newNode.getContent().getConclusion();
		final String[] premises = newNode.getContent().getPremises();
		final String ruleProposition = getRuleProposition(conclusion, premises);
		
		// If proposition does not exist as I-Node: add new I-Node
		final Node iNodeConclusion = graph.makeAndGetINode(conclusion);
		final Node[] iNodesPremises = new Node[premises.length];
		for(int i = 0; i < iNodesPremises.length; i++)
			iNodesPremises[i] = graph.makeAndGetINode(premises[i]);
		final Node iNodeRule = graph.makeAndGetINode(ruleProposition);
		
		// Assert the inference
		final RuleApplicationNode ruleApplicationNode = new RuleApplicationNode(iNodesPremises, iNodeConclusion);  
		if(!graph.getNodesOfType(RuleApplicationNode.class).contains(ruleApplicationNode)){ 
			graph.addNode(ruleApplicationNode);
			for(Node iNodePremise : iNodesPremises)
				graph.connect(iNodePremise, ruleApplicationNode); 
			graph.connect(iNodeRule, ruleApplicationNode);
			graph.connect(ruleApplicationNode, iNodeConclusion);  
		} 
		
		// Assert the commitment for the sender 
		for(Node iNodePremise : iNodesPremises)
			DemoProtocol.ensureCommitted(graph, newNode.getSender(), iNodePremise); 
		DemoProtocol.ensureCommitted(graph, newNode.getSender(), iNodeRule);
		DemoProtocol.ensureCommitted(graph, newNode.getSender(), iNodeConclusion);
		
		// Make a mandate
		graph.clearMandate(newNode.getSender(), newNode.getReceiver()); // Sender fulfilled its mandate
		final List<DemoLocution> newMandate = new ArrayList<>();
		newMandate.add(new Why(ruleProposition));
		newMandate.add(new Claim(DemoProtocol.negate(conclusion)));
		newMandate.add(new Claim(DemoProtocol.negate(ruleProposition)));
		newMandate.add(new Since(DemoProtocol.negate(conclusion), new String[0]));
		newMandate.add(new Since(DemoProtocol.negate(ruleProposition), new String[0]));
		newMandate.add(new Skip());
		for(String premise : premises){
			newMandate.add(new Since(DemoProtocol.negate(premise), new String[0]));
			newMandate.add(new Why(premise)); 
			newMandate.add(new Claim(DemoProtocol.negate(premise)));
			newMandate.add(new Concede(premise));
		}
		newMandate.add(new Concede(ruleProposition));
		newMandate.add(new Concede(conclusion));
		
		graph.setMandate(newNode.getReceiver(), newNode.getSender(), newMandate.toArray(new DemoLocution[newMandate.size()]));  
	}
	
	/** A since is a response to any past challenge. */
	private final static List<Node> responseRelation(final argumentationProtocols.graphs.DialogGraph dialogGraph, final Argument2Node<Since> newNode){ 
		if(dialogGraph instanceof DialogGraph && newNode.getContent() instanceof DemoLocution){
			DialogGraph graph = (DialogGraph) dialogGraph;
			String conclusion = newNode.getContent().getConclusion();
			
			List<Node> result = new ArrayList<>(); 
			for(Argument2Node<?> message : graph.getMessageNodes()){
				if(newNode.getSender().equals(message.getReceiver()) && newNode.getReceiver().equals(message.getSender()) && 
						DemoProtocol.messageIsChallengeAbout(conclusion, message))
					result.add(message); 
			}
			return result; 
		}
		// Otherwise not a direct response
		return Collections.emptyList();
	}

	public final boolean equals(final Object object){
		if(object == null) return false;
		if(object == this) return true;
		if(object instanceof Since){
			Since otherSince = (Since) object;
			if(!otherSince.getConclusion().equals(this.conclusion)) return false;
			if(otherSince.premises.length != this.premises.length) return false;
			for(String myPremise : this.premises){
				boolean found = false;
				for(String otherPremise : otherSince.premises){
					if(otherPremise.equals(myPremise)){
						found = true;
						break;
					}
				}
				if(!found) return false;
			}
			return true; 
		} else return false;
	}
	
	/** Produce the major premise of the inference that this locution suggests. */
	public final static String getRuleProposition(final String conclusion, final String[] premises){
		StringBuffer buffer = new StringBuffer();
		buffer.append('(');
		boolean first = true;
		for(String premise : premises){
			if(!first) buffer.append(" & ");
			first = false;
			buffer.append(premise);
		}
		buffer.append(" => ").append(conclusion).append(')');
		return buffer.toString();
	}
	
	public final String toString(){
		return "since: "+getRuleProposition(this.conclusion, this.premises);
	}
}
