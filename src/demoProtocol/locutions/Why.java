package demoProtocol.locutions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
 
import argumentationProtocols.Template;
import argumentationProtocols.graphs.Argument2Node; 
import argumentationProtocols.graphs.Node;
import demoProtocol.DialogGraph;
import demoProtocol.DemoProtocol;
/**
 * A why locution is used to challenge an agent to explain itself, or retract an earlier made 
 * claim. 
 * 
 * @author Bas Testerink
 *
 */
public final class Why implements DemoLocution {
	private final String proposition;

	public Why(final String proposition){
		this.proposition = proposition;
	}

	public final String getProposition(){
		return this.proposition;
	}

	/** Creates the template. */
	public static final Template<Why> produceTemplate(){
		return new Template<Why>(
				Why.class, 
				DemoProtocol::mandateCheck, 
				Why::responseRelation, 
				Why::argument1Update);
	}

	/** A why locution has to be answered by an explanation or the other agent taking back 
	 * the earlier claim (a since or retract locution). Other than that it does not change the 
	 * argument 1 graph. */
	private final static void argument1Update(final argumentationProtocols.graphs.DialogGraph dialogGraph, final Argument2Node<Why> newNode){
		if(!(dialogGraph instanceof DialogGraph))
			return; // Can't update non-demo graphs
		final DialogGraph graph = (DialogGraph) dialogGraph;
		final String proposition = newNode.getContent().getProposition();
		
		// Make a mandate 
		graph.clearMandate(newNode.getSender(), newNode.getReceiver()); // Sender fulfilled its mandate
		final DemoLocution since = new Since(proposition, new String[0]); 
		final DemoLocution retract = new Retract(proposition);
		final DemoLocution skip = new Skip();
		graph.setMandate(newNode.getReceiver(), newNode.getSender(), new DemoLocution[]{ since, retract, skip });  
	}

	/** A why locution is a response to any message that claims the proposition from the why locution. */
	private final static List<Node> responseRelation(final argumentationProtocols.graphs.DialogGraph dialogGraph, final Argument2Node<Why> newNode){
		if(dialogGraph instanceof DialogGraph && newNode.getContent() instanceof DemoLocution){
			final DialogGraph graph = (DialogGraph) dialogGraph;
			final String proposition = newNode.getContent().getProposition();
			
			final List<Node> result = new ArrayList<>(); 
			for(Argument2Node<?> message : graph.getMessageNodes()){
				if(newNode.getSender().equals(message.getReceiver()) && newNode.getReceiver().equals(message.getSender()) && 
					DemoProtocol.messageClaimsProposition(proposition, message))
					result.add(message); 
			}
			return result; 
		}
		// Otherwise not a direct response
		return Collections.emptyList();
	}
	
	public final boolean equals(final Object object){
		if(object instanceof Why){
			return ((Why)object).getProposition().equals(this.proposition);
		} else return false;
	}
	
	public final String toString(){
		return "why: "+this.proposition;
	}
}
