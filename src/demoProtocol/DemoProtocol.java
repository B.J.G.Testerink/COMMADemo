package demoProtocol;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import argumentationProtocols.ArgumentationDialogProtocol; 
import argumentationProtocols.graphs.Argument2Node;
import argumentationProtocols.graphs.CommitmentNode;
import argumentationProtocols.graphs.Edge;
import argumentationProtocols.graphs.Node;
import demoProtocol.locutions.Claim;
import demoProtocol.locutions.Concede;
import demoProtocol.locutions.DemoLocution;
import demoProtocol.locutions.Question;
import demoProtocol.locutions.Retract;
import demoProtocol.locutions.Since;
import demoProtocol.locutions.Skip;
import demoProtocol.locutions.Why;
import org.hal.core.agent.AgentID; 
/**
 * The demo protocol is an adaptation of the 2 player protocol from (Prakken, 2005: "Flexiblity 
 * and coherence in dialogue games for argumentation"). In this protocol we take a strict turn 
 * taking mechanism between peers. An agent may send a skip locution in order to yield its turn. 
 * Furthermore, we have the common locutions claim, question, why, since, concede and retract. 
 * 
 * A claim asserts a proposition.
 * A question prompts an agent to make a claim.
 * A why challenges an earlier made claim (by either a claim or since locution)
 * A since locution provides a reason for an earlier made claim.
 * A concede commits an agent to something that another agent said previously.
 * A retract retracts the commitment of an agent for a particular proposition.
 * 
 * The condition of when an agent is allowed to send/receive a locution is managed by a mandate system. 
 * A mandate is interpreted as the options that the other agent must pick from, but can be empty, in 
 * which case any locution is allowed. As an example, when an agent asks a question, the other agent 
 * must answer with a claim or retract locution, and it is not allowed to respond with for instance 
 * a question itself. If an agent sends the skip locution, then this allows the agent to respond to any 
 * earlier made utterance. Technical details can be found in the template specifications which we choose 
 * to implement in the locution classes of this protocol.
 * 
 * This protocol is for demo purposes and is not proposed as the go-to protocol for P2P dialogues. 
 * It can be used as a starting point for when you want to make a new P2P protocol. Some things that 
 * might be considered when making a protocol:
 * 
 * - Ensuring that, with the exception of the first locution, a locution is always a response to an earlier 
 *   locution (coherence, which is not guaranteed in this protocol)
 * - Making commitments directed, so that an agent can be committed to P wrt peer A and not committed to P wrt peer B.
 *   In this demo protocol an agent only maintains its commitment status as the last claim/concede/retract that it made 
 *   to any of its peers.
 * - Linking locutions to argument schemes, this protocol only uses a "inference by defeasible rule scheme" and "conflict 
 *   by negation scheme". 
 * - Adding additional argument 1 effects to locutions. In this protocol when earlier a proposition P was mentioned, then as soon 
 *   as Not P is mentioned, the templates add conflict nodes. 
 *    
 *    
 *    This class implements various methods that are used by the templates, which are programmed in the locution classes.
 * @author Bas Testerink
 *
 */
public final class DemoProtocol extends ArgumentationDialogProtocol<DemoLocution> {

	public DemoProtocol(final UUID sessionId){
		super(sessionId); 

		// In this demo protocol we have that send and receive templates are the same. 
		// This is not always the case, however.
		super.addReceiveTemplate(Claim.produceTemplate());
		super.addReceiveTemplate(Concede.produceTemplate());
		super.addReceiveTemplate(Question.produceTemplate());
		super.addReceiveTemplate(Retract.produceTemplate());
		super.addReceiveTemplate(Since.produceTemplate());
		super.addReceiveTemplate(Why.produceTemplate());
		super.addReceiveTemplate(Skip.produceTemplate());
		
		super.addSendTemplate(Claim.produceTemplate());
		super.addSendTemplate(Concede.produceTemplate());
		super.addSendTemplate(Question.produceTemplate());
		super.addSendTemplate(Retract.produceTemplate());
		super.addSendTemplate(Since.produceTemplate());
		super.addSendTemplate(Why.produceTemplate());
		super.addSendTemplate(Skip.produceTemplate()); 
	}
	
	/**Parse a locution. Returns null if the locution could not be parsed. */
	public final static DemoLocution locutionParser(final String locution){
		// Check if the input must be a locution of the form "NAME Arg1 Arg2 ... Argk"
		if(locution == null) return null;
		final int firstSpace = locution.indexOf(' '); 
		final String name = firstSpace < 0 ? locution : locution.substring(0, firstSpace);
		
		// Check the options
		if(name.equalsIgnoreCase("skip")) return new Skip(); 
		else if(name.equalsIgnoreCase("since")){
			// A since locution is structured as "since Conclusion Premise1 ... Premisek"
			final String conclusion = parseProposition(locution, firstSpace + 1);
			final List<String> premises = new ArrayList<>();
			int nextIndex = firstSpace + 2 + conclusion.length();
			while(nextIndex < locution.length()){
				final String premise = parseProposition(locution, nextIndex);
				premises.add(premise);
				nextIndex += premise.length() + 1;
			} 
			return new Since(conclusion, premises.toArray(new String[premises.size()]));
		} else {
			// The rest is structured as "Name Arg"
			if(name.equalsIgnoreCase("claim")) return new Claim(parseProposition(locution, firstSpace + 1));
			else if(name.equalsIgnoreCase("concede")) return new Concede(parseProposition(locution, firstSpace + 1));
			else if(name.equalsIgnoreCase("retract")) return new Retract(parseProposition(locution, firstSpace + 1));
			else if(name.equalsIgnoreCase("question")) return new Question(parseProposition(locution, firstSpace + 1));
			else if(name.equalsIgnoreCase("why")) return new Why(parseProposition(locution, firstSpace + 1));
		}
		System.out.println("PARSE ERROR"); // TODO exception 
		return null;
	} 
	
	/** In the source string extract a proposition. A proposition is either a series of characters without 
	 * spaces such as "ABC" and "A_B_C" or it is parenthesized such as "(A B C)" and may be preceded with "not ". */
	private static final String parseProposition(final String source, final int startIndex){
		if(source == null || startIndex < 0 || startIndex >= source.length()) return null; // Can't parse the proposition
		int propositionStart = startIndex;
		if(source.startsWith("not", startIndex))
			propositionStart = startIndex + 4;
		int propositionEnd = propositionStart;
		boolean parenthesis = source.charAt(propositionStart) == '(';
		while((!parenthesis && source.charAt(propositionEnd) != ' ') || (parenthesis && source.charAt(propositionEnd - 1) != ')')){
			propositionEnd++;
			if(propositionEnd == source.length()) break;
		}
		return source.substring(startIndex, propositionEnd);
	}

	/** Produces a demo dialog graph. */
	protected final DialogGraph makeDialogGraph(final AgentID owner){
		return new DialogGraph(owner);
	}
	
	/** Makes the negated version of a proposition. P becomes not P and not P becomes P. */
	public final static String negate(final String proposition){
		if(proposition.startsWith("not ")){
			return proposition.substring(4);
		} else return "not "+proposition;
	}
	
	/** Call to ensure that in the dialog graph the agent is represented to not be committed to the given node. */
	public final static void ensureNotCommitted(final DialogGraph graph, final AgentID agent, final Node node){
		ensureCommitmentStatus(graph, agent, node, false); 
	}

	/** Call to ensure that in the dialog graph the agent is represented to be committed to the given node. */
	public final static void ensureCommitted(final DialogGraph graph, final AgentID agent, final Node node){
		ensureCommitmentStatus(graph, agent, node, true);
	}

	/** Call to ensure that in the dialog graph the agent is represented to (not) be committed to the given node. */
	private final static void ensureCommitmentStatus(final DialogGraph graph, final AgentID agent, final Node node, final boolean shouldBeCommitted){
		boolean foundCommitment = false;
		// A commitment is modeled by a linked list of commitment nodes. Uneven nodes (including the first commitment) 
		// model that the agent is now committed, even nodes model the retraction of a commitment.
		for(Edge edge : node.getIncomingEdges()){
			if(edge.getStart() instanceof CommitmentNode){
				CommitmentNode commitment = (CommitmentNode) edge.getStart();
				if(commitment.getCommittedAgent().equals(agent)){
					foundCommitment = true;
					boolean isCommitted = true; // One commitment in the linked list means that the commitment is active
					while(!commitment.getIncomingEdges().isEmpty()){ // While the end of the list is not encountered
						isCommitted = !isCommitted; // Flip the boolean
						commitment = (CommitmentNode) commitment.getIncomingEdges().get(0).getStart();
					}
					// Flip the commitment if so required
					if((!isCommitted && shouldBeCommitted)||(isCommitted && !shouldBeCommitted)){
						final CommitmentNode newCommitment = new CommitmentNode(agent, commitment);
						graph.addNode(newCommitment);
						graph.connect(newCommitment, commitment); 
					}
				}
			}
		}

		// Add new commitment if the agent was not committed yet but it should be committed
		if(!foundCommitment && shouldBeCommitted){
			final CommitmentNode newCommitment = new CommitmentNode(agent, node);
			graph.addNode(newCommitment);
			graph.connect(newCommitment, node); 
		}
	}

	/** Returns false iff there is a mandate and the locution is not part of that mandate. */
	public final static boolean mandateCheck(final argumentationProtocols.graphs.DialogGraph dialogGraph, final Argument2Node<?> messageNode){ 
		if(dialogGraph instanceof DialogGraph && messageNode.getContent() instanceof DemoLocution){
			final DialogGraph graph = (DialogGraph) dialogGraph;    
			return graph.emptyMandate(messageNode.getSender(), messageNode.getReceiver()) || graph.containedInMandate(messageNode.getSender(), messageNode.getReceiver(), (DemoLocution) messageNode.getContent()); 
		} else return false; 
	}
	
	/** For a given message (stored as an argument 2 node), check whether that message claims the proposition (i.e. it is a claim 
	 * locution or a since locution that claims the proposition). */
	public static final boolean messageClaimsProposition(final String proposition, final Argument2Node<?> node){
		if(node.getContent() instanceof Claim){
			return proposition.equals(((Claim)node.getContent()).getProposition());
		} else if(node.getContent() instanceof Since){
			// A since locution claims its premises and the major premise (the A,B=>C rule)
			Since since = (Since)node.getContent(); // TODO: handle the case where the proposition is (A,B=>C) and the rule of the since is (B,A=>C)
			for(String premise : since.getPremises())
				if(premise.equals(proposition))
					return true;
			return proposition.equals(Since.getRuleProposition(since.getConclusion(), since.getPremises()));
		}
		return false;
	} 

	/** For a given message (stored as an argument 2 node), check whether that message asks about the proposition (i.e. it is a question 
	 * locution that contains the proposition). */
	public static final boolean messageIsQuestionAbout(final String proposition, final Argument2Node<?> node){
		if(node.getContent() instanceof Question){
			return proposition.equals(((Question)node.getContent()).getProposition());
		}
		return false;
	} 

	/** For a given message (stored as an argument 2 node), check whether that message challenges the proposition (i.e. it is a why 
	 * locution that contains the proposition). */
	public static final boolean messageIsChallengeAbout(final String proposition, final Argument2Node<?> node){
		if(node.getContent() instanceof Why){
			return proposition.equals(((Why)node.getContent()).getProposition());
		}
		return false;
	} 
	 
	
}
