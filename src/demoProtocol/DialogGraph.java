package demoProtocol;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import argumentationProtocols.graphs.Argument2Node;
import argumentationProtocols.graphs.ConflictNode;
import argumentationProtocols.graphs.INode;
import argumentationProtocols.graphs.Node;
import demoProtocol.locutions.DemoLocution;
import demoProtocol.locutions.Since;
import org.hal.core.agent.AgentID;

public final class DialogGraph extends argumentationProtocols.graphs.DialogGraph { 
	private final Map<AgentID, DemoLocution[]> mandatePeersToMe, mandateMeToPeers;  
	private final Map<String, Node> propositionToINode;

	public DialogGraph(final AgentID owner) {
		super(owner); 
		this.mandatePeersToMe = new HashMap<>();
		this.mandateMeToPeers = new HashMap<>();  
		this.propositionToINode = new HashMap<>(); 
	}
	
	/** Obtain or create if necessary the i-node that holds the proposition. Will automatically add conflict nodes if 
	 * the given i-node conflicts with an earlier i-node. */
	public final Node makeAndGetINode(final String proposition){
		Node result = this.propositionToINode.get(proposition);
		if(result == null){
			result = new INode(proposition);
			this.propositionToINode.put(proposition, result);
			addNode(result);
			
			// Check for conflicting INodes that already exist (in this demo we only look at conflicts by negation, which are automatically asserted)
			final Node negatedNode = this.propositionToINode.get(DemoProtocol.negate(proposition));
			if(negatedNode != null){
				final List<ConflictNode> existing = getNodesOfType(ConflictNode.class);

				// Add two conflict nodes, since negation-conflicts always go two ways
				final ConflictNode conflictA = new ConflictNode(result, negatedNode);
				final ConflictNode conflictB = new ConflictNode(negatedNode, result);
				if(!existing.contains(conflictA)){ 
					addNode(conflictA); 
					connect(result, conflictA);
					connect(conflictA, negatedNode);
				}
				if(!existing.contains(conflictB)){ 
					addNode(conflictB); 
					connect(negatedNode, conflictB);
					connect(conflictB, result);
				}  
			} 
		}
		return result;
	} 
	
	/**Check whether the mandate is empty for a given sender-receiver pair. */
	public final boolean emptyMandate(final AgentID sender, final AgentID receiver){ 
		DemoLocution[] mandateForAgent = getMandate(sender, receiver);
		if(mandateForAgent == null) return true;
		return mandateForAgent.length == 0; 
	}
	
	/** Check whether the given locution is in the mandate for the given sender-receiver pair. */
	public final boolean containedInMandate(final AgentID sender, final AgentID receiver, final DemoLocution locution){   
		DemoLocution[] mandateForAgent = getMandate(sender, receiver);
		if(mandateForAgent != null){
			boolean found = false;
			for(DemoLocution inMandate : mandateForAgent){
				// A since locution is only in the mandate with an empty premise list, hence we only check the conclusion
				if(locution instanceof Since && inMandate instanceof Since){
					found = ((Since)locution).getConclusion().equals(((Since)inMandate).getConclusion());
				} else found = inMandate.equals(locution);
				if(found) return true;
			}
		}
		return false;
	}
	
	/** Get the mandate for a given sender-receiver pair. */
	private final DemoLocution[] getMandate(final AgentID sender, final AgentID receiver){
		if(sender.equals(this.getOwner())) return this.mandateMeToPeers.get(receiver);
		else return this.mandatePeersToMe.get(sender);
	}
	
	/** Set the mandate for a given sender-receiver pair. */
	public final void setMandate(final AgentID sender, final AgentID receiver, final DemoLocution[] locutions){
		if(sender.equals(this.getOwner())) this.mandateMeToPeers.put(receiver, locutions);
		else this.mandatePeersToMe.put(sender, locutions);
	}
	
	/** Clear the mandate for a given sender-receiver pair. */
	public final void clearMandate(final AgentID sender, final AgentID receiver){ 
		if(sender.equals(this.getOwner())) this.mandateMeToPeers.put(receiver, new DemoLocution[0]);
		else this.mandatePeersToMe.put(sender, new DemoLocution[0]);
	} 
	
	@SuppressWarnings("rawtypes")
	/** Returns the message nodes in the graph. */
	public final List<Argument2Node> getMessageNodes(){
		return getNodesOfType(Argument2Node.class); 
	} 
	

	@SuppressWarnings("unchecked")
	/**Get the nodes that are an instance of the given type.*/
	public final <T> List<T> getNodesOfType(final Class<T> klass){
		return super.nodes
			.stream()
			.filter(node -> {return klass.isInstance(node); })
			.map(node -> { return (T) node; })
			.collect(Collectors.toList()); 
	} 
	
	/** Use the demo graph converter to convert the demo graph to JSON. */
	public final StringBuffer toJSON(final Map<AgentID, String> peerNames){
		return (new DemoGraphToJSONConverter(this)).toJSON(peerNames);
	}
}
