This archive contains a demo of some communication structures that can occur in peer-to-peer multi-agent systems with arguing agents. The demonstration is not focused on providing example protocols or strategies. The protocol is a simple strict turn based system with one move per round. The agents may claim (claim p), challenge (why p), question (question p), concede (concede p), argue (p since q) and skip their turn (skip). The strategies of the agents are somewhat prescripted. 

For a video and explanation of the visualisation of the demo, see our previous demo: https://git.science.uu.nl/B.J.G.Testerink/OO2APL-P2PArgumentationDialogDemo

This file explains the different scenarios, what files are in this archive, how to setup an eclipse project to modify the demo, and how to use the demo.

Scenarios

The demo contains three scenarios: division of labor, yellow pages and public forum. 

Division of labor entails an interaction where the client agent engages in an argumentation dialog with the group node as if it is an arguing agent. The group node routes all locutions to experts who in turn argue with the group node as if it is the client. In this scenario, the client does not know who the experts are, and the experts do not know the client. All arguments are seen by the client and the group node.

Yellow pages are a system where a central administration (the yellow pages) is used to find expertise. In this demonstration, the client submits questions to the group node and waits for experts to contact it. If they do, then the client engages directly with them in an argumentation dialog. Hence, the group node only sees questions, but never sees the arguments.

A public forum is a construction where agents submit argumentation locution to the group node, but do not directly engage in an argumentation node with it. Furthermore, the group node does not reply with argumentation locutions. Instead, the agents request the public dialog graph and then make a move based on that graph. Hence, the client in this scenario has no argument 1 part in its dialog graph as it never makes arguments and never receives them. However, its belief base does contain the expert arguments as it extracts this information from the group's dialog graph.

Files

This archive consists of the following folders and files:

* Visualizer: this directory is the visualizer for dialogue graphs. Open "visualizer.html" for the main demonstration. 
* Visualizer/js/dialogGraphX.js contains the JSON dialogue graphs from the demos. The three different interactions are stored here.
* The other folders and files are part of the Eclipse project. Running a demo produces the dialog graph javascript file.

How-to run and modify

For those who are only interested in the demonstration itself, and not in the source code, one can simply open 'visualizer/visualizer.html' in a browser. Click on agents to load their view. If something is not properly loaded, then a refresh usually does the trick (we use visjs and semantic ui for the layout, hence some minor layout issues are out of our control). In the argument 2 graph, if you click on a cluster it will open it. Double clicking on a locution will cluster all the locutions between the sender and receiver of that locution.

For modifying the visualizer of the demo, change 'visualizer/visualizer.html' or 'visualizer/js/P2PArgumentationDialogDemo.js'. 

To modify the Java part, first install Eclipse (www.eclipse.org). You can then import the project via import -> existing projects into workspace. 

The project contains all source code of a simple argumentation engine, agent library and the demonstrations themselves (COMMADemoX.java, which also contain the main methods)

For any questions, please mail Bas Testerink, Bas.Testerink@politie.nl