var dialogGraphsYellowPage = {
 
"client":{
	"owner":"client",
	"agents":["group","client","expertB","expertA"],
	"argument2Graph":{
		"messages":[
			{"id":0,"sender":"client","content":"question: PA","receiver":"group"},
			{"id":1,"sender":"group","content":"skip","receiver":"client"},
			{"id":2,"sender":"expertA","content":"claim: PA","receiver":"client"},
			{"id":3,"sender":"client","content":"why: PA","receiver":"expertA"},
			{"id":4,"sender":"expertA","content":"since: (QA => PA)","receiver":"client"},
			{"id":5,"sender":"client","content":"concede: PA","receiver":"expertA"},
			{"id":6,"sender":"expertA","content":"skip","receiver":"client"},
			{"id":7,"sender":"client","content":"question: PB","receiver":"group"},
			{"id":8,"sender":"group","content":"skip","receiver":"client"},
			{"id":9,"sender":"expertB","content":"claim: PB","receiver":"client"},
			{"id":10,"sender":"client","content":"why: PB","receiver":"expertB"},
			{"id":11,"sender":"expertB","content":"since: (QB => PB)","receiver":"client"},
			{"id":12,"sender":"client","content":"concede: PB","receiver":"expertB"},
			{"id":13,"sender":"expertB","content":"skip","receiver":"client"}
		],
		"responseRelation":[
			{"start":3,"end":2},
			{"start":4,"end":3},
			{"start":5,"end":2},
			{"start":10,"end":9},
			{"start":11,"end":10},
			{"start":12,"end":9}
		]
	},
	"argument1Graph":{
		"iNodes":[
			{"id":14,"proposition":"PA"},
			{"id":15,"proposition":"QA"},
			{"id":16,"proposition":"(QA => PA)"},
			{"id":17,"proposition":"PB"},
			{"id":18,"proposition":"QB"},
			{"id":19,"proposition":"(QB => PB)"}
		],
		"sNodes":[
			{"id":20,"type":"RA"},
			{"id":21,"type":"RA"}
		],
		"argument1Edges":[
			{"id":22,"start":15,"end":20},
			{"id":23,"start":16,"end":20},
			{"id":24,"start":18,"end":21},
			{"id":25,"start":19,"end":21},
			{"id":26,"start":20,"end":14},
			{"id":27,"start":21,"end":17}
		]
	},
	"illocutionaryForce":{
		"commitmentNodes":[
			{"id":28,"owner":"expertA"},
			{"id":29,"owner":"expertA"},
			{"id":30,"owner":"expertA"},
			{"id":31,"owner":"client"},
			{"id":32,"owner":"expertB"},
			{"id":33,"owner":"expertB"},
			{"id":34,"owner":"expertB"},
			{"id":35,"owner":"client"}
		],
		"commitmentEdges":[
			{"id":36,"start":28,"end":14},
			{"id":37,"start":29,"end":15},
			{"id":38,"start":30,"end":16},
			{"id":39,"start":31,"end":14},
			{"id":40,"start":32,"end":17},
			{"id":41,"start":33,"end":18},
			{"id":42,"start":34,"end":19},
			{"id":43,"start":35,"end":17}
		],
		"effects":[
			{"message":0,"newNodes":[],"newEdges":[]},
			{"message":1,"newNodes":[],"newEdges":[]},
			{"message":2,"newNodes":[14,28],"newEdges":[36]},
			{"message":3,"newNodes":[],"newEdges":[]},
			{"message":4,"newNodes":[15,16,20,29,30],"newEdges":[22,23,26,37,38]},
			{"message":5,"newNodes":[31],"newEdges":[39]},
			{"message":6,"newNodes":[],"newEdges":[]},
			{"message":7,"newNodes":[],"newEdges":[]},
			{"message":8,"newNodes":[],"newEdges":[]},
			{"message":9,"newNodes":[17,32],"newEdges":[40]},
			{"message":10,"newNodes":[],"newEdges":[]},
			{"message":11,"newNodes":[18,19,21,33,34],"newEdges":[24,25,27,41,42]},
			{"message":12,"newNodes":[35],"newEdges":[43]},
			{"message":13,"newNodes":[],"newEdges":[]}
		]
	},
	"aif":{
		"nodes":[
			{"id":"aif:inode","text":"Information node"},
			{"id":"aif:snode","text":"Scheme application node"},
			{"id":"aif:preference","text":"Preference node"},
			{"id":"aif:rule","text":"Rule application node"},
			{"id":"aif:conflict","text":"Conflict node"},
			{"id":"aif:form","text":"Form"},
			{"id":"aif:premiseDescription","text":"Argument premise description"},
			{"id":"aif:conclusionDescription","text":"Argument conclusion description"},
			{"id":"aif:presumptionDescription","text":"Argument presumption description"},
			{"id":"aif:scheme","text":"Scheme"},
			{"id":"aif:preferenceScheme","text":"Preference scheme"},
			{"id":"aif:conflictScheme","text":"Conflict scheme"},
			{"id":"aif:ruleScheme","text":"Rule scheme"},
			{"id":"aif:deductiveRuleScheme","text":"Deductive rule scheme"},
			{"id":"aif:inductiveRuleScheme","text":"Inductive rule scheme"},
			{"id":"aif:presumptiveRuleScheme","text":"Presumptive rule scheme"},
			{"id":"schemes:negation","text":"Conflict due to negation (p vs. not p)"},
			{"id":"schemes:negationConflicted","text":"A"},
			{"id":"schemes:negationConflicting","text":"Not A"},
			{"id":"schemes:defeasible","text":"Inference by defeasible rule"},
			{"id":"schemes:defeasibleMinorPremise","text":"A"},
			{"id":"schemes:defeasibleMajorPremise","text":"A => B"},
			{"id":"schemes:defeasibleConclusion","text":"B"}
		],
		"edges":[
			{"start":"aif:preference","end":"aif:snode"},
			{"start":"aif:rule","end":"aif:snode"},
			{"start":"aif:conflict","end":"aif:snode"},
			{"start":"aif:premiseDescription","end":"aif:form"},
			{"start":"aif:conclusionDescription","end":"aif:form"},
			{"start":"aif:presumptionDescription","end":"aif:form"},
			{"start":"aif:scheme","end":"aif:form"},
			{"start":"aif:preferenceScheme","end":"aif:scheme"},
			{"start":"aif:conflictScheme","end":"aif:scheme"},
			{"start":"aif:ruleScheme","end":"aif:scheme"},
			{"start":"aif:deductiveRuleScheme","end":"aif:ruleScheme"},
			{"start":"aif:inductiveRuleScheme","end":"aif:ruleScheme"},
			{"start":"aif:presumptiveRuleScheme","end":"aif:ruleScheme"},
			{"start":"schemes:negation","end":"aif:conflictScheme"},
			{"start":"schemes:negationConflicted","end":"aif:premiseDescription"},
			{"start":"schemes:negationConflicting","end":"aif:conclusionDescription"},
			{"start":"schemes:defeasible","end":"aif:presumptiveRuleScheme"},
			{"start":"schemes:defeasibleMinorPremise","end":"aif:premiseDescription"},
			{"start":"schemes:defeasibleMajorPremise","end":"aif:premiseDescription"},
			{"start":"schemes:defeasibleConclusion","end":"aif:conclusionDescription"},
			{"start":14,"end":"aif:inode"},
			{"start":15,"end":"aif:inode"},
			{"start":16,"end":"aif:inode"},
			{"start":17,"end":"aif:inode"},
			{"start":18,"end":"aif:inode"},
			{"start":19,"end":"aif:inode"},
			{"start":20,"end":"aif:rule"},
			{"start":20,"end":"schemes:defeasible"},
			{"start":15,"end":"schemes:defeasibleMinorPremise"},
			{"start":16,"end":"schemes:defeasibleMajorPremise"},
			{"start":14,"end":"schemes:defeasibleConclusion"},
			{"start":21,"end":"aif:rule"},
			{"start":21,"end":"schemes:defeasible"},
			{"start":18,"end":"schemes:defeasibleMinorPremise"},
			{"start":19,"end":"schemes:defeasibleMajorPremise"},
			{"start":17,"end":"schemes:defeasibleConclusion"}
		]
	}
},
"group":{
	"owner":"group",
	"agents":["group","client","expertB","expertA"],
	"argument2Graph":{
		"messages":[
			{"id":0,"sender":"client","content":"question: PA","receiver":"group"},
			{"id":1,"sender":"group","content":"question: PA","receiver":"expertA"},
			{"id":2,"sender":"group","content":"question: PA","receiver":"expertB"},
			{"id":3,"sender":"group","content":"skip","receiver":"client"},
			{"id":4,"sender":"expertA","content":"skip","receiver":"group"},
			{"id":5,"sender":"expertB","content":"skip","receiver":"group"},
			{"id":6,"sender":"client","content":"question: PB","receiver":"group"},
			{"id":7,"sender":"group","content":"question: PB","receiver":"expertA"},
			{"id":8,"sender":"group","content":"question: PB","receiver":"expertB"},
			{"id":9,"sender":"group","content":"skip","receiver":"client"},
			{"id":10,"sender":"expertA","content":"skip","receiver":"group"},
			{"id":11,"sender":"expertB","content":"skip","receiver":"group"}
		],
		"responseRelation":[

		]
	},
	"argument1Graph":{
		"iNodes":[

		],
		"sNodes":[

		],
		"argument1Edges":[

		]
	},
	"illocutionaryForce":{
		"commitmentNodes":[

		],
		"commitmentEdges":[

		],
		"effects":[
			{"message":0,"newNodes":[],"newEdges":[]},
			{"message":1,"newNodes":[],"newEdges":[]},
			{"message":2,"newNodes":[],"newEdges":[]},
			{"message":3,"newNodes":[],"newEdges":[]},
			{"message":4,"newNodes":[],"newEdges":[]},
			{"message":5,"newNodes":[],"newEdges":[]},
			{"message":6,"newNodes":[],"newEdges":[]},
			{"message":7,"newNodes":[],"newEdges":[]},
			{"message":8,"newNodes":[],"newEdges":[]},
			{"message":9,"newNodes":[],"newEdges":[]},
			{"message":10,"newNodes":[],"newEdges":[]},
			{"message":11,"newNodes":[],"newEdges":[]}
		]
	},
	"aif":{
		"nodes":[
			{"id":"aif:inode","text":"Information node"},
			{"id":"aif:snode","text":"Scheme application node"},
			{"id":"aif:preference","text":"Preference node"},
			{"id":"aif:rule","text":"Rule application node"},
			{"id":"aif:conflict","text":"Conflict node"},
			{"id":"aif:form","text":"Form"},
			{"id":"aif:premiseDescription","text":"Argument premise description"},
			{"id":"aif:conclusionDescription","text":"Argument conclusion description"},
			{"id":"aif:presumptionDescription","text":"Argument presumption description"},
			{"id":"aif:scheme","text":"Scheme"},
			{"id":"aif:preferenceScheme","text":"Preference scheme"},
			{"id":"aif:conflictScheme","text":"Conflict scheme"},
			{"id":"aif:ruleScheme","text":"Rule scheme"},
			{"id":"aif:deductiveRuleScheme","text":"Deductive rule scheme"},
			{"id":"aif:inductiveRuleScheme","text":"Inductive rule scheme"},
			{"id":"aif:presumptiveRuleScheme","text":"Presumptive rule scheme"},
			{"id":"schemes:negation","text":"Conflict due to negation (p vs. not p)"},
			{"id":"schemes:negationConflicted","text":"A"},
			{"id":"schemes:negationConflicting","text":"Not A"},
			{"id":"schemes:defeasible","text":"Inference by defeasible rule"},
			{"id":"schemes:defeasibleMinorPremise","text":"A"},
			{"id":"schemes:defeasibleMajorPremise","text":"A => B"},
			{"id":"schemes:defeasibleConclusion","text":"B"}
		],
		"edges":[
			{"start":"aif:preference","end":"aif:snode"},
			{"start":"aif:rule","end":"aif:snode"},
			{"start":"aif:conflict","end":"aif:snode"},
			{"start":"aif:premiseDescription","end":"aif:form"},
			{"start":"aif:conclusionDescription","end":"aif:form"},
			{"start":"aif:presumptionDescription","end":"aif:form"},
			{"start":"aif:scheme","end":"aif:form"},
			{"start":"aif:preferenceScheme","end":"aif:scheme"},
			{"start":"aif:conflictScheme","end":"aif:scheme"},
			{"start":"aif:ruleScheme","end":"aif:scheme"},
			{"start":"aif:deductiveRuleScheme","end":"aif:ruleScheme"},
			{"start":"aif:inductiveRuleScheme","end":"aif:ruleScheme"},
			{"start":"aif:presumptiveRuleScheme","end":"aif:ruleScheme"},
			{"start":"schemes:negation","end":"aif:conflictScheme"},
			{"start":"schemes:negationConflicted","end":"aif:premiseDescription"},
			{"start":"schemes:negationConflicting","end":"aif:conclusionDescription"},
			{"start":"schemes:defeasible","end":"aif:presumptiveRuleScheme"},
			{"start":"schemes:defeasibleMinorPremise","end":"aif:premiseDescription"},
			{"start":"schemes:defeasibleMajorPremise","end":"aif:premiseDescription"},
			{"start":"schemes:defeasibleConclusion","end":"aif:conclusionDescription"},

		]
	}
},
"expertA":{
	"owner":"expertA",
	"agents":["group","client","expertA"],
	"argument2Graph":{
		"messages":[
			{"id":0,"sender":"group","content":"question: PA","receiver":"expertA"},
			{"id":1,"sender":"expertA","content":"skip","receiver":"group"},
			{"id":2,"sender":"expertA","content":"claim: PA","receiver":"client"},
			{"id":3,"sender":"client","content":"why: PA","receiver":"expertA"},
			{"id":4,"sender":"expertA","content":"since: (QA => PA)","receiver":"client"},
			{"id":5,"sender":"client","content":"concede: PA","receiver":"expertA"},
			{"id":6,"sender":"expertA","content":"skip","receiver":"client"},
			{"id":7,"sender":"group","content":"question: PB","receiver":"expertA"},
			{"id":8,"sender":"expertA","content":"skip","receiver":"group"}
		],
		"responseRelation":[
			{"start":3,"end":2},
			{"start":4,"end":3},
			{"start":5,"end":2}
		]
	},
	"argument1Graph":{
		"iNodes":[
			{"id":9,"proposition":"PA"},
			{"id":10,"proposition":"QA"},
			{"id":11,"proposition":"(QA => PA)"}
		],
		"sNodes":[
			{"id":12,"type":"RA"}
		],
		"argument1Edges":[
			{"id":13,"start":10,"end":12},
			{"id":14,"start":11,"end":12},
			{"id":15,"start":12,"end":9}
		]
	},
	"illocutionaryForce":{
		"commitmentNodes":[
			{"id":16,"owner":"expertA"},
			{"id":17,"owner":"expertA"},
			{"id":18,"owner":"expertA"},
			{"id":19,"owner":"client"}
		],
		"commitmentEdges":[
			{"id":20,"start":16,"end":9},
			{"id":21,"start":17,"end":10},
			{"id":22,"start":18,"end":11},
			{"id":23,"start":19,"end":9}
		],
		"effects":[
			{"message":0,"newNodes":[],"newEdges":[]},
			{"message":1,"newNodes":[],"newEdges":[]},
			{"message":2,"newNodes":[9,16],"newEdges":[20]},
			{"message":3,"newNodes":[],"newEdges":[]},
			{"message":4,"newNodes":[10,11,12,17,18],"newEdges":[13,14,15,21,22]},
			{"message":5,"newNodes":[19],"newEdges":[23]},
			{"message":6,"newNodes":[],"newEdges":[]},
			{"message":7,"newNodes":[],"newEdges":[]},
			{"message":8,"newNodes":[],"newEdges":[]}
		]
	},
	"aif":{
		"nodes":[
			{"id":"aif:inode","text":"Information node"},
			{"id":"aif:snode","text":"Scheme application node"},
			{"id":"aif:preference","text":"Preference node"},
			{"id":"aif:rule","text":"Rule application node"},
			{"id":"aif:conflict","text":"Conflict node"},
			{"id":"aif:form","text":"Form"},
			{"id":"aif:premiseDescription","text":"Argument premise description"},
			{"id":"aif:conclusionDescription","text":"Argument conclusion description"},
			{"id":"aif:presumptionDescription","text":"Argument presumption description"},
			{"id":"aif:scheme","text":"Scheme"},
			{"id":"aif:preferenceScheme","text":"Preference scheme"},
			{"id":"aif:conflictScheme","text":"Conflict scheme"},
			{"id":"aif:ruleScheme","text":"Rule scheme"},
			{"id":"aif:deductiveRuleScheme","text":"Deductive rule scheme"},
			{"id":"aif:inductiveRuleScheme","text":"Inductive rule scheme"},
			{"id":"aif:presumptiveRuleScheme","text":"Presumptive rule scheme"},
			{"id":"schemes:negation","text":"Conflict due to negation (p vs. not p)"},
			{"id":"schemes:negationConflicted","text":"A"},
			{"id":"schemes:negationConflicting","text":"Not A"},
			{"id":"schemes:defeasible","text":"Inference by defeasible rule"},
			{"id":"schemes:defeasibleMinorPremise","text":"A"},
			{"id":"schemes:defeasibleMajorPremise","text":"A => B"},
			{"id":"schemes:defeasibleConclusion","text":"B"}
		],
		"edges":[
			{"start":"aif:preference","end":"aif:snode"},
			{"start":"aif:rule","end":"aif:snode"},
			{"start":"aif:conflict","end":"aif:snode"},
			{"start":"aif:premiseDescription","end":"aif:form"},
			{"start":"aif:conclusionDescription","end":"aif:form"},
			{"start":"aif:presumptionDescription","end":"aif:form"},
			{"start":"aif:scheme","end":"aif:form"},
			{"start":"aif:preferenceScheme","end":"aif:scheme"},
			{"start":"aif:conflictScheme","end":"aif:scheme"},
			{"start":"aif:ruleScheme","end":"aif:scheme"},
			{"start":"aif:deductiveRuleScheme","end":"aif:ruleScheme"},
			{"start":"aif:inductiveRuleScheme","end":"aif:ruleScheme"},
			{"start":"aif:presumptiveRuleScheme","end":"aif:ruleScheme"},
			{"start":"schemes:negation","end":"aif:conflictScheme"},
			{"start":"schemes:negationConflicted","end":"aif:premiseDescription"},
			{"start":"schemes:negationConflicting","end":"aif:conclusionDescription"},
			{"start":"schemes:defeasible","end":"aif:presumptiveRuleScheme"},
			{"start":"schemes:defeasibleMinorPremise","end":"aif:premiseDescription"},
			{"start":"schemes:defeasibleMajorPremise","end":"aif:premiseDescription"},
			{"start":"schemes:defeasibleConclusion","end":"aif:conclusionDescription"},
			{"start":9,"end":"aif:inode"},
			{"start":10,"end":"aif:inode"},
			{"start":11,"end":"aif:inode"},
			{"start":12,"end":"aif:rule"},
			{"start":12,"end":"schemes:defeasible"},
			{"start":10,"end":"schemes:defeasibleMinorPremise"},
			{"start":11,"end":"schemes:defeasibleMajorPremise"},
			{"start":9,"end":"schemes:defeasibleConclusion"}
		]
	}
},
"expertB":{
	"owner":"expertB",
	"agents":["group","client","expertB"],
	"argument2Graph":{
		"messages":[
			{"id":0,"sender":"group","content":"question: PA","receiver":"expertB"},
			{"id":1,"sender":"expertB","content":"skip","receiver":"group"},
			{"id":2,"sender":"group","content":"question: PB","receiver":"expertB"},
			{"id":3,"sender":"expertB","content":"skip","receiver":"group"},
			{"id":4,"sender":"expertB","content":"claim: PB","receiver":"client"},
			{"id":5,"sender":"client","content":"why: PB","receiver":"expertB"},
			{"id":6,"sender":"expertB","content":"since: (QB => PB)","receiver":"client"},
			{"id":7,"sender":"client","content":"concede: PB","receiver":"expertB"},
			{"id":8,"sender":"expertB","content":"skip","receiver":"client"}
		],
		"responseRelation":[
			{"start":5,"end":4},
			{"start":6,"end":5},
			{"start":7,"end":4}
		]
	},
	"argument1Graph":{
		"iNodes":[
			{"id":9,"proposition":"PB"},
			{"id":10,"proposition":"QB"},
			{"id":11,"proposition":"(QB => PB)"}
		],
		"sNodes":[
			{"id":12,"type":"RA"}
		],
		"argument1Edges":[
			{"id":13,"start":10,"end":12},
			{"id":14,"start":11,"end":12},
			{"id":15,"start":12,"end":9}
		]
	},
	"illocutionaryForce":{
		"commitmentNodes":[
			{"id":16,"owner":"expertB"},
			{"id":17,"owner":"expertB"},
			{"id":18,"owner":"expertB"},
			{"id":19,"owner":"client"}
		],
		"commitmentEdges":[
			{"id":20,"start":16,"end":9},
			{"id":21,"start":17,"end":10},
			{"id":22,"start":18,"end":11},
			{"id":23,"start":19,"end":9}
		],
		"effects":[
			{"message":0,"newNodes":[],"newEdges":[]},
			{"message":1,"newNodes":[],"newEdges":[]},
			{"message":2,"newNodes":[],"newEdges":[]},
			{"message":3,"newNodes":[],"newEdges":[]},
			{"message":4,"newNodes":[9,16],"newEdges":[20]},
			{"message":5,"newNodes":[],"newEdges":[]},
			{"message":6,"newNodes":[10,11,12,17,18],"newEdges":[13,14,15,21,22]},
			{"message":7,"newNodes":[19],"newEdges":[23]},
			{"message":8,"newNodes":[],"newEdges":[]}
		]
	},
	"aif":{
		"nodes":[
			{"id":"aif:inode","text":"Information node"},
			{"id":"aif:snode","text":"Scheme application node"},
			{"id":"aif:preference","text":"Preference node"},
			{"id":"aif:rule","text":"Rule application node"},
			{"id":"aif:conflict","text":"Conflict node"},
			{"id":"aif:form","text":"Form"},
			{"id":"aif:premiseDescription","text":"Argument premise description"},
			{"id":"aif:conclusionDescription","text":"Argument conclusion description"},
			{"id":"aif:presumptionDescription","text":"Argument presumption description"},
			{"id":"aif:scheme","text":"Scheme"},
			{"id":"aif:preferenceScheme","text":"Preference scheme"},
			{"id":"aif:conflictScheme","text":"Conflict scheme"},
			{"id":"aif:ruleScheme","text":"Rule scheme"},
			{"id":"aif:deductiveRuleScheme","text":"Deductive rule scheme"},
			{"id":"aif:inductiveRuleScheme","text":"Inductive rule scheme"},
			{"id":"aif:presumptiveRuleScheme","text":"Presumptive rule scheme"},
			{"id":"schemes:negation","text":"Conflict due to negation (p vs. not p)"},
			{"id":"schemes:negationConflicted","text":"A"},
			{"id":"schemes:negationConflicting","text":"Not A"},
			{"id":"schemes:defeasible","text":"Inference by defeasible rule"},
			{"id":"schemes:defeasibleMinorPremise","text":"A"},
			{"id":"schemes:defeasibleMajorPremise","text":"A => B"},
			{"id":"schemes:defeasibleConclusion","text":"B"}
		],
		"edges":[
			{"start":"aif:preference","end":"aif:snode"},
			{"start":"aif:rule","end":"aif:snode"},
			{"start":"aif:conflict","end":"aif:snode"},
			{"start":"aif:premiseDescription","end":"aif:form"},
			{"start":"aif:conclusionDescription","end":"aif:form"},
			{"start":"aif:presumptionDescription","end":"aif:form"},
			{"start":"aif:scheme","end":"aif:form"},
			{"start":"aif:preferenceScheme","end":"aif:scheme"},
			{"start":"aif:conflictScheme","end":"aif:scheme"},
			{"start":"aif:ruleScheme","end":"aif:scheme"},
			{"start":"aif:deductiveRuleScheme","end":"aif:ruleScheme"},
			{"start":"aif:inductiveRuleScheme","end":"aif:ruleScheme"},
			{"start":"aif:presumptiveRuleScheme","end":"aif:ruleScheme"},
			{"start":"schemes:negation","end":"aif:conflictScheme"},
			{"start":"schemes:negationConflicted","end":"aif:premiseDescription"},
			{"start":"schemes:negationConflicting","end":"aif:conclusionDescription"},
			{"start":"schemes:defeasible","end":"aif:presumptiveRuleScheme"},
			{"start":"schemes:defeasibleMinorPremise","end":"aif:premiseDescription"},
			{"start":"schemes:defeasibleMajorPremise","end":"aif:premiseDescription"},
			{"start":"schemes:defeasibleConclusion","end":"aif:conclusionDescription"},
			{"start":9,"end":"aif:inode"},
			{"start":10,"end":"aif:inode"},
			{"start":11,"end":"aif:inode"},
			{"start":12,"end":"aif:rule"},
			{"start":12,"end":"schemes:defeasible"},
			{"start":10,"end":"schemes:defeasibleMinorPremise"},
			{"start":11,"end":"schemes:defeasibleMajorPremise"},
			{"start":9,"end":"schemes:defeasibleConclusion"}
		]
	}
}};
var visualization = {
    "client":{"color":"#ffc9c9"},
    "group":{"color":"#fff544","icon":{face: 'FontAwesome', code: '\uf0c0', size: 50}},
    "expertA":{"color":"#ffc889",  "icon":{face: 'FontAwesome', code: '\uf21b', size: 50}},
    "expertB":{"color":"#baff44",  "icon":{face: 'FontAwesome', code: '\uf21b', size: 50}}
};
