var dialogGraphs = {
 
"client":{
	"owner":"client",
	"agents":["group","client"],
	"argument2Graph":{
		"messages":[
			{"id":0,"sender":"group","content":"skip","receiver":"client"},
			{"id":1,"sender":"client","content":"question: PA","receiver":"group"},
			{"id":2,"sender":"group","content":"skip","receiver":"client"},
			{"id":3,"sender":"client","content":"skip","receiver":"group"},
			{"id":4,"sender":"group","content":"skip","receiver":"client"},
			{"id":5,"sender":"client","content":"why: PA","receiver":"group"},
			{"id":6,"sender":"group","content":"skip","receiver":"client"},
			{"id":7,"sender":"client","content":"skip","receiver":"group"},
			{"id":8,"sender":"group","content":"skip","receiver":"client"},
			{"id":9,"sender":"client","content":"question: PB","receiver":"group"},
			{"id":10,"sender":"group","content":"skip","receiver":"client"},
			{"id":11,"sender":"client","content":"skip","receiver":"group"},
			{"id":12,"sender":"group","content":"skip","receiver":"client"},
			{"id":13,"sender":"client","content":"why: PB","receiver":"group"},
			{"id":14,"sender":"group","content":"skip","receiver":"client"},
			{"id":15,"sender":"client","content":"skip","receiver":"group"},
			{"id":16,"sender":"group","content":"skip","receiver":"client"},
			{"id":17,"sender":"client","content":"skip","receiver":"group"}
		],
		"responseRelation":[

		]
	},
	"argument1Graph":{
		"iNodes":[

		],
		"sNodes":[

		],
		"argument1Edges":[

		]
	},
	"illocutionaryForce":{
		"commitmentNodes":[

		],
		"commitmentEdges":[

		],
		"effects":[
			{"message":0,"newNodes":[],"newEdges":[]},
			{"message":1,"newNodes":[],"newEdges":[]},
			{"message":2,"newNodes":[],"newEdges":[]},
			{"message":3,"newNodes":[],"newEdges":[]},
			{"message":4,"newNodes":[],"newEdges":[]},
			{"message":5,"newNodes":[],"newEdges":[]},
			{"message":6,"newNodes":[],"newEdges":[]},
			{"message":7,"newNodes":[],"newEdges":[]},
			{"message":8,"newNodes":[],"newEdges":[]},
			{"message":9,"newNodes":[],"newEdges":[]},
			{"message":10,"newNodes":[],"newEdges":[]},
			{"message":11,"newNodes":[],"newEdges":[]},
			{"message":12,"newNodes":[],"newEdges":[]},
			{"message":13,"newNodes":[],"newEdges":[]},
			{"message":14,"newNodes":[],"newEdges":[]},
			{"message":15,"newNodes":[],"newEdges":[]},
			{"message":16,"newNodes":[],"newEdges":[]},
			{"message":17,"newNodes":[],"newEdges":[]}
		]
	},
	"aif":{
		"nodes":[
			{"id":"aif:inode","text":"Information node"},
			{"id":"aif:snode","text":"Scheme application node"},
			{"id":"aif:preference","text":"Preference node"},
			{"id":"aif:rule","text":"Rule application node"},
			{"id":"aif:conflict","text":"Conflict node"},
			{"id":"aif:form","text":"Form"},
			{"id":"aif:premiseDescription","text":"Argument premise description"},
			{"id":"aif:conclusionDescription","text":"Argument conclusion description"},
			{"id":"aif:presumptionDescription","text":"Argument presumption description"},
			{"id":"aif:scheme","text":"Scheme"},
			{"id":"aif:preferenceScheme","text":"Preference scheme"},
			{"id":"aif:conflictScheme","text":"Conflict scheme"},
			{"id":"aif:ruleScheme","text":"Rule scheme"},
			{"id":"aif:deductiveRuleScheme","text":"Deductive rule scheme"},
			{"id":"aif:inductiveRuleScheme","text":"Inductive rule scheme"},
			{"id":"aif:presumptiveRuleScheme","text":"Presumptive rule scheme"},
			{"id":"schemes:negation","text":"Conflict due to negation (p vs. not p)"},
			{"id":"schemes:negationConflicted","text":"A"},
			{"id":"schemes:negationConflicting","text":"Not A"},
			{"id":"schemes:defeasible","text":"Inference by defeasible rule"},
			{"id":"schemes:defeasibleMinorPremise","text":"A"},
			{"id":"schemes:defeasibleMajorPremise","text":"A => B"},
			{"id":"schemes:defeasibleConclusion","text":"B"}
		],
		"edges":[
			{"start":"aif:preference","end":"aif:snode"},
			{"start":"aif:rule","end":"aif:snode"},
			{"start":"aif:conflict","end":"aif:snode"},
			{"start":"aif:premiseDescription","end":"aif:form"},
			{"start":"aif:conclusionDescription","end":"aif:form"},
			{"start":"aif:presumptionDescription","end":"aif:form"},
			{"start":"aif:scheme","end":"aif:form"},
			{"start":"aif:preferenceScheme","end":"aif:scheme"},
			{"start":"aif:conflictScheme","end":"aif:scheme"},
			{"start":"aif:ruleScheme","end":"aif:scheme"},
			{"start":"aif:deductiveRuleScheme","end":"aif:ruleScheme"},
			{"start":"aif:inductiveRuleScheme","end":"aif:ruleScheme"},
			{"start":"aif:presumptiveRuleScheme","end":"aif:ruleScheme"},
			{"start":"schemes:negation","end":"aif:conflictScheme"},
			{"start":"schemes:negationConflicted","end":"aif:premiseDescription"},
			{"start":"schemes:negationConflicting","end":"aif:conclusionDescription"},
			{"start":"schemes:defeasible","end":"aif:presumptiveRuleScheme"},
			{"start":"schemes:defeasibleMinorPremise","end":"aif:premiseDescription"},
			{"start":"schemes:defeasibleMajorPremise","end":"aif:premiseDescription"},
			{"start":"schemes:defeasibleConclusion","end":"aif:conclusionDescription"},

		]
	}
},
"group":{
	"owner":"group",
	"agents":["group","expertB","expertA","client"],
	"argument2Graph":{
		"messages":[
			{"id":0,"sender":"group","content":"skip","receiver":"client"},
			{"id":1,"sender":"client","content":"question: PA","receiver":"group"},
			{"id":2,"sender":"group","content":"skip","receiver":"expertB"},
			{"id":3,"sender":"group","content":"skip","receiver":"expertA"},
			{"id":4,"sender":"group","content":"skip","receiver":"client"},
			{"id":5,"sender":"expertB","content":"skip","receiver":"group"},
			{"id":6,"sender":"expertA","content":"claim: PA","receiver":"group"},
			{"id":7,"sender":"client","content":"skip","receiver":"group"},
			{"id":8,"sender":"group","content":"skip","receiver":"expertB"},
			{"id":9,"sender":"group","content":"skip","receiver":"expertA"},
			{"id":10,"sender":"group","content":"skip","receiver":"client"},
			{"id":11,"sender":"expertB","content":"skip","receiver":"group"},
			{"id":12,"sender":"expertA","content":"skip","receiver":"group"},
			{"id":13,"sender":"client","content":"why: PA","receiver":"group"},
			{"id":14,"sender":"group","content":"skip","receiver":"expertB"},
			{"id":15,"sender":"group","content":"skip","receiver":"expertA"},
			{"id":16,"sender":"group","content":"skip","receiver":"client"},
			{"id":17,"sender":"expertB","content":"skip","receiver":"group"},
			{"id":18,"sender":"expertA","content":"since: (QA => PA)","receiver":"group"},
			{"id":19,"sender":"client","content":"skip","receiver":"group"},
			{"id":20,"sender":"group","content":"skip","receiver":"expertB"},
			{"id":21,"sender":"group","content":"skip","receiver":"expertA"},
			{"id":22,"sender":"group","content":"skip","receiver":"client"},
			{"id":23,"sender":"expertB","content":"skip","receiver":"group"},
			{"id":24,"sender":"expertA","content":"skip","receiver":"group"},
			{"id":25,"sender":"client","content":"question: PB","receiver":"group"},
			{"id":26,"sender":"group","content":"skip","receiver":"expertB"},
			{"id":27,"sender":"group","content":"skip","receiver":"expertA"},
			{"id":28,"sender":"group","content":"skip","receiver":"client"},
			{"id":29,"sender":"expertB","content":"claim: PB","receiver":"group"},
			{"id":30,"sender":"expertA","content":"skip","receiver":"group"},
			{"id":31,"sender":"client","content":"skip","receiver":"group"},
			{"id":32,"sender":"group","content":"skip","receiver":"expertB"},
			{"id":33,"sender":"group","content":"skip","receiver":"expertA"},
			{"id":34,"sender":"group","content":"skip","receiver":"client"},
			{"id":35,"sender":"expertB","content":"skip","receiver":"group"},
			{"id":36,"sender":"expertA","content":"skip","receiver":"group"},
			{"id":37,"sender":"client","content":"why: PB","receiver":"group"},
			{"id":38,"sender":"group","content":"skip","receiver":"expertB"},
			{"id":39,"sender":"group","content":"skip","receiver":"expertA"},
			{"id":40,"sender":"group","content":"skip","receiver":"client"},
			{"id":41,"sender":"expertB","content":"since: (QB => PB)","receiver":"group"},
			{"id":42,"sender":"expertA","content":"skip","receiver":"group"},
			{"id":43,"sender":"client","content":"skip","receiver":"group"},
			{"id":44,"sender":"group","content":"skip","receiver":"expertB"},
			{"id":45,"sender":"group","content":"skip","receiver":"expertA"},
			{"id":46,"sender":"group","content":"skip","receiver":"client"},
			{"id":47,"sender":"expertB","content":"skip","receiver":"group"},
			{"id":48,"sender":"expertA","content":"skip","receiver":"group"},
			{"id":49,"sender":"client","content":"skip","receiver":"group"}
		],
		"responseRelation":[

		]
	},
	"argument1Graph":{
		"iNodes":[
			{"id":50,"proposition":"PA"},
			{"id":51,"proposition":"QA"},
			{"id":52,"proposition":"(QA => PA)"},
			{"id":53,"proposition":"PB"},
			{"id":54,"proposition":"QB"},
			{"id":55,"proposition":"(QB => PB)"}
		],
		"sNodes":[
			{"id":56,"type":"RA"},
			{"id":57,"type":"RA"}
		],
		"argument1Edges":[
			{"id":58,"start":51,"end":56},
			{"id":59,"start":52,"end":56},
			{"id":60,"start":54,"end":57},
			{"id":61,"start":55,"end":57},
			{"id":62,"start":56,"end":50},
			{"id":63,"start":57,"end":53}
		]
	},
	"illocutionaryForce":{
		"commitmentNodes":[
			{"id":64,"owner":"expertA"},
			{"id":65,"owner":"expertA"},
			{"id":66,"owner":"expertA"},
			{"id":67,"owner":"expertB"},
			{"id":68,"owner":"expertB"},
			{"id":69,"owner":"expertB"}
		],
		"commitmentEdges":[
			{"id":70,"start":64,"end":50},
			{"id":71,"start":65,"end":51},
			{"id":72,"start":66,"end":52},
			{"id":73,"start":67,"end":53},
			{"id":74,"start":68,"end":54},
			{"id":75,"start":69,"end":55}
		],
		"effects":[
			{"message":0,"newNodes":[],"newEdges":[]},
			{"message":1,"newNodes":[],"newEdges":[]},
			{"message":2,"newNodes":[],"newEdges":[]},
			{"message":3,"newNodes":[],"newEdges":[]},
			{"message":4,"newNodes":[],"newEdges":[]},
			{"message":5,"newNodes":[],"newEdges":[]},
			{"message":6,"newNodes":[50,64],"newEdges":[70]},
			{"message":7,"newNodes":[],"newEdges":[]},
			{"message":8,"newNodes":[],"newEdges":[]},
			{"message":9,"newNodes":[],"newEdges":[]},
			{"message":10,"newNodes":[],"newEdges":[]},
			{"message":11,"newNodes":[],"newEdges":[]},
			{"message":12,"newNodes":[],"newEdges":[]},
			{"message":13,"newNodes":[],"newEdges":[]},
			{"message":14,"newNodes":[],"newEdges":[]},
			{"message":15,"newNodes":[],"newEdges":[]},
			{"message":16,"newNodes":[],"newEdges":[]},
			{"message":17,"newNodes":[],"newEdges":[]},
			{"message":18,"newNodes":[51,52,56,65,66],"newEdges":[58,59,62,71,72]},
			{"message":19,"newNodes":[],"newEdges":[]},
			{"message":20,"newNodes":[],"newEdges":[]},
			{"message":21,"newNodes":[],"newEdges":[]},
			{"message":22,"newNodes":[],"newEdges":[]},
			{"message":23,"newNodes":[],"newEdges":[]},
			{"message":24,"newNodes":[],"newEdges":[]},
			{"message":25,"newNodes":[],"newEdges":[]},
			{"message":26,"newNodes":[],"newEdges":[]},
			{"message":27,"newNodes":[],"newEdges":[]},
			{"message":28,"newNodes":[],"newEdges":[]},
			{"message":29,"newNodes":[53,67],"newEdges":[73]},
			{"message":30,"newNodes":[],"newEdges":[]},
			{"message":31,"newNodes":[],"newEdges":[]},
			{"message":32,"newNodes":[],"newEdges":[]},
			{"message":33,"newNodes":[],"newEdges":[]},
			{"message":34,"newNodes":[],"newEdges":[]},
			{"message":35,"newNodes":[],"newEdges":[]},
			{"message":36,"newNodes":[],"newEdges":[]},
			{"message":37,"newNodes":[],"newEdges":[]},
			{"message":38,"newNodes":[],"newEdges":[]},
			{"message":39,"newNodes":[],"newEdges":[]},
			{"message":40,"newNodes":[],"newEdges":[]},
			{"message":41,"newNodes":[54,55,57,68,69],"newEdges":[60,61,63,74,75]},
			{"message":42,"newNodes":[],"newEdges":[]},
			{"message":43,"newNodes":[],"newEdges":[]},
			{"message":44,"newNodes":[],"newEdges":[]},
			{"message":45,"newNodes":[],"newEdges":[]},
			{"message":46,"newNodes":[],"newEdges":[]},
			{"message":47,"newNodes":[],"newEdges":[]},
			{"message":48,"newNodes":[],"newEdges":[]},
			{"message":49,"newNodes":[],"newEdges":[]}
		]
	},
	"aif":{
		"nodes":[
			{"id":"aif:inode","text":"Information node"},
			{"id":"aif:snode","text":"Scheme application node"},
			{"id":"aif:preference","text":"Preference node"},
			{"id":"aif:rule","text":"Rule application node"},
			{"id":"aif:conflict","text":"Conflict node"},
			{"id":"aif:form","text":"Form"},
			{"id":"aif:premiseDescription","text":"Argument premise description"},
			{"id":"aif:conclusionDescription","text":"Argument conclusion description"},
			{"id":"aif:presumptionDescription","text":"Argument presumption description"},
			{"id":"aif:scheme","text":"Scheme"},
			{"id":"aif:preferenceScheme","text":"Preference scheme"},
			{"id":"aif:conflictScheme","text":"Conflict scheme"},
			{"id":"aif:ruleScheme","text":"Rule scheme"},
			{"id":"aif:deductiveRuleScheme","text":"Deductive rule scheme"},
			{"id":"aif:inductiveRuleScheme","text":"Inductive rule scheme"},
			{"id":"aif:presumptiveRuleScheme","text":"Presumptive rule scheme"},
			{"id":"schemes:negation","text":"Conflict due to negation (p vs. not p)"},
			{"id":"schemes:negationConflicted","text":"A"},
			{"id":"schemes:negationConflicting","text":"Not A"},
			{"id":"schemes:defeasible","text":"Inference by defeasible rule"},
			{"id":"schemes:defeasibleMinorPremise","text":"A"},
			{"id":"schemes:defeasibleMajorPremise","text":"A => B"},
			{"id":"schemes:defeasibleConclusion","text":"B"}
		],
		"edges":[
			{"start":"aif:preference","end":"aif:snode"},
			{"start":"aif:rule","end":"aif:snode"},
			{"start":"aif:conflict","end":"aif:snode"},
			{"start":"aif:premiseDescription","end":"aif:form"},
			{"start":"aif:conclusionDescription","end":"aif:form"},
			{"start":"aif:presumptionDescription","end":"aif:form"},
			{"start":"aif:scheme","end":"aif:form"},
			{"start":"aif:preferenceScheme","end":"aif:scheme"},
			{"start":"aif:conflictScheme","end":"aif:scheme"},
			{"start":"aif:ruleScheme","end":"aif:scheme"},
			{"start":"aif:deductiveRuleScheme","end":"aif:ruleScheme"},
			{"start":"aif:inductiveRuleScheme","end":"aif:ruleScheme"},
			{"start":"aif:presumptiveRuleScheme","end":"aif:ruleScheme"},
			{"start":"schemes:negation","end":"aif:conflictScheme"},
			{"start":"schemes:negationConflicted","end":"aif:premiseDescription"},
			{"start":"schemes:negationConflicting","end":"aif:conclusionDescription"},
			{"start":"schemes:defeasible","end":"aif:presumptiveRuleScheme"},
			{"start":"schemes:defeasibleMinorPremise","end":"aif:premiseDescription"},
			{"start":"schemes:defeasibleMajorPremise","end":"aif:premiseDescription"},
			{"start":"schemes:defeasibleConclusion","end":"aif:conclusionDescription"},
			{"start":50,"end":"aif:inode"},
			{"start":51,"end":"aif:inode"},
			{"start":52,"end":"aif:inode"},
			{"start":53,"end":"aif:inode"},
			{"start":54,"end":"aif:inode"},
			{"start":55,"end":"aif:inode"},
			{"start":56,"end":"aif:rule"},
			{"start":56,"end":"schemes:defeasible"},
			{"start":51,"end":"schemes:defeasibleMinorPremise"},
			{"start":52,"end":"schemes:defeasibleMajorPremise"},
			{"start":50,"end":"schemes:defeasibleConclusion"},
			{"start":57,"end":"aif:rule"},
			{"start":57,"end":"schemes:defeasible"},
			{"start":54,"end":"schemes:defeasibleMinorPremise"},
			{"start":55,"end":"schemes:defeasibleMajorPremise"},
			{"start":53,"end":"schemes:defeasibleConclusion"}
		]
	}
},
"expertA":{
	"owner":"expertA",
	"agents":["group","expertA"],
	"argument2Graph":{
		"messages":[
			{"id":0,"sender":"group","content":"skip","receiver":"expertA"},
			{"id":1,"sender":"expertA","content":"claim: PA","receiver":"group"},
			{"id":2,"sender":"group","content":"skip","receiver":"expertA"},
			{"id":3,"sender":"expertA","content":"skip","receiver":"group"},
			{"id":4,"sender":"group","content":"skip","receiver":"expertA"},
			{"id":5,"sender":"expertA","content":"since: (QA => PA)","receiver":"group"},
			{"id":6,"sender":"group","content":"skip","receiver":"expertA"},
			{"id":7,"sender":"expertA","content":"skip","receiver":"group"},
			{"id":8,"sender":"group","content":"skip","receiver":"expertA"},
			{"id":9,"sender":"expertA","content":"skip","receiver":"group"},
			{"id":10,"sender":"group","content":"skip","receiver":"expertA"},
			{"id":11,"sender":"expertA","content":"skip","receiver":"group"},
			{"id":12,"sender":"group","content":"skip","receiver":"expertA"},
			{"id":13,"sender":"expertA","content":"skip","receiver":"group"},
			{"id":14,"sender":"group","content":"skip","receiver":"expertA"},
			{"id":15,"sender":"expertA","content":"skip","receiver":"group"}
		],
		"responseRelation":[

		]
	},
	"argument1Graph":{
		"iNodes":[
			{"id":16,"proposition":"PA"},
			{"id":17,"proposition":"QA"},
			{"id":18,"proposition":"(QA => PA)"}
		],
		"sNodes":[
			{"id":19,"type":"RA"}
		],
		"argument1Edges":[
			{"id":20,"start":17,"end":19},
			{"id":21,"start":18,"end":19},
			{"id":22,"start":19,"end":16}
		]
	},
	"illocutionaryForce":{
		"commitmentNodes":[
			{"id":23,"owner":"expertA"},
			{"id":24,"owner":"expertA"},
			{"id":25,"owner":"expertA"}
		],
		"commitmentEdges":[
			{"id":26,"start":23,"end":16},
			{"id":27,"start":24,"end":17},
			{"id":28,"start":25,"end":18}
		],
		"effects":[
			{"message":0,"newNodes":[],"newEdges":[]},
			{"message":1,"newNodes":[16,23],"newEdges":[26]},
			{"message":2,"newNodes":[],"newEdges":[]},
			{"message":3,"newNodes":[],"newEdges":[]},
			{"message":4,"newNodes":[],"newEdges":[]},
			{"message":5,"newNodes":[17,18,19,24,25],"newEdges":[20,21,22,27,28]},
			{"message":6,"newNodes":[],"newEdges":[]},
			{"message":7,"newNodes":[],"newEdges":[]},
			{"message":8,"newNodes":[],"newEdges":[]},
			{"message":9,"newNodes":[],"newEdges":[]},
			{"message":10,"newNodes":[],"newEdges":[]},
			{"message":11,"newNodes":[],"newEdges":[]},
			{"message":12,"newNodes":[],"newEdges":[]},
			{"message":13,"newNodes":[],"newEdges":[]},
			{"message":14,"newNodes":[],"newEdges":[]},
			{"message":15,"newNodes":[],"newEdges":[]}
		]
	},
	"aif":{
		"nodes":[
			{"id":"aif:inode","text":"Information node"},
			{"id":"aif:snode","text":"Scheme application node"},
			{"id":"aif:preference","text":"Preference node"},
			{"id":"aif:rule","text":"Rule application node"},
			{"id":"aif:conflict","text":"Conflict node"},
			{"id":"aif:form","text":"Form"},
			{"id":"aif:premiseDescription","text":"Argument premise description"},
			{"id":"aif:conclusionDescription","text":"Argument conclusion description"},
			{"id":"aif:presumptionDescription","text":"Argument presumption description"},
			{"id":"aif:scheme","text":"Scheme"},
			{"id":"aif:preferenceScheme","text":"Preference scheme"},
			{"id":"aif:conflictScheme","text":"Conflict scheme"},
			{"id":"aif:ruleScheme","text":"Rule scheme"},
			{"id":"aif:deductiveRuleScheme","text":"Deductive rule scheme"},
			{"id":"aif:inductiveRuleScheme","text":"Inductive rule scheme"},
			{"id":"aif:presumptiveRuleScheme","text":"Presumptive rule scheme"},
			{"id":"schemes:negation","text":"Conflict due to negation (p vs. not p)"},
			{"id":"schemes:negationConflicted","text":"A"},
			{"id":"schemes:negationConflicting","text":"Not A"},
			{"id":"schemes:defeasible","text":"Inference by defeasible rule"},
			{"id":"schemes:defeasibleMinorPremise","text":"A"},
			{"id":"schemes:defeasibleMajorPremise","text":"A => B"},
			{"id":"schemes:defeasibleConclusion","text":"B"}
		],
		"edges":[
			{"start":"aif:preference","end":"aif:snode"},
			{"start":"aif:rule","end":"aif:snode"},
			{"start":"aif:conflict","end":"aif:snode"},
			{"start":"aif:premiseDescription","end":"aif:form"},
			{"start":"aif:conclusionDescription","end":"aif:form"},
			{"start":"aif:presumptionDescription","end":"aif:form"},
			{"start":"aif:scheme","end":"aif:form"},
			{"start":"aif:preferenceScheme","end":"aif:scheme"},
			{"start":"aif:conflictScheme","end":"aif:scheme"},
			{"start":"aif:ruleScheme","end":"aif:scheme"},
			{"start":"aif:deductiveRuleScheme","end":"aif:ruleScheme"},
			{"start":"aif:inductiveRuleScheme","end":"aif:ruleScheme"},
			{"start":"aif:presumptiveRuleScheme","end":"aif:ruleScheme"},
			{"start":"schemes:negation","end":"aif:conflictScheme"},
			{"start":"schemes:negationConflicted","end":"aif:premiseDescription"},
			{"start":"schemes:negationConflicting","end":"aif:conclusionDescription"},
			{"start":"schemes:defeasible","end":"aif:presumptiveRuleScheme"},
			{"start":"schemes:defeasibleMinorPremise","end":"aif:premiseDescription"},
			{"start":"schemes:defeasibleMajorPremise","end":"aif:premiseDescription"},
			{"start":"schemes:defeasibleConclusion","end":"aif:conclusionDescription"},
			{"start":16,"end":"aif:inode"},
			{"start":17,"end":"aif:inode"},
			{"start":18,"end":"aif:inode"},
			{"start":19,"end":"aif:rule"},
			{"start":19,"end":"schemes:defeasible"},
			{"start":17,"end":"schemes:defeasibleMinorPremise"},
			{"start":18,"end":"schemes:defeasibleMajorPremise"},
			{"start":16,"end":"schemes:defeasibleConclusion"}
		]
	}
},
"expertB":{
	"owner":"expertB",
	"agents":["group","expertB"],
	"argument2Graph":{
		"messages":[
			{"id":0,"sender":"group","content":"skip","receiver":"expertB"},
			{"id":1,"sender":"expertB","content":"skip","receiver":"group"},
			{"id":2,"sender":"group","content":"skip","receiver":"expertB"},
			{"id":3,"sender":"expertB","content":"skip","receiver":"group"},
			{"id":4,"sender":"group","content":"skip","receiver":"expertB"},
			{"id":5,"sender":"expertB","content":"skip","receiver":"group"},
			{"id":6,"sender":"group","content":"skip","receiver":"expertB"},
			{"id":7,"sender":"expertB","content":"skip","receiver":"group"},
			{"id":8,"sender":"group","content":"skip","receiver":"expertB"},
			{"id":9,"sender":"expertB","content":"claim: PB","receiver":"group"},
			{"id":10,"sender":"group","content":"skip","receiver":"expertB"},
			{"id":11,"sender":"expertB","content":"skip","receiver":"group"},
			{"id":12,"sender":"group","content":"skip","receiver":"expertB"},
			{"id":13,"sender":"expertB","content":"since: (QB => PB)","receiver":"group"},
			{"id":14,"sender":"group","content":"skip","receiver":"expertB"},
			{"id":15,"sender":"expertB","content":"skip","receiver":"group"}
		],
		"responseRelation":[

		]
	},
	"argument1Graph":{
		"iNodes":[
			{"id":16,"proposition":"PB"},
			{"id":17,"proposition":"QB"},
			{"id":18,"proposition":"(QB => PB)"}
		],
		"sNodes":[
			{"id":19,"type":"RA"}
		],
		"argument1Edges":[
			{"id":20,"start":17,"end":19},
			{"id":21,"start":18,"end":19},
			{"id":22,"start":19,"end":16}
		]
	},
	"illocutionaryForce":{
		"commitmentNodes":[
			{"id":23,"owner":"expertB"},
			{"id":24,"owner":"expertB"},
			{"id":25,"owner":"expertB"}
		],
		"commitmentEdges":[
			{"id":26,"start":23,"end":16},
			{"id":27,"start":24,"end":17},
			{"id":28,"start":25,"end":18}
		],
		"effects":[
			{"message":0,"newNodes":[],"newEdges":[]},
			{"message":1,"newNodes":[],"newEdges":[]},
			{"message":2,"newNodes":[],"newEdges":[]},
			{"message":3,"newNodes":[],"newEdges":[]},
			{"message":4,"newNodes":[],"newEdges":[]},
			{"message":5,"newNodes":[],"newEdges":[]},
			{"message":6,"newNodes":[],"newEdges":[]},
			{"message":7,"newNodes":[],"newEdges":[]},
			{"message":8,"newNodes":[],"newEdges":[]},
			{"message":9,"newNodes":[16,23],"newEdges":[26]},
			{"message":10,"newNodes":[],"newEdges":[]},
			{"message":11,"newNodes":[],"newEdges":[]},
			{"message":12,"newNodes":[],"newEdges":[]},
			{"message":13,"newNodes":[17,18,19,24,25],"newEdges":[20,21,22,27,28]},
			{"message":14,"newNodes":[],"newEdges":[]},
			{"message":15,"newNodes":[],"newEdges":[]}
		]
	},
	"aif":{
		"nodes":[
			{"id":"aif:inode","text":"Information node"},
			{"id":"aif:snode","text":"Scheme application node"},
			{"id":"aif:preference","text":"Preference node"},
			{"id":"aif:rule","text":"Rule application node"},
			{"id":"aif:conflict","text":"Conflict node"},
			{"id":"aif:form","text":"Form"},
			{"id":"aif:premiseDescription","text":"Argument premise description"},
			{"id":"aif:conclusionDescription","text":"Argument conclusion description"},
			{"id":"aif:presumptionDescription","text":"Argument presumption description"},
			{"id":"aif:scheme","text":"Scheme"},
			{"id":"aif:preferenceScheme","text":"Preference scheme"},
			{"id":"aif:conflictScheme","text":"Conflict scheme"},
			{"id":"aif:ruleScheme","text":"Rule scheme"},
			{"id":"aif:deductiveRuleScheme","text":"Deductive rule scheme"},
			{"id":"aif:inductiveRuleScheme","text":"Inductive rule scheme"},
			{"id":"aif:presumptiveRuleScheme","text":"Presumptive rule scheme"},
			{"id":"schemes:negation","text":"Conflict due to negation (p vs. not p)"},
			{"id":"schemes:negationConflicted","text":"A"},
			{"id":"schemes:negationConflicting","text":"Not A"},
			{"id":"schemes:defeasible","text":"Inference by defeasible rule"},
			{"id":"schemes:defeasibleMinorPremise","text":"A"},
			{"id":"schemes:defeasibleMajorPremise","text":"A => B"},
			{"id":"schemes:defeasibleConclusion","text":"B"}
		],
		"edges":[
			{"start":"aif:preference","end":"aif:snode"},
			{"start":"aif:rule","end":"aif:snode"},
			{"start":"aif:conflict","end":"aif:snode"},
			{"start":"aif:premiseDescription","end":"aif:form"},
			{"start":"aif:conclusionDescription","end":"aif:form"},
			{"start":"aif:presumptionDescription","end":"aif:form"},
			{"start":"aif:scheme","end":"aif:form"},
			{"start":"aif:preferenceScheme","end":"aif:scheme"},
			{"start":"aif:conflictScheme","end":"aif:scheme"},
			{"start":"aif:ruleScheme","end":"aif:scheme"},
			{"start":"aif:deductiveRuleScheme","end":"aif:ruleScheme"},
			{"start":"aif:inductiveRuleScheme","end":"aif:ruleScheme"},
			{"start":"aif:presumptiveRuleScheme","end":"aif:ruleScheme"},
			{"start":"schemes:negation","end":"aif:conflictScheme"},
			{"start":"schemes:negationConflicted","end":"aif:premiseDescription"},
			{"start":"schemes:negationConflicting","end":"aif:conclusionDescription"},
			{"start":"schemes:defeasible","end":"aif:presumptiveRuleScheme"},
			{"start":"schemes:defeasibleMinorPremise","end":"aif:premiseDescription"},
			{"start":"schemes:defeasibleMajorPremise","end":"aif:premiseDescription"},
			{"start":"schemes:defeasibleConclusion","end":"aif:conclusionDescription"},
			{"start":16,"end":"aif:inode"},
			{"start":17,"end":"aif:inode"},
			{"start":18,"end":"aif:inode"},
			{"start":19,"end":"aif:rule"},
			{"start":19,"end":"schemes:defeasible"},
			{"start":17,"end":"schemes:defeasibleMinorPremise"},
			{"start":18,"end":"schemes:defeasibleMajorPremise"},
			{"start":16,"end":"schemes:defeasibleConclusion"}
		]
	}
}};
var visualization = {
    "client":{"color":"#ffc9c9"},
    "group":{"color":"#fff544","icon":{face: 'FontAwesome', code: '\uf0c0', size: 50}},
    "expertA":{"color":"#ffc889",  "icon":{face: 'FontAwesome', code: '\uf21b', size: 50}},
    "expertB":{"color":"#baff44",  "icon":{face: 'FontAwesome', code: '\uf21b', size: 50}}
};
