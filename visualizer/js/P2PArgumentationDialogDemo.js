// Author: Bas Testerink
// This file is structured based on the components that are shown in the interface. 

function setDialogGraph(name){
	if(name == "YellowPage"){
		dialogGraphs = dialogGraphsYellowPage;
	} else if(name == "Division"){
		dialogGraphs = dialogGraphsDivision;
	} else if(name == "Forum"){
		dialogGraphs = dialogGraphsForum;
	}
	makeConnectivityGraph();
}

// Load the various data when an agent is selected in the overview
function onAgentSelect(agent){ 
	dialogGraph = dialogGraphs[agent];
	redrawTimeline();
	drawArgument2();
	argument2Graph.fit();
	argument2Graph.stabilize();
	drawArgument1();
	argument1Graph.fit();
	argument1Graph.stabilize();
	prepareIllocutionaryForce();
	drawAIF();
	aifGraph.fit();
	aifGraph.stabilize();
}



//////////////////////////
/// CONNECTIVITY GRAPH ///
//////////////////////////
// The data containers
var connectivityNodes;  
var connectivityEdges;  
// Layout options
var connectivityGraphOptions = {nodes:{shadow:true},edges:{shadow:true}};  
var connectivityGraph;

// Make the graph on the top of the screen that allows the user to select an agent to view.
function makeConnectivityGraph(){ 
	// Setup the css classes for the agents
	var css = document.createElement("style");
	css.type = "text/css";
	for(agent in visualization){
		if(visualization.hasOwnProperty(agent) && "color" in visualization[agent]){ 
			css.innerHTML += ".vis-item."+agent.replace(/\s/g, '')+" {border-color: black; background-color: "+visualization[agent].color+"; box-shadow: 2px 2px 3px 0px rgba(0,0,0,0.75);}\r\n";
		}
	}
	document.body.appendChild(css);

	// Make the graph itself
	connectivityNodes = new vis.DataSet();  
	connectivityEdges = new vis.DataSet(); 
	var data = {
		nodes: connectivityNodes,
		edges: connectivityEdges
	};
	var container = document.getElementById('connectivity');
	$('#connectivity').html('');
	connectivityGraph = new vis.Network(container, data, argument2GraphOptions); 
	connectivityGraph.on("selectNode", function (params) {
        onAgentSelect(params.nodes[0]);
    });
	var madeEdges = new Object();
	var encountered = [];

	// Assert the nodes and edges
	for(agent in dialogGraphs){
		if(dialogGraphs.hasOwnProperty(agent)){
			if(!encountered.includes(agent)) {
				addConnectivityElement(connectivityNodes, agent);
				encountered.push(agent);
			}
			dialogGraphs[agent].agents.forEach(function(peer){
				if(!encountered.includes(peer)){ 
					addConnectivityElement(connectivityNodes, peer);
					encountered.push(peer);
				}
				var makeEdge = peer != agent;
				if(peer in madeEdges){
					makeEdge = makeEdge && !madeEdges[peer].includes(agent);
				}
				if(agent in madeEdges){
					makeEdge = makeEdge && !madeEdges[agent].includes(peer);
				} else madeEdges[agent] = [];
				if(makeEdge){ 
					connectivityEdges.add({from:agent, to: peer, arrows:{from: true, to: true}});
					madeEdges[agent].push(peer);
				}
			});
		}
	} 
}

// Assert an agent to the overview
function addConnectivityElement(nodes, id){
	try{
		var color = '#000000'; 
		var icon = {
              face: 'FontAwesome',
              code: '\uf007',
              size: 50,
              color: color
            };
		if(id in visualization){
			color = ("color" in visualization[id]) ? visualization[id].color : color;  
			icon = ("icon" in visualization[id]) ? visualization[id].icon : icon;  
			icon.color = color;
		} 
		nodes.add({
			id: id,
			label: id,
            color: color,
			shape: 'icon',
            icon: icon
		});
	}catch (err) { console.log(err);
		//
	}
}

///////////////////////////
/// THE DIALOG TIMELINE ///
///////////////////////////

// The timeline item
var timeline;
// The items that are currently on the timeline
var dataSetTimelineItems;	 
// The timeline configuration 
var timelineoptions = {
	"showCurrentTime": false,
	"autoResize": false,
	maxHeight: "500px",
	minHeight: "500px",
	start: (new Date(Date.now())),
	end: (new Date(Date.now() + 1000 * 100))
};
// Amount of time between locutions on the timeline
var deltaTimeAmongLocutions = 20; 

// Draw the timeline that shows for an agent what messages it sent and received
function redrawTimeline(){ 
	// Make the time line itself
	dataSetTimelineItems = new vis.DataSet();	 
	var container = document.getElementById('dialog');
	$('#dialog').html('');
	timeline = new vis.Timeline(container, dataSetTimelineItems, timelineoptions); 

	// Add the messages
	var time = 0;
	dialogGraph.argument2Graph.messages.forEach(function(message){
		time += deltaTimeAmongLocutions;
		addTimeLineMessage(message.id, message.sender, message.receiver,message.content, time);
	});

	// End with the error if one is present
    if("error" in dialogGraph){
        addTimeLineError(dialogGraph.owner, dialogGraph.error.reason, time + deltaTimeAmongLocutions);
    }
} 

// Add a locution to the time line
function addTimeLineMessage(messageId, sender, receiver, message, time){
	var id = sender+time;  
	var msg = {
		"id": id,
		"content": "<i class='comment icon'></i>"+messageId+" "+sender+": (to "+receiver+") "+message,
		"start": (new Date(timelineoptions.start)).setSeconds(timelineoptions.start.getSeconds()+time), 
		"icon": "<i class='massive comment outline icon'></i>",
		"text": "text",
		"header": "header",
		"time": 1, 
		"className": (sender in visualization && visualization[sender].hasOwnProperty("color")) ? sender.replace(/\s/g, '') : "defaultClass"
	}
	dataSetTimelineItems.add(msg); 
}

// Add an error to the time line
function addTimeLineError(sender, message, time){
	var id = sender+time;
	var msg = {
		"id": id,
		"content": "<i class='warning sign icon'></i>"+sender+": "+message,
		"start": (new Date(timelineoptions.start)).setSeconds(timelineoptions.start.getSeconds()+time), 
		"icon": "<i class='massive warning sign icon'></i>",
		"text": "text",
		"header": "header",
		"time": 1,
		"className": "error"	
	}
	dataSetTimelineItems.add(msg); 
}

//////////////////
/// ARGUMENT 2 ///
//////////////////

// The data containers
var argument2Nodes;  
var argument2Edges;  
// Layout options
var argument2GraphOptions = {nodes:{shadow:true},edges:{shadow:true}};  
var argument2Graph;

// Draw the graph that shows the coherence among locutions
function drawArgument2(){
	// Make the graph itself
	argument2Nodes = new vis.DataSet();  
	argument2Edges = new vis.DataSet(); 
	var data = {
		nodes: argument2Nodes,
		edges: argument2Edges
	};
	var container = document.getElementById('argument2');
	$('#argument2').html('');
	argument2Graph = new vis.Network(container, data, argument2GraphOptions); 
	
	// Add the nodes and edges
	dialogGraph.argument2Graph.messages.forEach(function(message){ 
		addArgument2Element(argument2Nodes, message.id, message.sender, message.receiver,'('+message.id+','+message.sender+','+message.content+','+message.receiver+')');
	});

	dialogGraph.argument2Graph.responseRelation.forEach(function(pair){ 
		argument2Edges.add({from:pair.end, to: pair.start, arrows:{to: true}});
	});

	// Cluster nodes and edges per agent-peer pair
	argument2Graph.on("selectNode", function(params) {
		if (params.nodes.length == 1) {
			if (argument2Graph.isCluster(params.nodes[0])) {
				argument2Graph.openCluster(params.nodes[0]);
			}
		}
	});
	argument2Graph.on("doubleClick", function(params) { 
		if (params.nodes.length == 1) {
			if(!argument2Graph.isCluster(params.nodes[0])) { 
				var node = argument2Nodes.get(params.nodes[0]);
				clusterArg2(node.sender,node.receiver);
			}
		}
	});

	// Make the initial clusters
	dialogGraph.agents.forEach(function(agent){
		clusterArg2(dialogGraph.owner, agent);
	}); 
}

// Add a locution to the argument 2 graph
function addArgument2Element(nodes, id, sender, receiver, label){
	try{
		nodes.add({
			id: id,
			sender: sender,
			receiver: receiver,
			label: label, 
			//color: color,
			shape: 'box'
		});
	}catch (err) { console.log(err);
		//
	}
}

// For a given agent pair, make the cluster of their messages
function clusterArg2(agentA, agentB){ 
	var clusterOptions = {
		joinCondition : function(childOptions) {
			return (childOptions.sender == agentA && childOptions.receiver == agentB) || 
				(childOptions.sender == agentB && childOptions.receiver == agentA);
		},
		clusterNodeProperties: {label: agentA+','+agentB, id:'cluster-'+agentA+"-"+agentB, shape: "icon", icon:{face: 'FontAwesome', code: '\uf086', size: 50}}
	};
	argument2Graph.cluster(clusterOptions);
}
 
//////////////////
/// ARGUMENT 1 ///
//////////////////
// The data containers
var argument1Nodes;  
var argument1Edges;  
// Layout options
var argument1GraphOptions = {nodes:{shadow:true},edges:{shadow:true}};  
var argument1Graph;
// The commitments 
var commitNodes; 	
var iNodeToCommitList; // double map: iNodeToCommitList[iNodeID][AgentId] = first node of commit list

// Draw the graph that captures the argument which has been made and the commitments
function drawArgument1(){
	// idToX is required for the illocutionary force graphs
	idToNode = new Object();
	idToEdge = new Object();

	// Containers for the argument 1 graph
	argument1Nodes = new vis.DataSet();  
	argument1Edges = new vis.DataSet(); 
	commitNodes = new Object(); 	
	iNodeToCommitList = new Object(); // double map: iNodeToCommitList[iNodeID][AgentId] = first node of commit list
	var data = {
		nodes: argument1Nodes,
		edges: argument1Edges
	};
	var container = document.getElementById('argument1');
	$('#argument1').html('');
	argument1Graph = new vis.Network(container, data, argument1GraphOptions); 
	 
	// Do the bookkeeping for the commitments
	dialogGraph.illocutionaryForce.commitmentNodes.forEach(function(commitmentNode){ 
		commitNodes[commitmentNode.id] = {owner: commitmentNode.owner};
	});
	dialogGraph.illocutionaryForce.commitmentEdges.forEach(function(edge){ 
		commitNodes[edge.start].next = edge.end;
		if(edge.end in commitNodes){ 
			commitNodes[edge.end].previous = edge.start;
		} else {
			if(!(edge.end in iNodeToCommitList)){
				iNodeToCommitList[edge.end] = new Object();
			}
			iNodeToCommitList[edge.end][commitNodes[edge.start].owner] = edge.start;
		}
	});

	// Add the arg1 elements
	dialogGraph.argument1Graph.iNodes.forEach(function(iNode){ 
		addArgument1Element(argument1Nodes, iNode.id, '('+iNode.id+','+iNode.proposition+')', 'iNode');
		dialogGraph.agents.forEach(function(agent){
			commitNodeId = iNode.id+agent;
			if(isCommitted(agent, iNode.id)){
				addArgument1Element(argument1Nodes, commitNodeId, agent, 'committed');
			} else {
				addArgument1Element(argument1Nodes, commitNodeId, agent, 'not committed');
			}
			argument1Edges.add({from:commitNodeId, to:  iNode.id});
		});
	});

	dialogGraph.argument1Graph.sNodes.forEach(function(sNode){ 
		addArgument1Element(argument1Nodes, sNode.id, '('+sNode.id+','+sNode.type+')', 'sNode');
	});

	dialogGraph.argument1Graph.argument1Edges.forEach(function(pair){ 
		addArgument1Edge(pair.id, pair.start, pair.end);
	});
}

// Go through the linked list of an i-node to see if the given agent is committed to that node
function isCommitted(agent, iNodeID){
	if(agent in iNodeToCommitList[iNodeID]){
		var committed = true;
		var node = commitNodes[iNodeToCommitList[iNodeID][agent]];
		while(node.previous != null){
			node = node.previous;
			committed = !committed;
		}
		return committed;
	}
	return false;
}

// Add an i-node or s-node to the argument 1 graph
function addArgument1Element(nodes, id, label, type){
	try{ 
		var shape = 'box';
		var color = 'aquamarine';
		if(type == 'sNode'){
			shape = 'circle';
		 	color = 'orange';
		} else if(type == "committed"){
			color = 'limegreen';
		} else if(type == "not committed"){
			color = '#FF7070';
		}
		node = {
			id: id,
			label: label,  
			shape: shape,
			color: color
		};
		idToNode[id] = node;
		nodes.add(node); 
	}catch (err) { console.log(err);
		//
	}
}

// Add an edge to the argument 1 graph
function addArgument1Edge(id, from, to){
	var edge = {id: id, from: from, to: to, arrows:{to: true}};
	idToEdge[id] = edge;
	argument1Edges.add(edge); 
}


///////////////////////////
/// ILLOCUTIONARY FORCE ///
///////////////////////////		

// The data containers
var illocutionaryForceNodes;  
var illocutionaryForceEdges;  
// Layout options
var illocutionaryForceOptions = {nodes:{shadow:true},edges:{shadow:true}};  
var illocutionaryForceGraph;
// Font for effects
var effectFont = {size:15, color:'white', face:'courier', strokeWidth:3, strokeColor:'#000000'};

var illocutionaryForceEffects;
var nodeAccumulator;
var edgeAccumulator;
var idToNode;
var idToEdge;
var dropDownHtml;
var messageToEffectGraph;
var commitmentAccumulator;

// When message is selected, we draw the argument 1 graph after that message and show which nodes & edges are new
function onMessageSelect(message){
	illocutionaryForceNodes = new vis.DataSet();  
	illocutionaryForceEdges = new vis.DataSet();
	var graph = messageToEffectGraph[message];   

	var data = {
		nodes: graph.nodes,
		edges: graph.edges
	};
	var container = document.getElementById('illocutionaryForce');
	$('#illocutionaryForce').html('');
	illocutionaryForceGraph = new vis.Network(container, data, illocutionaryForceOptions); 
}

// Turn the effects in the dialog graph to pre made argument 1 graphs that are annotated with the changes
function prepareIllocutionaryForce(){ 
	illocutionaryForceEffects = new Object();
	nodeAccumulator = [];
	edgeAccumulator = [];
	dropDownHtml = ''; 
	messageToEffectGraph = new Object(); 
	commitmentAccumulator = new Object();

	// Calculate the graphs
	dialogGraph.illocutionaryForce.effects.forEach(function(effect){
		illocutionaryForceEffects[effect.message] = effect;
	});
	dialogGraph.argument2Graph.messages.forEach(addMessageIllocutionaryForce); 

	$('#message .menu').html(dropDownHtml);
	$('#message').dropdown({ 
		onChange: function(value, text, $selectedItem) {
			onMessageSelect(value);
		}
	});

} 

// For a given message, draw the argument 1 graph that was the result after the message where the labels of 
// new nodes use the effect font
function addMessageIllocutionaryForce(message){  
	// Add as option in the drop down menu
	dropDownHtml += '<div class="item" data-value="'+message.id+'">'+message.id+':'+message.sender+','+message.content+','+message.receiver+'</div>';

	// Calculate the graph
	messageToEffectGraph[message.id] = new Object();
	messageToEffectGraph[message.id].nodes = [];
	messageToEffectGraph[message.id].edges = [];  

	var effect = illocutionaryForceEffects[message.id];
	if(effect.newNodes.length != 0 || effect.newEdges.length != 0){ 
		// Add accumulated nodes & edges with new ids, but not commits
		addAccumulator(message.id);
		var newCommitments = []; 

		// Add the effect nodes & edges to the graph and to the accumulator, but not the commits
		effect.newNodes.forEach(function(node){
			if(node in idToNode){ 
				var clone = cloneNode(idToNode[node]);
				nodeAccumulator.push(idToNode[node]); // Accumulate the node
				clone.label = clone.label+"*";
				clone.font = effectFont;
				messageToEffectGraph[message.id].nodes.push(clone);
				if(node in iNodeToCommitList){
					dialogGraph.agents.forEach(function(agent){
						var map = commitmentAccumulator[node];
						if(map == null){
							map = new Object();
							commitmentAccumulator[node] = map;
						}
						map[agent] = false;
						newCommitments.push(node+'-'+agent);
					});  
				}
			} else if(node in commitNodes){
				var commitNode = commitNodes[node];
				var committed = true;
				while(commitNode.next in commitNodes){
					committed = !committed; // Flip the boolean
					commitNode = commitNodes[commitNode.next];
				} // Commitnode.next now points to the iNode 
				var map = commitmentAccumulator[commitNode.next];
				if(map == null){
					map = new Object();
					commitmentAccumulator[commitNode.next] = map;
				}
				map[commitNode.owner] = committed;
				newCommitments.push(commitNode.next+'-'+commitNode.owner);
			}
		});

		effect.newEdges.forEach(function(edge){
			if(edge in idToEdge){
				var clone = cloneEdge(idToEdge[edge]);
				edgeAccumulator.push(idToEdge[edge]); // Accumulate the edge
				clone.label = '*'; 
				clone.font = effectFont;
				messageToEffectGraph[message.id].edges.push(clone); 
			}
		}); 

		for(iNode in commitmentAccumulator){
			if(commitmentAccumulator.hasOwnProperty(iNode)){
				for(agent in commitmentAccumulator[iNode]){
					if(commitmentAccumulator[iNode].hasOwnProperty(agent)){
						var commitmentId = iNode+'-'+agent;
						var committed = commitmentAccumulator[iNode][agent];
						var label = agent;
						var commitment = {
							id: commitmentId,
							shape: 'box',
							label: label,  
							color: (committed ? 'limegreen' : '#FF7070')
						}; 
						if(newCommitments.includes(commitmentId)){
							commitment.label += '*';
							commitment.font = effectFont;
						}
						messageToEffectGraph[message.id].nodes.push(commitment);
						messageToEffectGraph[message.id].edges.push({from: commitmentId, to: iNode});
					}
				}
			}
		} 
	} else { 
		messageToEffectGraph[message.id].nodes.push({
			id: 0,
			label: "No Effect", 
		}); 
	}
}

// Add the accumulated nodes and edges to the effect graph of the given node
function addAccumulator(id){
	nodeAccumulator.forEach(function(node){
		messageToEffectGraph[id].nodes.push(node);
	});

	edgeAccumulator.forEach(function(edge){ 
		messageToEffectGraph[id].edges.push(edge);
	});
}

// Clone a node
function cloneNode(node){
	var clone = new Object();
	clone.id = node.id;
	clone.label = node.label;
	clone.color = node.color;
	clone.shape = node.shape;
	return clone;
}

// Clone an edge
function cloneEdge(edge){
	var clone = new Object();
	clone.arrows = edge.arrows;
	clone.from = edge.from;
	clone.to = edge.to;
	clone.id = edge.id;
	return clone;
}

 
////////////
/// AIF  ///
////////////

// The data containers
var aifNodes;  
var aifEdges;  
// Layout options
var aifGraphOptions = {nodes:{shadow:true},edges:{shadow:true}};  
var aifGraph;

// Draws the AIF ontology and the connection from argument 1 to this ontology
function drawAIF(){
	aifNodes = new vis.DataSet();  
	aifEdges = new vis.DataSet(); 
	var data = {
		nodes: aifNodes,
		edges: aifEdges
	};
	var container = document.getElementById('aif');
	$('#aif').html('');
	aifGraph = new vis.Network(container, data, aifGraphOptions); 
	 
	// The ontology nodes:
	dialogGraph.aif.nodes.forEach(function(node){ 
		addAIFElement(node.id, node.text, node.id.substring(0,node.id.indexOf(':')));
	});

	// The dialog nodes
	dialogGraph.argument1Graph.iNodes.forEach(function(iNode){ 
		addAIFElement(iNode.id, '('+iNode.id+','+iNode.proposition+')', 'iNode');
	});

	dialogGraph.argument1Graph.sNodes.forEach(function(sNode){ 
		addAIFElement(sNode.id, '('+sNode.id+','+sNode.type+')', 'sNode');
	});

	// All the AIF connections
	dialogGraph.aif.edges.forEach(function(pair){ 
		aifEdges.add({from:pair.start, to: pair.end, arrows:{to: true}});
	}); 
}

function addAIFElement(id, label, type){
	try{ 
		var shape = 'box';
		if(type == 'sNode'){
			shape = 'circle';
		 	color = 'orange';
		} else if(type == 'iNode'){
			var color = 'aquamarine';
		} else if(type == 'scheme'){ 
		 	color = 'plum';
		} else if(type == 'aif'){ 
		 	color = 'yellow';
		}
		aifNodes.add({
			id: id,
			label: label,  
			shape: shape,
			color: color
		}); 
	}catch (err) { console.log(err);
		//
	}
}
